//
//  SettingsViewControllerGeneral.swift
//  AvantiCamera
//
//  Created by James Hager on 3/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import StoreKit
import MessageUI

class SettingsViewControllerGeneral: UITableViewController, MFMailComposeViewControllerDelegate {
    
    override var prefersStatusBarHidden: Bool {
        return  false
    }
    
    var settingsDataItems = [Int: SettingsDataItem]()
    var index = 0
    var settingsDataItemsIndexForIAP = [Int: Int]()
    var cellsToRegister: Set<SettingsCellIdentifiers> = []
    
    var defaultCellSeparatorInset: UIEdgeInsets!
    let noCellSeparatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    
    var doAdjustFonts = false
    
    var canDoBackdoor = false
    
    // list of available in-app purchases
    var products = [SKProduct]()
    var product: SKProduct!
    
    // priceFormatter is used to show proper, localized currency
    lazy var priceFormatter: NumberFormatter = {
        let pf = NumberFormatter()
        pf.formatterBehavior = .behavior10_4
        pf.numberStyle = .currency
        return pf
    }()
    
    lazy var file: String = getSourceFileNameFromFullPath(#file)
    
    func initSettingsDataItems() {
        print("=== \(file).\(#function) ===")
    }
    
    // MARK: - IB Stuff
    
    override func viewDidLoad() {
        print("=========================================")
        print("=== \(file).\(#function) === \(debugDate())")
        print("=========================================")
        super.viewDidLoad()
        
        initSettingsDataItems()
        
        registerCells()
        
        //tableView.layoutMargins = UIEdgeInsets.zero
        
        tableView.estimatedRowHeight = 68.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
        
        /*
         if !IAPPurchased(IAPProducts.FullVersion) {
         // Subscribe to a notification that fires when a product is purchased.
         NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewControllerGeneral.productPurchased(_:)), name: NSNotification.Name(rawValue: IAPHelperProductPurchasedNotification), object: nil)
         
         if products.count == 0 {
         NotificationCenter.default.addObserver(
         self,
         selector: #selector(SettingsViewControllerGeneral.reload),
         name: NSNotification.Name(rawValue: "applicationDidBecomeActive"),
         object: nil)
         }
         }
         */
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        print("-----------------------------------------")
        print("=== \(file).\(#function) === \(debugDate())")
        print("-----------------------------------------")
        
        // IAP
        /*
         if connectedToNetwork() && !IAPPurchased(IAPProducts.FullVersion) {
         print("--- \(file).\(#function) - prep to get products from iTunes connect")
         
         refreshControl = UIRefreshControl()
         refreshControl?.addTarget(self, action: #selector(SettingsViewControllerGeneral.reload), for: .valueChanged)
         reload()
         refreshControl?.beginRefreshing()
         
         // Subscribe to a notification that fires when a product is purchased.
         NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewControllerGeneral.productPurchased(_:)), name: NSNotification.Name(rawValue: IAPHelperProductPurchasedNotification), object: nil)
         }
         */
        
        tableView.reloadData()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        print("=== \(file).\(#function) - self=\(self)")
        
        /*
         if !IAPPurchased(IAPProducts.FullVersion) {
         if previousTraitCollection?.preferredContentSizeCategory != traitCollection.preferredContentSizeCategory {
         doAdjustFonts = true
         tableView.reloadData()
         }
         }
         */
    }
    
    func registerCells() {
        print("=== \(file).\(#function) ===")
        for settingsCellIdentifier in cellsToRegister {
            print("--- \(file).\(#function) - '\(settingsCellIdentifier)'")
            tableView.register(UINib(nibName: settingsCellIdentifier.rawValue, bundle: nil), forCellReuseIdentifier: settingsCellIdentifier.rawValue)
        }
    }
    
    func reload() {
        print("=== \(file).\(#function) ===")
        // fetch the products from iTunes connect, redisplay the table on successful completion
        products = []
        tableView.reloadData()
        /*
         IAPProducts.store.requestProductsWithCompletionHandler { success, products in
         if success {
         self.products = products
         print("--- \(self.file).\(#function) - returned from IAPProducts.store.requestProductsWithCompletionHandler - products.count=\(self.products.count)")
         self.tableView.reloadData()
         }
         self.refreshControl?.endRefreshing()
         }
         */
    }
    
    /*
     func productPurchased(_ notification: Notification) {
     print("=== \(file).\(#function) - notification='\(notification)' ===")
     let productIdentifier = notification.object as! String
     UserDefaults.standard.set(true, forKey: productIdentifier)
     displayPurchased(self)
     setUpIAPFunctionality()
     tableView.reloadData()
     }
     */
    
    func addSettingsDataItem(_ settingsDataItem: SettingsDataItem) {
        print("=== \(file).\(#function) - type=\(settingsDataItem.dataType) ===")
        settingsDataItems[index] = settingsDataItem
        
        var previousSettingsDataItemsIndexForIAPValue: Int!
        if let previousSettingsDataItemsIndexForIAP = settingsDataItemsIndexForIAP[index - 1] {
            previousSettingsDataItemsIndexForIAPValue = previousSettingsDataItemsIndexForIAP
        } else {
            previousSettingsDataItemsIndexForIAPValue = -1
        }
        
        if settingsDataItem.dataType == .upgradeToFullVersion {
            settingsDataItemsIndexForIAP[index] = previousSettingsDataItemsIndexForIAPValue + 2
        } else {
            settingsDataItemsIndexForIAP[index] = previousSettingsDataItemsIndexForIAPValue + 1
        }
        
        cellsToRegister.insert(settingsDataItem.cellType)
        
        print("--- \(file).\(#function) - type=\(settingsDataItem.dataType), settingsDataItemsIndexForIAP[\(index)]=\(settingsDataItemsIndexForIAP[index]!)")
        
        index += 1
    }
    
    func rowToShowForIndexPath(_ indexPath: IndexPath) -> Int {
        /*
         var rowToShow: Int!
         if IAPPurchased(IAPProducts.FullVersion) {
         rowToShow = settingsDataItemsIndexForIAP[indexPath.row]!
         } else {
         rowToShow = indexPath.row
         }
         */
        let rowToShow = indexPath.row
        print("=== \(file).\(#function) \(displayPath(indexPath)) -> \(rowToShow) ===")
        return rowToShow
    }
    
    func setBottomSeperator(indexPath: IndexPath, cell: UITableViewCell) {
        //print("=== \(file).\(#function) - indexPath.......: \(displayPath(indexPath))")
        
        var nextRowIndexPath = indexPath
        nextRowIndexPath.row += 1
        //print("--- \(file).\(#function) - nextRowIndexPath: \(displayPath(nextRowIndexPath))")
        
        if defaultCellSeparatorInset == nil {
            defaultCellSeparatorInset = cell.separatorInset
        }
        
        var hideBottomSeperator = false
        
        if nextRowIndexPath.row < tableView(tableView, numberOfRowsInSection: indexPath.section) {
            let rowToShow = rowToShowForIndexPath(nextRowIndexPath)
            let settingsDataItem = settingsDataItems[rowToShow]!
            if settingsDataItem.cellType == .sectionCell {
                //print("--- \(file).\(#function) - next row cellType is .sectionCell")
                hideBottomSeperator =  true
                //} else {
                //print("--- \(file).\(#function) - next row cellType is not .sectionCell")
            }
            //} else {
            //print("--- \(file).\(#function) - there is no next row")
        }
        
        if hideBottomSeperator {
            cell.separatorInset = noCellSeparatorInset
        } else {
            cell.separatorInset = defaultCellSeparatorInset
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        print("=== \(file).\(#function) -> 1 ===")
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*
         var numberOfRows: Int!
         if IAPPurchased(IAPProducts.FullVersion) {
         let lastIndex = settingsDataItemsIndexForIAP.count - 1
         print("=== \(file).\(#function) - lastIndex \(lastIndex) ===")
         numberOfRows = lastIndex - (settingsDataItemsIndexForIAP[lastIndex]! - lastIndex) + 1
         } else {
         numberOfRows =  settingsDataItems.count
         }
         */
        let numberOfRows =  settingsDataItems.count
        print("=== \(file).\(#function) -> \(numberOfRows) ===")
        return numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("=== \(file).\(#function) \(displayPath(indexPath)) === \(displayPath(indexPath)) ===")
        let rowToShow = rowToShowForIndexPath(indexPath)
        let settingsDataItem = settingsDataItems[rowToShow]!
        
        /*
         if !IAPPurchased(IAPProducts.FullVersion) {
         //print("--- \(file).\(#function) \(displayPath(indexPath)) - row=\(indexPath.row), numRows=\(settingsDataItems.count)")
         if indexPath.row == settingsDataItems.count - 1 {
         canDoBackdoor = true
         //print("--- \(file).\(#function) \(displayPath(indexPath)) - canDoBackdoor = true")
         }
         }
         */
        
        switch settingsDataItem.cellType {
        case .textCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsTextCell
            cell.configureCell(settingsDataItem.text, withDisclosureIndicator: settingsDataItem.disclosureIndicator)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .segmentedControlCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsSegmentedControlCell
            var enabled: Bool!
            if let closure = settingsDataItem.enabledOnClosure {
                enabled = closure()
            } else {
                enabled = true
            }
            cell.configureCell(settingsDataItem.text, segmentedControlName: settingsDataItem.segmentedControlName, values: settingsDataItem.values as! [String], segmentWidth: settingsDataItem.segmentWidth, enabled: enabled)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .switchCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsSwitchCell
            var enabled: Bool!
            if let closure = settingsDataItem.enabledOnClosure {
                enabled = closure()
            } else {
                enabled = true
            }
            cell.configureCell(settingsDataItem.text, switchName: settingsDataItem.switchName, enabled: enabled)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .valueCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsValueCell
            cell.configureCell(settingsDataItem.text, valueName: settingsDataItem.valueName, valueType: settingsDataItem.valueType, values: settingsDataItem.values)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .sectionCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsSectionCell
            cell.configureCell(settingsDataItem.text)
            cell.separatorInset = noCellSeparatorInset
            print("--- \(file).\(#function) - this is a .sectionCell ---------------")
            return cell
            
        case .bulletItemCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsBulletItemCell
            cell.configureCell(settingsDataItem.text, withDisclosureIndicator: settingsDataItem.disclosureIndicator)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .textSmallCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsTextSmallCell
            
            if settingsDataItem.dataType == .version {
                let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
                let bundle = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
                var text = "ACamera \(version) (\(bundle))"
                /*
                 if IAPPurchased(IAPProducts.FullVersion) {
                 text += ", Full Version"
                 } else {
                 text += ", Limited Version"
                 }
                 */
                text += "\n© \(copyrightYear) Avanti Applications, LLC"
                cell.configureCell(text)
                
            } else if let closure = settingsDataItem.textFromClosure {
                cell.configureCell(closure())
                
            } else {
                cell.configureCell(settingsDataItem.text)
            }
            
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
            
        case .legalCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsLegalCell
            cell.configureCell(item: settingsDataItem.text, desc: settingsDataItem.text2)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .textBuyRestoreCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsTextBuyRestoreCell
            
            if settingsDataItem.dataType == .upgradeToFullVersion {
                var descriptionText = fullVersionDescriptionText
                if products.count > 0 {
                    if priceFormatter.locale != products[0].priceLocale {
                        priceFormatter.locale = products[0].priceLocale
                    }
                    descriptionText += " for \(priceFormatter.string(from: products[0].price)!)"
                }
                descriptionText += "."
                cell.configureCell("Upgrade to Full Version", descriptionText: descriptionText, product: product, viewController: self, doAdjustFonts: doAdjustFonts)
            }
            
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("=== \(file).\(#function) \(displayPath(indexPath)) ===")
        
        let rowToShow = rowToShowForIndexPath(indexPath)
        let settingsDataItem = settingsDataItems[rowToShow]!
        
        if let closure = settingsDataItem.closure {
            print("--- \(file).\(#function) \(displayPath(indexPath)) - closure is defined")
            closure()
            
        } else if settingsDataItem.dataType == .valuePicker {
            let vc = SettingsValueViewController(nibName: "SettingsViewGeneral", bundle: nil)
            vc.viewTitle = settingsDataItem.text
            vc.valueName = settingsDataItem.valueName
            vc.valueType = settingsDataItem.valueType
            vc.values = settingsDataItem.values
            //navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            //navigationController?.pushViewController(vc, animated: true)
            
            show(vc, sender: nil)
        }
    }
}
