
import UIKit

class SettingsTextCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String, withDisclosureIndicator disclosureIndicator: Bool = false) {
        label.text = text
        
        if disclosureIndicator {
            accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        } else {
            selectionStyle = UITableViewCellSelectionStyle.none
        }
    }
}
