//
//  SettingsViewController.swift
//  AvantiCamera
//
//  Created by James Hager on 3/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import StoreKit
import MessageUI

class SettingsViewController: SettingsViewControllerGeneral {
    
    var coreDataStack: CoreDataStack!
    
    var camera: Camera!
    
    /*
     override var shouldAutorotate: Bool {
     return true
     }
     
     override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
     return .all
     }
     */
    
    var deviceHasFlash: Bool {
        get {
            return camera.deviceHasFlash && camera.isCameraAvailable
        }
    }
    
    var deviceHasTorch: Bool {
        get {
            return camera.deviceHasTorch && camera.isCameraAvailable
        }
    }
    
    var viewTitle: String!
    var valueName: String!
    var values: [Double: String] = [:]
    var valueValues: [Double] = []
    
    var aboutViewController: AboutViewController!
    var speechCommandViewController: SpeechCommandViewController!
    
    @IBAction func SettingsValueViewControllerDidSetNewValue(_ segue: UIStoryboardSegue) {
        print("=== ForecastSettings.SettingsValueViewControllerDidSetNewValue ===")
        tableView.reloadData()
    }
    
    var fileLocal = "SettingsViewController"
    
    override func initSettingsDataItems() {
        print("=== \(fileLocal).\(#function) ===")
        
        let cameraSettings = CameraSettings()
        let segmentWidth = CGFloat(50)
        
        var settingsDataItem: SettingsDataItem!
        
        settingsDataItem = SettingsDataItem(dataType: .version)
        addSettingsDataItem(settingsDataItem)
        
        /*
         settingsDataItem = SettingsDataItem(dataType: .upgradeToFullVersion)
         addSettingsDataItem(settingsDataItem)
         */
        
        settingsDataItem = SettingsDataItem(text: "About", closure: {self.showAbout()})
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Recommend ACamera", closure: {self.handleRecommend()})
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Support", closure: {self.presentModalMailComposeViewController(true)})
        addSettingsDataItem(settingsDataItem)
        
        /*
         settingsDataItem = SettingsDataItem(text: "Legal", closure: {self.showLegal()})
         addSettingsDataItem(settingsDataItem)
         */
        
        settingsDataItem = SettingsDataItem(text: "Reset Settings...", closure: {self.handleResetSettings()})
        addSettingsDataItem(settingsDataItem)
        
        /*
         // Camera Settings
         
         if camera.deviceHasBackCamera && camera.deviceHasFrontCamera {
         settingsDataItem = SettingsDataItem(text: "Camera Settings", cellType: .sectionCell)
         addSettingsDataItem(settingsDataItem)
         
         settingsDataItem = SettingsDataItem(text: "Camera", segmentedControlName: cameraSettings.fontBackStateKey, values: cameraSettings.fontBackStates as AnyObject, segmentWidth: segmentWidth * 3 / 2)
         addSettingsDataItem(settingsDataItem)
         }
         */
        
        // Photo Settings
        
        settingsDataItem = SettingsDataItem(text: "Photo Settings", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Aspect Ratio", valueName: cameraSettings.aspectRatioKey, valueType: .double, values: cameraSettings.aspectRatios as AnyObject)
        addSettingsDataItem(settingsDataItem)
        
        if deviceHasFlash {
            settingsDataItem = SettingsDataItem(text: "Flash", segmentedControlName: cameraSettings.flashStateKey, values: cameraSettings.flashStates as AnyObject, segmentWidth: segmentWidth)
            addSettingsDataItem(settingsDataItem)
        }
        
        settingsDataItem = SettingsDataItem(text: "Self Timer", segmentedControlName: cameraSettings.selfTimerStateKey, values: cameraSettings.selfTimerStates as AnyObject, segmentWidth: segmentWidth)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Burst", segmentedControlName: cameraSettings.burstStateKey, values: cameraSettings.burstStates as AnyObject, segmentWidth: segmentWidth)
        addSettingsDataItem(settingsDataItem)
        
        // Video Settings
        
        if deviceHasTorch {
            settingsDataItem = SettingsDataItem(text: "Video Settings", cellType: .sectionCell)
            addSettingsDataItem(settingsDataItem)
            
            settingsDataItem = SettingsDataItem(text: "Light", segmentedControlName: cameraSettings.torchStateKey, values: cameraSettings.torchStates as AnyObject, segmentWidth: segmentWidth)
            addSettingsDataItem(settingsDataItem)
        }
        
        // Speech Settings
        
        settingsDataItem = SettingsDataItem(text: "Speech Settings", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
        
        let cameraActions: [CameraAction] = [.setPhoto, .takePhoto, .setVideo, .startVideo, .stopVideo, .stopSpeech]
        
        for cameraAction in cameraActions {
            settingsDataItem = SettingsDataItem(text: cameraAction.command, closure: {self.showSpeechCommands(cameraAction: cameraAction)})
            addSettingsDataItem(settingsDataItem)
        }
        
        // Export Settings
        
        settingsDataItem = SettingsDataItem(text: "Export Settings", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Photo Format", valueName: cameraSettings.photoFormatKey, valueType: .string, values: cameraSettings.photoFormats as AnyObject)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Photo Color Space", valueName: cameraSettings.photoColorSpaceKey, valueType: .string, values: cameraSettings.photoColorSpaces as AnyObject)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Photo Image Quality", valueName: cameraSettings.photoImageQualityKey, valueType: .double, values: cameraSettings.photoImageQualities as AnyObject)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Video Format", valueName: cameraSettings.videoFormatKey, valueType: .string, values: cameraSettings.videoFormats as AnyObject)
        addSettingsDataItem(settingsDataItem)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("=== \(fileLocal).\(#function) ===")
        navigationItem.title = "Settings"
    }
    
    func handleRecommend() {
        let url  = URL(string: "itms-apps://itunes.apple.com/us/app/aweather/id1369779274?ls=1&mt=8")
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!)
        }
    }
    
    func handleResetSettings() {
        print("=== \(fileLocal).\(#function) ===")
        
        let alert = UIAlertController(title: "Reset Settings",
                                      message: "Reset the settings to the defaut values?",
                                      preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        let okAction = UIAlertAction(title: "Reset", style: .destructive, handler: {(alert: UIAlertAction!) in self.resetSettings()})
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func resetSettings() {
        print("=== \(fileLocal).\(#function) ===")
        setUserDefaults()
        coreDataStack.deleteAllSpeechCommands()
        setDefaultSpeechCommands(coreDataStack: coreDataStack)
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    // MARK: - UITableViewDelegate Methods
    
    // MARK: - Navigation
    
    func showAbout() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if aboutViewController == nil {
            aboutViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
            aboutViewController.camera = camera
        }
        
        navigationController?.pushViewController(aboutViewController, animated: true)
    }
    
    func showSpeechCommands(cameraAction: CameraAction) {
        print("=== \(file).\(#function) === \(debugDate()) - \(cameraAction)")
        
        if speechCommandViewController == nil {
            speechCommandViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SpeechCommandViewController") as! SpeechCommandViewController
            speechCommandViewController.coreDataStack = coreDataStack
        }
        
        speechCommandViewController.cameraAction = cameraAction
        
        navigationController?.pushViewController(speechCommandViewController, animated: true)
    }
    
    //MARK: - MFMailCompose
    
    func presentModalMailComposeViewController(_ animated: Bool) {
        if MFMailComposeViewController.canSendMail() {
            let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            let bundle = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
            let subject = "ACamera \(version) (\(bundle))"
            let body = "Feature request or bug report?"
            let recipients = ["Avanti Applications Support <support@avantiapplications.com>"]
            
            let mailComposeVC = MFMailComposeViewController()
            mailComposeVC.mailComposeDelegate = self
            
            mailComposeVC.navigationBar.barStyle = .black
            mailComposeVC.navigationBar.barTintColor = Colors.avantiGreen
            mailComposeVC.navigationBar.tintColor = Colors.textSelectable
            mailComposeVC.navigationBar.isTranslucent = false
            
            mailComposeVC.setSubject(subject)
            mailComposeVC.setMessageBody(body, isHTML: true)
            mailComposeVC.setToRecipients(recipients)
            
            present(mailComposeVC, animated: animated, completion: {UIApplication.shared.statusBarStyle = .lightContent})
            
        } else {
            let alert = UIAlertController(title: "Error", message: "Your device doesn't support Mail messaging.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        if error != nil {
            print("Error: \(String(describing: error))")
        }
        
        dismiss(animated: true, completion: nil)
    }
}
