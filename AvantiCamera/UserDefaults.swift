//
//  UserDefaults.swift
//  AvantiCamera
//
//  Created by James Hager on 3/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

func setUserDefaults() {
    print("=== UserDefaults.\(#function) ===")
    
    let userDefaults = UserDefaults.standard
    setUserDefaultSettings(userDefaults)
    userDefaults.synchronize()
}

func setUserDefaultSettings(_ userDefaults: UserDefaults) {
    print("=== UserDefaults.\(#function) ===")
    let cameraSettings = CameraSettings()
    
    // camera settings
    userDefaults.set(cameraSettings.fontBackStateDefault, forKey: cameraSettings.fontBackStateKey)
    
    // photo settings
    userDefaults.set(cameraSettings.aspectRatioDefault, forKey: cameraSettings.aspectRatioKey)
    userDefaults.set(cameraSettings.flashStateDefault, forKey: cameraSettings.flashStateKey)
    userDefaults.set(cameraSettings.selfTimerStateDefault, forKey: cameraSettings.selfTimerStateKey)
    userDefaults.set(cameraSettings.burstStateDefault, forKey: cameraSettings.burstStateKey)
    userDefaults.set(0, forKey: "CameraInterval")
    
    // video settings
    userDefaults.set(cameraSettings.torchStateDefault, forKey: cameraSettings.torchStateKey)
    
    // export formats
    userDefaults.set(cameraSettings.photoFormatDefault, forKey: cameraSettings.photoFormatKey)
    userDefaults.set(cameraSettings.photoColorSpaceDefault, forKey: cameraSettings.photoColorSpaceKey)
    userDefaults.set(cameraSettings.photoImageQualityDefault, forKey: cameraSettings.photoImageQualityKey)
    userDefaults.set(cameraSettings.videoFormatDefault, forKey: cameraSettings.videoFormatKey)
}

func setDefaultSpeechCommands(coreDataStack: CoreDataStack) {
    print("=== UserDefaults.\(#function) ===")
    setDefaultSpeechCommands(cameraAction: .setPhoto, coreDataStack: coreDataStack)
    setDefaultSpeechCommands(cameraAction: .takePhoto, coreDataStack: coreDataStack)
    
    setDefaultSpeechCommands(cameraAction: .setVideo, coreDataStack: coreDataStack)
    setDefaultSpeechCommands(cameraAction: .startVideo, coreDataStack: coreDataStack)
    setDefaultSpeechCommands(cameraAction: .stopVideo, coreDataStack: coreDataStack)
    
    setDefaultSpeechCommands(cameraAction: .stopSpeech, coreDataStack: coreDataStack)
    
    coreDataStack.saveContext()
}

func setDefaultSpeechCommands(cameraAction: CameraAction, coreDataStack: CoreDataStack) {
    print("=== UserDefaults.\(#function) === \(cameraAction)")
    
    switch cameraAction {
        
    // photo
    case .setPhoto:
        coreDataStack.makeSpeechCommand(cameraAction: .setPhoto, verb: "set", object: "photo", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .setPhoto, verb: "select", object: "photo", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .setPhoto, verb: "set", object: "camera", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .setPhoto, verb: "select", object: "camera", andSaveContext: false)
        
    case .takePhoto:
        coreDataStack.makeSpeechCommand(cameraAction: .takePhoto, verb: "take", object: "photo", andSaveContext: false)
        
    // video
    case .setVideo:
        coreDataStack.makeSpeechCommand(cameraAction: .setVideo, verb: "set", object: "video", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .setVideo, verb: "select", object: "video", andSaveContext: false)
        
    case .startVideo:
        coreDataStack.makeSpeechCommand(cameraAction: .startVideo, verb: "start", object: "video", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .startVideo, verb: "start", object: "recording", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .startVideo, verb: "begin", object: "recording", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .startVideo, verb: "take", object: "video", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .startVideo, verb: "record", object: "video", andSaveContext: false)
        
    case .stopVideo:
        coreDataStack.makeSpeechCommand(cameraAction: .stopVideo, verb: "stop", object: "video", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .stopVideo, verb: "stop", object: "recording", andSaveContext: false)
        coreDataStack.makeSpeechCommand(cameraAction: .stopVideo, verb: "end", object: "recording", andSaveContext: false)
        
    // speech
    case .stopSpeech:
        coreDataStack.makeSpeechCommand(cameraAction: .stopSpeech, verb: "stop", object: "speech", andSaveContext: false)
        
    default: break
    }
}
