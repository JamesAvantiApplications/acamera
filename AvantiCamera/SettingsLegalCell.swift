import UIKit

class SettingsLegalCell: UITableViewCell {
    
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(item: String, desc: String) {
        itemLabel.text = item
        descLabel.text = desc
    }
}
