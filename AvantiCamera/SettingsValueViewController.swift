//
//  SettingsValueViewController.swift
//  Weather_NWS
//
//  Created by James Hager on 11/20/15.
//  Copyright © 2015 James Hager. All rights reserved.
//

import UIKit

class SettingsValueViewController: UITableViewController {
    
    let userDefaults = UserDefaults.standard
    
    var viewTitle: String!
    var valueName: String!
    var valueType: ValueType!
    var values: AnyObject!
    var valuesToDisplay: [AnyObject]!
    var indexOfSavedValue: Int!
    
    var valuesHelper: SettingsValueHelper!
    
    lazy var value: AnyObject = self.valuesHelper.getSavedValueForValueName()
    
    var selectedIndexPath = IndexPath()
    
    lazy var file: String = getSourceFileNameFromFullPath(#file)
    
    struct TableViewCellIdentifiers {
        static let settingsValueViewCell = "SettingsValueViewCell"
    }
    
    override func viewDidLoad() {
        print("=========================================")
        print("=== \(file).\(#function) === \(debugDate())")
        print("=========================================")
        super.viewDidLoad()
        
        navigationItem.title = viewTitle
        
        let cellNib = UINib(nibName: TableViewCellIdentifiers.settingsValueViewCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: TableViewCellIdentifiers.settingsValueViewCell)
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        valuesHelper = SettingsValueHelper(valueName: valueName, values: values)
        valuesToDisplay = valuesHelper.valuesToDisplay()
        
        for value in valuesToDisplay {
            print("--- \(file).\(#function) - valuesToDisplay='\(value)'")
        }
        
        indexOfSavedValue = valuesHelper.getIndexOfSavedValueForValueName()
        print("--- \(file).\(#function) - indexOfSavedValue=\(indexOfSavedValue)")
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("=== \(file).\(#function) === \(displayPath(indexPath)) ===")
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.settingsValueViewCell, for: indexPath) as! SettingsValueViewCell
        
        print("--- \(file).\(#function) -- \(displayPath(indexPath)) - valueToDisplay='\(valuesToDisplay[indexPath.row])'")
        cell.label?.text = "\(valuesToDisplay[indexPath.row])"
        
        if indexPath.row == indexOfSavedValue {
            selectedIndexPath = indexPath
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
            cell.tintColor = Colors.avantiGreen
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.default
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("=== SettingsValue.tableView:didSelectRowAtIndexPath ===")
        print("--- SettingsValue.tableView:didSelectRowAtIndexPath - indexPath.row=\(indexPath.row)")
        if indexPath.row != selectedIndexPath.row {
            valuesHelper.saveValueForIndex(indexPath.row)
        }
        
        navigationController?.popViewController(animated: true)
    }
}
