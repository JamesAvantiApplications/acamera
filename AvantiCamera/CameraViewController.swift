//
//  CameraViewController.swift
//  AvantiCamera
//
//  Created by James Hager on 3/21/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  Video capture/reply based on https://stackoverflow.com/questions/41697568/capturing-video-with-avfoundation
//  Image capture was inspired by https://stackoverflow.com/questions/37869963/how-to-use-avcapturephotooutput
//  Video save to photo library was inspired by https://stackoverflow.com/questions/29482738/swift-save-video-from-nsurl-to-user-camera-roll
//  Speech recognition based on https://medium.com/ios-os-x-development/speech-recognition-with-swift-in-ios-10-50d5f4e59c48
//

import UIKit
import AVFoundation
import Photos
import Speech
//import MobileCoreServices

class CameraViewController: UIViewController {
    
    var coreDataStack: CoreDataStack!
    
    var isStatusBarHidden = true
    
    override var prefersStatusBarHidden: Bool { return  isStatusBarHidden }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    var firstWillAppear = true
    
    var orientationOnLaunch: ACDeviceOrientation = .portrait {
        didSet {
            print("=== \(file).\(#function) === \(debugDate()) - didSet orientationOnLaunch: \(orientationOnLaunch)")
        }
    }
    
    var notInViewDidLoad = false
    var displayPreviewOnWillAppear = true
    
    let camera = Camera()
    
    var cameraMode = CameraMode.photo {
        didSet {
            if notInViewDidLoad {
                setCameraDisplay()
            }
        }
    }
    
    var recognizeLongPressOnCameraIcon = false {
        didSet {
            //print("=== ACImageView.delegate didSet - listenForVideoIndicatorSizeShouldChange: \(listenForVideoIndicatorSizeShouldChange)")
            if recognizeLongPressOnCameraIcon != recognizeLongPressOnCameraIconSet {
                recognizeLongPressOnCameraIconSet = recognizeLongPressOnCameraIcon
                if recognizeLongPressOnCameraIcon {
                    cameraIconImageView.addGestureRecognizer(cameraLongPressGestureRecognizer)
                } else {
                    cameraIconImageView.removeGestureRecognizer(cameraLongPressGestureRecognizer)
                }
            }
        }
    }
    var recognizeLongPressOnCameraIconSet = false
    
    lazy var cameraLongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.toggleFrontBackCamera(_:)))
    
    var camPreviewSubview: UIView!
    var statusLabel: UILabel!
    var selfTimerLabel: UILabel!
    var selfTimerLabelAttributes: [NSAttributedStringKey : Any]!
    
    var camPreviewConstraints = [NSLayoutConstraint]()
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var assetCreatedIdentifier: String!
    
    var isSpeechAvailable = false {
        didSet {
            handleDidSetIsSpeechAvailable()
        }
    }
    
    var isSpeechAvailableSet = false
    
    var isRecognizingSpeech = false {
        didSet {
            if isRecognizingSpeech {
                wasRecognizingSpeech = true
            }
            setSpeechDisplay()
        }
    }
    
    var wasRecognizingSpeech = false
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    var mostRecentlyProcessedSegmentDuration: TimeInterval = 0
    
    var cameraOperation: CameraOperation!
    
    var doCheckSettingsInUse = true
    
    let previewImageSize = CGSize(width: 80, height: 80)
    
    var videoPhotoButtonView: CameraButtonView!
    var videoDurationLabel: UILabel!
    
    var settingsViewController: SettingsViewController!
    var photoLibraryViewController: PhotoLibraryViewController!
    var photoViewController: PhotoViewController!
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    // MARK: - IB Stuff
    
    @IBOutlet weak var camPreview: UIView!
    
    @IBOutlet weak var settingsIconImageView: UIImageView!
    @IBOutlet weak var leftBottomView: UIView!
    @IBOutlet weak var cameraIconImageView: UIImageView!
    @IBOutlet weak var cameraButtonView: CameraButtonView!
    @IBOutlet weak var rightBottomView: UIView!
    @IBOutlet weak var thumbImageView: ACImageView!
    @IBOutlet weak var voiceButtonImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("=========================================")
        //print("=== \(file).\(#function) === \(debugDate())")
        //print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation())")
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation()), deviceOrientation: \(camera.deviceOrientation) \(orientationOnLaunch)")
        print("=========================================")
        
        cameraOperation = CameraOperation(coreDataStack: coreDataStack)
        
        orientationOnLaunch.setFromDeviceOrientation()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        
        camera.delegate = self
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        cameraButtonView.widthAnchor.constraint(equalTo: cameraButtonView.heightAnchor, multiplier: 1).isActive = true
        cameraButtonView.camera = camera
        
        thumbImageView.contentMode = .scaleAspectFit
        thumbImageView.videoIndicatorSize = CGFloat(10)
        
        print("--- \(file).\(#function) - call setUpView")
        setUpView()
        
        print("--- \(file).\(#function) - call requestAuthorizations")
        requestAuthorizations()
        
        cameraOperation.delegate = self
        
        let settingsGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.showSettings))
        settingsIconImageView.addGestureRecognizer(settingsGestureRecognizer)
        settingsIconImageView.isUserInteractionEnabled = true
        
        let cameraGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.toggleCameraMode))
        cameraIconImageView.addGestureRecognizer(cameraGestureRecognizer)
        cameraIconImageView.isUserInteractionEnabled = true
        
        let showLibraryRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.showLibrary))
        thumbImageView.addGestureRecognizer(showLibraryRecognizer)
        
        let voiceGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.toggleSpeechRecognition))
        voiceButtonImageView.addGestureRecognizer(voiceGestureRecognizer)
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.applicationWillEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.applicationDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.applicationWillResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        
        notInViewDidLoad = true
        camera.actOnIsCameraAvailableDidSet = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("-----------------------------------------")
        //print("=== \(file).\(#function) === \(debugDate())")
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation()), deviceOrientation: \(camera.deviceOrientation) \(orientationOnLaunch)")
        print("-----------------------------------------")
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        handleWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("+++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++")
        //print("=== \(file).\(#function) === \(debugDate())")
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation()), deviceOrientation: \(camera.deviceOrientation) \(orientationOnLaunch)")
        print("+++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("=== \(file).\(#function) === \(debugDate())")
        
        /*
         if cameraMode == .video {
         stopRecording()
         }
         */
        
        if isRecognizingSpeech {
            stopRecognizeSpeech()
        }
        
        handleWillDisappear()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if videoDurationLabel != nil {
            //let bodyFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
            //videoDurationLabel.font = UIFont.monospacedDigitSystemFont(ofSize: bodyFont.pointSize, weight: .regular)
            //videoDurationLabel.adjustsFontForContentSizeCategory = true
            //videoDurationLabel.sizeToFit()
            videoDurationLabel.font = videoDurationLabelFont()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation()), deviceOrientation: \(camera.deviceOrientation)  \(orientationOnLaunch)")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation()), deviceOrientation: \(camera.deviceOrientation) \(orientationOnLaunch)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Setup View and View Controllers
    
    func setUpView() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        camPreviewSubview = UIView(frame: CGRect.zero)
        camPreviewSubview.translatesAutoresizingMaskIntoConstraints = false
        camPreview.addSubview(camPreviewSubview)
        
        camPreviewSubview.centerXAnchor.constraint(equalTo: camPreview.centerXAnchor).isActive = true
        camPreviewSubview.centerYAnchor.constraint(equalTo: camPreview.centerYAnchor).isActive = true
        
        //camPreviewSubview.backgroundColor = .gray
        camPreviewSubview.backgroundColor = .clear
        
        //camPreview.backgroundColor = .gray
        //camPreviewSubview.backgroundColor = .red
        
        if let image = UIImage(named: "Settings") {
            settingsIconImageView.image = image.withRenderingMode(.alwaysTemplate)
            settingsIconImageView.tintColor = .lightGray
        }
        
        setCamPreviewConstraints()
        
        setCameraDisplay()
        
        setSpeechDisplay()
        
        isRecognizingSpeech = false
    }
    
    func requestAuthorizations() {
        print("=== \(file).\(#function) === \(debugDate())")
        camera.requestAuthorization()
        requestSpeechAuthorization()
    }
    
    func setCamPreviewConstraints() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        NSLayoutConstraint.deactivate(camPreviewConstraints)
        camPreviewConstraints = [NSLayoutConstraint]()
        
        print("--- \(file).\(#function) --- \(debugDate()) - deviceOrientation: \(camera.deviceOrientation)")
        
        let maxWidth = camPreview.frame.width
        let maxHeight = camPreview.frame.height
        
        /*
         var maxWidth: CGFloat!
         var maxHeight: CGFloat!
         
         if camera.deviceOrientation == .portrait {
         maxWidth = camPreview.frame.width
         maxHeight = camPreview.frame.height
         } else {
         maxWidth = camPreview.frame.height
         maxHeight = camPreview.frame.width
         }
         */
        print("--- \(file).\(#function) --- \(debugDate()) - maxWidth: \(maxWidth), maxHeight: \(maxHeight)")
        
        let actualWidth: CGFloat = 1.0
        let actualHeight: CGFloat = actualWidth * camera.camPreviewAspectRatio
        
        /*
         var actualWidth: CGFloat!
         var actualHeight: CGFloat!
         
         if camera.deviceOrientation == .portrait {
         actualWidth = 1.0
         actualHeight = actualWidth * camera.camPreviewAspectRatio
         } else {
         actualHeight = 1.0
         actualWidth = actualHeight / camera.camPreviewAspectRatio
         }
         */
        print("--- \(file).\(#function) --- \(debugDate()) - actualWidth: \(actualWidth), actualHeight: \(actualHeight)")
        
        let scale = min(maxWidth/actualWidth, maxHeight/actualHeight)
        let width = actualWidth*scale
        let height = actualHeight*scale
        
        /*
         print("--- \(file).\(#function) - camPreviewAspectRatio: \(camPreviewAspectRatio)")
         print("--- \(file).\(#function) - maxHeight: \(maxHeight)")
         print("--- \(file).\(#function) -  maxWidth: \(maxWidth)")
         print("--- \(file).\(#function) -    height: \(height)")
         print("--- \(file).\(#function) -     width: \(width)")
         */
        
        camPreviewConstraints.append(camPreviewSubview.widthAnchor.constraint(equalToConstant: width))
        camPreviewConstraints.append(camPreviewSubview.heightAnchor.constraint(equalToConstant: height))
        
        NSLayoutConstraint.activate(camPreviewConstraints)
        
        camPreviewSubview.layoutIfNeeded()
        
        var newFrame = CGRect.zero
        newFrame.size = CGSize(width: width, height: height)
        
        //print("--- \(file).\(#function) - newFrame: \(newFrame)")
        
        previewLayer?.frame = newFrame
    }
    
    func setButtonTransforms(cameraOrientation: ACDeviceOrientation) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if !firstWillAppear {
            let transform = cameraOrientation.transform()
            
            var newVideoDurationLabelText = ""
            var newVideoDurationLabelLines = 0
            if let videoDurationLabelText = videoDurationLabel?.text {
                let info = convertVideoDurationLabelText(videoDurationLabelText)
                newVideoDurationLabelText = info.text
                newVideoDurationLabelLines = info.numberOfLines
            }
            
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
                self.statusLabel?.transform = transform
                self.selfTimerLabel?.transform = transform
                self.settingsIconImageView.transform = transform
                self.cameraIconImageView.transform = transform
                self.cameraButtonView.transform = transform
                self.thumbImageView.transform = transform
                self.videoDurationLabel?.transform = transform
                self.videoDurationLabel?.text = newVideoDurationLabelText
                self.videoDurationLabel?.numberOfLines = newVideoDurationLabelLines
                self.voiceButtonImageView.transform = transform
            }, completion: { (completed: Bool) -> Void in
            })
        }
    }
    
    func setStatusBar(deviceOrientation: ACDeviceOrientation) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if !firstWillAppear {
            isStatusBarHidden = deviceOrientation.isStatusBarHidden(view: view)
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    func convertVideoDurationLabelText(_ string: String) -> (text: String, numberOfLines: Int) {
        // convert
        // "hh:mm:ss" to "hhh\nmmm\nsss"
        // "hhh\nmmm\nsss" to "hh:mm:ss"
        
        var newString = ""
        var numberOfLines = 0
        
        let colonComponents = string.components(separatedBy: ":")
        if colonComponents.count == 3 {
            newString = colonComponents[0] + "h\n" + colonComponents[1] + "m\n" + colonComponents[2] + "s"
            numberOfLines = 3
            
        } else {
            let returnComponents = string.components(separatedBy: "\n")
            if returnComponents.count == 3 {
                newString = returnComponents[0].dropLast() + ":" + returnComponents[1].dropLast() + ":" + returnComponents[2].dropLast()
                numberOfLines = 1
            }
        }
        
        return (newString, numberOfLines)
    }
    
    func setupPreview() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if previewLayer == nil {
            previewLayer = AVCaptureVideoPreviewLayer(session: camera.captureSession)
            previewLayer.frame = camPreviewSubview.bounds
            //previewLayer.videoGravity = AVLayerVideoGravity.resizeAspect
            previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            camPreviewSubview.layer.addSublayer(previewLayer)
        }
    }
    
    func setMode(_ newMode: CameraMode){
        print("=== \(file).\(#function) === \(debugDate())")
        if newMode != cameraMode {
            cameraMode = newMode
            setCameraDisplay()
        }
    }
    
    func setCameraDisplay() {
        print("=== \(file).\(#function) === \(debugDate()) - cameraMode: \(cameraMode), isCameraAvailable: \(camera.isCameraAvailable)")
        
        // set aspect ratio
        switch cameraMode {
        case .photo:
            stopRecordingVideo()
            camera.camPreviewAspectRatio = camera.photoAspectRatio
            if let image = UIImage(named: "Camera") {
                cameraIconImageView.image = image.withRenderingMode(.alwaysTemplate)
            }
        case .video:
            camera.camPreviewAspectRatio = camera.videoAspectRatio
            if let image = UIImage(named: "VideoCamera") {
                cameraIconImageView.image = image.withRenderingMode(.alwaysTemplate)
            }
        }
        
        // set button
        if camera.isCameraAvailable {
            cameraIconImageView.tintColor = .lightGray
            switch cameraMode {
            case .photo:
                cameraButtonView.setState(.photo)
            case .video:
                cameraButtonView.setState(.video)
            }
        } else {
            cameraIconImageView.tintColor = Colors.inactive
            cameraButtonView.setState(.inactive)
        }
        
        // set interaction, etc
        if camera.isCameraAvailable {
            if camera.setupSession() {
                setupPreview()
                camera.startSession()
            }
            camPreview.backgroundColor = .black
            //camPreviewSubview.backgroundColor = .clear
            if statusLabel != nil {
                statusLabel.isHidden = true
            }
            //cameraButtonView.isUserInteractionEnabled = true
            
        } else {
            camPreview.backgroundColor = Colors.inactive
            //camPreviewSubview.backgroundColor = .gray
            if statusLabel == nil {
                createStatusLabel()
            }
            setStandardStatusLabel()
            statusLabel.isHidden = false
            //cameraButtonView.isUserInteractionEnabled = false
        }
        
        view.layoutIfNeeded()
    }
    
    func setSpeechDisplay() {
        print("=== \(file).\(#function) === \(debugDate()) - isSpeechAvailable: \(isSpeechAvailable)")
        
        if isSpeechAvailable {
            voiceButtonImageView.isUserInteractionEnabled = true
            
            if isRecognizingSpeech {
                if let image = UIImage(named: "MicrophoneOn") {
                    voiceButtonImageView.image = image
                }
            } else {
                if let image = UIImage(named: "Microphone") {
                    voiceButtonImageView.image = image.withRenderingMode(.alwaysTemplate)
                    voiceButtonImageView.tintColor = .lightGray
                }
            }
            
        } else {
            voiceButtonImageView.isUserInteractionEnabled = false
            if let image = UIImage(named: "Microphone") {
                voiceButtonImageView.image = image.withRenderingMode(.alwaysTemplate)
                voiceButtonImageView.tintColor = Colors.inactive
            }
        }
    }
    
    //MARK:- Actions
    
    func prepToShowOtherVC() {
        //print("=== \(file).\(#function) === \(debugDate())")
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation()), deviceOrientation: \(camera.deviceOrientation)  \(orientationOnLaunch)")
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.supportedInterfaceOrientations = [.all]
        }
        
        UIDevice.current.setValue(camera.deviceOrientation.uiInterfaceOrientation().rawValue, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
    }
    
    @objc func showSettings() {
        //print("=== \(file).\(#function) === \(debugDate())")
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation()), deviceOrientation: \(camera.deviceOrientation)  \(orientationOnLaunch)")
        
        doCheckSettingsInUse = true
        
        prepToShowOtherVC()
        
        if settingsViewController == nil {
            settingsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            settingsViewController.coreDataStack = coreDataStack
        }
        
        settingsViewController.camera = camera
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.pushViewController(settingsViewController, animated: true)
    }
    
    @objc func showLibrary() {
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation())")
        
        if cameraMode == .video {
            camera.stopRecording()
        }
        
        prepToShowOtherVC()
        
        if photoLibraryViewController == nil {
            photoLibraryViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotoLibraryViewController") as! PhotoLibraryViewController
            photoLibraryViewController.photoLibraryHelper = camera.photoLibraryHelper
            photoLibraryViewController.photoViewController = photoViewController
        }
        
        photoLibraryViewController.doHandleWillAppear = true
        photoLibraryViewController.updateAssets = true
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.pushViewController(photoLibraryViewController, animated: true)
        
        /*
         navigationController?.setNavigationBarHidden(false, animated: false)
         navigationController?.pushViewController(photoLibraryViewController, animated: false)
         */
    }
    
    @objc func toggleCameraMode() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        showFirstCameraGestureAlert()
        
        if cameraMode == .photo {
            cameraMode = .video
        } else {
            cameraMode = .photo
        }
    }
    
    @objc func toggleFrontBackCamera(_ gestureRecognizer: UIGestureRecognizer) {
        print("=== \(file).\(#function) === \(debugDate())")
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        
        if longPress.state == .began {
            showFirstCameraGestureAlert()
            camera.toggleFrontBackCamera()
        }
    }
    
    func showFirstCameraGestureAlert() {
        //print("=== \(file).\(#function) === \(debugDate())")
        
        if camera.isBothCamerasAvailable {
            let userDefaults = UserDefaults.standard
            let firstCameraGestureAlertWasShown = userDefaults.bool(forKey: "FirstCameraGestureAlertWasShown")
            
            if !firstCameraGestureAlertWasShown {
                userDefaults.set(true, forKey: "FirstCameraGestureAlertWasShown")
                userDefaults.synchronize()
                showAlert(title: "Camera Icon", message: "A tap, or short press, will toggle photo/video capture.\n\nA long press will toggle the front/back camera.", okTitle: "Got It")
            }
        }
    }
    
    @objc func applicationWillEnterForeground() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        handleWillAppear()
        photoLibraryViewController?.handleWillAppear()
    }
    
    @objc func applicationDidBecomeActive() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if firstWillAppear {
            firstWillAppear = false
            
            afterDelay(0.5) {
                self.camera.deviceOrientation = self.orientationOnLaunch
                self.isStatusBarHidden = self.camera.deviceOrientation.isStatusBarHidden(view: self.view)
                self.setNeedsStatusBarAppearanceUpdate()
            }
            
        } else {
            handleWillAppear()
            photoLibraryViewController?.handleWillAppear()
        }
    }
    
    func handleWillAppear() {
        print("--- --- --- --- --- --- --- --- --- --- ---")
        //print("=== \(file).\(#function) === \(debugDate())")
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation()), deviceOrientation: \(camera.deviceOrientation) \(orientationOnLaunch)")
        print("--- --- --- --- --- --- --- --- --- --- ---")
        
        camera.startMotionManager()
        
        print("=== \(file).\(#function) === \(debugDate()) - isCameraAvailable: \(camera.isCameraAvailable)")
        
        requestAuthorizations()
        
        if doCheckSettingsInUse {
            print("--- \(file).\(#function) - doCheckSettingsInUse")
            doCheckSettingsInUse = false
            camera.checkSettingsInUse()
        }
        
        if camera.isPhotoLibraryAvailable && displayPreviewOnWillAppear {
            displayPreview()
            displayPreviewOnWillAppear = true
            
        } else if !camera.isPhotoLibraryAvailable && photoLibraryViewController != nil {
            print("--- \(file).\(#function) - !isPhotoLibraryAvailable and photoLibraryViewController is defined")
            
            if photoViewController != nil {
                print("--- \(file).\(#function) - !isPhotoLibraryAvailable and photoViewController is defined")
                if let _ = navigationController?.topViewController as? PhotoViewController  {
                    print("--- \(file).\(#function) - photoViewController is topViewController")
                    navigationController?.popViewController(animated: false)
                }
                photoLibraryViewController.photoViewController = nil
                photoViewController = nil
            }
            
            if let _ = navigationController?.topViewController as? PhotoLibraryViewController  {
                print("--- \(file).\(#function) - photoLibraryViewController is topViewController")
                navigationController?.popViewController(animated: false)
            }
            photoLibraryViewController = nil
        }
        
        if isSpeechAvailable && wasRecognizingSpeech {
            startRecognizeSpeech()
        }
    }
    
    func handleWillDisappear() {
        camera.stopMotionManager()
    }
    
    @objc func applicationWillResignActive() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if cameraMode == .video {
            camera.stopRecording()
        }
        
        if isRecognizingSpeech {
            stopRecognizeSpeech()
        }
        
        handleWillDisappear()
    }
    
    @objc func toggleSpeechRecognition() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if !isRecognizingSpeech {
            if notConnectedToNetwork(message: "You must be connected to a network in order to use speech recognition.") {
                return
            }
            startRecognizeSpeech()
        } else {
            wasRecognizingSpeech = false
            stopRecognizeSpeech()
        }
    }
    
    func showAlert(title: String?, message: String?, okTitle: String = "OK") {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Setup Camera
    
    func createStatusLabel() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        statusLabel = UILabel()
        setStandardStatusLabel()
        statusLabel.numberOfLines = 0
        statusLabel.textAlignment = .center
        statusLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
        statusLabel.adjustsFontForContentSizeCategory = true
        statusLabel.sizeToFit()
        statusLabel.transform = camera.deviceOrientation.transform()
        
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        camPreviewSubview.addSubview(statusLabel)
        
        statusLabel.centerXAnchor.constraint(equalTo: camPreviewSubview.centerXAnchor).isActive = true
        statusLabel.centerYAnchor.constraint(equalTo: camPreviewSubview.centerYAnchor).isActive = true
    }
    
    func setStandardStatusLabel() {
        print("=== \(file).\(#function) === \(debugDate())")
        statusLabel.text = "The camera\nis not accessible."
        statusLabel.textColor = Colors.lighterGray
    }
    
    func createSelfTimerLabel() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        selfTimerLabelAttributes = [
            .strokeColor: UIColor.darkGray,
            .foregroundColor: Colors.lightLightGray,
            .strokeWidth: -1.0,
            .font: UIFont.boldSystemFont(ofSize: 200)
        ]
        
        selfTimerLabel = UILabel()
        setSelfTimerLabel(string: "99")
        selfTimerLabel.textAlignment = .center
        selfTimerLabel.isHidden = true
        selfTimerLabel.transform = camera.deviceOrientation.transform()
        
        selfTimerLabel.translatesAutoresizingMaskIntoConstraints = false
        camPreviewSubview.addSubview(selfTimerLabel)
        
        selfTimerLabel.centerXAnchor.constraint(equalTo: camPreviewSubview.centerXAnchor).isActive = true
        selfTimerLabel.centerYAnchor.constraint(equalTo: camPreviewSubview.centerYAnchor).isActive = true
    }
    
    func setSelfTimerLabel(string: String) {
        if selfTimerLabel == nil {
            createSelfTimerLabel()
        }
        selfTimerLabel.attributedText = NSAttributedString(string: string, attributes: selfTimerLabelAttributes)
        selfTimerLabel.isHidden = false
    }
    
    func showSelfTimerLabel(string: String) {
        setSelfTimerLabel(string: string)
        
        selfTimerLabel.alpha = 0.8
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.selfTimerLabel.alpha = 0
        }, completion: { (completed: Bool) -> Void in
        })
    }
    
    //MARK:- Camera
    
    /*
     func takePicture() {
     print("=== \(file).\(#function) === \(debugDate())")
     
     stopRecordingVideo()
     
     camera.takePicture()
     }
     */
    
    func startRecordingVideo() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if camera.isCameraAvailable {
            camera.startRecording()
        } else {
            if statusLabel == nil {
                createStatusLabel()
            }
            statusLabel.isHidden = false
            statusLabel.text = "Recording\n(The camera\nis not accessible.)"
            statusLabel.textColor = .red
        }
    }
    
    func stopRecordingVideo() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if camera.isCameraAvailable {
            camera.stopRecording()
        } else {
            if statusLabel == nil {
                createStatusLabel()
            }
            setStandardStatusLabel()
        }
    }
    
    //MARK:- Speech Recognition
    
    func requestSpeechAuthorization() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        SFSpeechRecognizer.requestAuthorization { authStatus in
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.isSpeechAvailable = true
                    print("--- \(self.file).\(#function) - authorized")
                case .denied:
                    self.isSpeechAvailable = false
                    print("--- \(self.file).\(#function) - denied")
                case .restricted:
                    self.isSpeechAvailable = false
                    print("--- \(self.file).\(#function) - restricted")
                case .notDetermined:
                    self.isSpeechAvailable = false
                    print("--- \(self.file).\(#function) - notDetermined")
                }
            }
        }
    }
    
    func handleDidSetIsSpeechAvailable() {
        print("=== \(file).\(#function) === \(debugDate()) - isSpeechAvailableSet/isSpeechAvailable: \(isSpeechAvailableSet) / \(isSpeechAvailable)")
        
        if isSpeechAvailable == isSpeechAvailableSet {
            return
        }
        
        isSpeechAvailableSet = isSpeechAvailable
        
        setSpeechDisplay()
    }
    
    func startRecognizeSpeech() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let node = audioEngine.inputNode
        
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            print("Error starting audioEngine: \(error.localizedDescription)")
            isRecognizingSpeech = false
            wasRecognizingSpeech = false
            audioEngine.inputNode.removeTap(onBus: 0)
            showAlert(title: "Error", message: "Could not start speech recognition due to a problem with the audio engine.")
            return
        }
        
        guard let myRecognizer = SFSpeechRecognizer() else {
            print("A speech recognizer is not supported for the current locale")
            isRecognizingSpeech = false
            wasRecognizingSpeech = false
            audioEngine.stop()
            audioEngine.inputNode.removeTap(onBus: 0)
            showAlert(title: "Error", message: "A speech recognizer is not supported for the current locale.")
            return
        }
        
        if !myRecognizer.isAvailable {
            print("A recognizer is not available right now")
            isRecognizingSpeech = false
            wasRecognizingSpeech = false
            audioEngine.stop()
            audioEngine.inputNode.removeTap(onBus: 0)
            showAlert(title: "Error", message: "A speech recognizer is not available right now.")
            return
        }
        
        isRecognizingSpeech = true
        
        speechRecognizer?.delegate = self
        
        var isFinalString = ""
        
        mostRecentlyProcessedSegmentDuration = 0
        
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {
                let transcription = result.bestTranscription
                let bestString = transcription.formattedString
                if result.isFinal {
                    isFinalString = " isFinal --- ---"
                    print("--- bestString --- ---\(isFinalString)\n\(bestString)\n--- --- --- ---")
                    self.stopRecognizeSpeech(resume: true)
                } else {
                    isFinalString = ""
                }
                print("--- bestString --- ---\(isFinalString)\n\(bestString)\n--- --- --- ---")
                
                if let lastSegment = transcription.segments.last,
                    lastSegment.duration > self.mostRecentlyProcessedSegmentDuration {
                    self.mostRecentlyProcessedSegmentDuration = lastSegment.duration
                    print("--- lastSegment.substring: '\(lastSegment.substring)'")
                    self.cameraOperation.testWord(lastSegment.substring)
                }
            } else if let error = error {
                print("Error recognizing audio: \(error.localizedDescription)")
            }
        })
    }
    
    func stopRecognizeSpeech(resume: Bool = false) {
        print("=== \(file).\(#function) === \(debugDate())")
        isRecognizingSpeech = resume
        recognitionTask?.cancel()
        request.endAudio()
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
        if resume {
            afterDelay(0.5) {
                self.startRecognizeSpeech()
            }
        }
    }
    
    //MARK:- Thumbnail Preview
    
    func displayPreview() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        camera.photoLibraryHelper.getAssets()
        
        camera.photoLibraryHelper.getImage(at: 0, imageSize: previewImageSize) { image, mediaType in
            if let image = image {
                self.thumbImageView.image = image
                self.thumbImageView.mediaType = mediaType
                print("--- \(self.file).\(#function) - completionHandler: image size \(image.size)")
            } else {
                self.thumbImageView.image = nil
                self.thumbImageView.mediaType = .unknown
            }
        }
    }
}

// MARK: - CameraDelegate

extension CameraViewController: CameraDelegate {
    
    func cameraOrientationDidChange(_ cameraOrientation: ACDeviceOrientation) {
        print("=== \(file).\(#function) === \(debugDate())")
        setButtonTransforms(cameraOrientation: cameraOrientation)
    }
    
    func deviceOrientationDidChange(_ deviceOrientation: ACDeviceOrientation) {
        print("=== \(file).\(#function) === \(debugDate())")
        setStatusBar(deviceOrientation: deviceOrientation)
    }
    
    func isPhotoLibraryAvailableDidChange(_ isPhotoLibraryAvailable: Bool) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        thumbImageView.isUserInteractionEnabled = isPhotoLibraryAvailable
        
        if isPhotoLibraryAvailable {
            displayPreview()
            photoLibraryViewController?.displayPhotosAndVideos()
        }
    }
    
    func isCameraAvailableDidChange(_ isCameraAvailable: Bool) {
        print("=== \(file).\(#function) === \(debugDate())")
        setCameraDisplay()
        camera.checkCamerasAvailable()
        
        recognizeLongPressOnCameraIcon = camera.isBothCamerasAvailable
    }
    
    func photoAspectRatioDidSet(_ photoAspectRatio: CGFloat) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if notInViewDidLoad {
            if cameraMode == .photo {
                setCameraDisplay()
            }
        }
    }
    
    func camPreviewAspectRatioDidSet(_ camPreviewAspectRatio: CGFloat) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if notInViewDidLoad {
            setCamPreviewConstraints()
        }
    }
    
    func cameraSelfTimerStateDidChange() {
        print("=== \(file).\(#function) === \(debugDate())")
        if cameraMode == .photo {
            cameraButtonView.setState(.photo)
        }
    }
    
    func selfTimerIsRunning(duration: Int, timeLeft: Int) {
        print("=== \(file).\(#function) === \(debugDate())")
        print("--- \(file).\(#function) --- \(debugDate()) - duration: \(duration), timeLeft: \(timeLeft)")
        showSelfTimerLabel(string: String(timeLeft))
    }
    
    func takingPicture() {
        print("=== \(file).\(#function) === \(debugDate())")
        selfTimerLabel?.isHidden = true
        
        // simulate shutter
        let alpha: CGFloat = 0
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.camPreviewSubview.alpha = alpha
        }, completion: { (completed: Bool) -> Void in
            let alpha: CGFloat = 1
            UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
                self.camPreviewSubview.alpha = alpha
            }, completion: { (completed: Bool) -> Void in
            })
        })
    }
    
    func savedImage(_ image: UIImage) {
        print("=== \(file).\(#function) === \(debugDate())")
        print("--- \(self.file).\(#function) - image size \(image.size)")
        
        thumbImageView.image = image
        thumbImageView.mediaType = .image
        photoLibraryViewController?.updateAssets = true
    }
    
    func startedRecording() {
        print("=== \(file).\(#function) === \(debugDate())")
        statusLabel.text = "Recording"
        statusLabel.textColor = .red
        cameraButtonView.setState(.videoRunning)
        
        if videoPhotoButtonView == nil {
            videoPhotoButtonView = CameraButtonView(frame: CGRect.zero)
            videoPhotoButtonView.camera = camera
            videoPhotoButtonView.isForPhotoInVideo = true
            videoPhotoButtonView.cameraButtonState = .photo
            
            videoPhotoButtonView.translatesAutoresizingMaskIntoConstraints = false
            leftBottomView.addSubview(videoPhotoButtonView)
            
            videoPhotoButtonView.topAnchor.constraint(equalTo: leftBottomView.topAnchor, constant: 10).isActive = true
            videoPhotoButtonView.bottomAnchor.constraint(equalTo: leftBottomView.bottomAnchor, constant: -10).isActive = true
            videoPhotoButtonView.centerXAnchor.constraint(equalTo: leftBottomView.centerXAnchor).isActive = true
            videoPhotoButtonView.widthAnchor.constraint(equalTo: videoPhotoButtonView.heightAnchor, multiplier: 1).isActive = true
        }
        
        if videoDurationLabel == nil {
            videoDurationLabel = UILabel()
            videoDurationLabel.textColor = Colors.lightLightGray
            videoDurationLabel.textAlignment = .left
            videoDurationLabel.font = videoDurationLabelFont()
            videoDurationLabel.transform = camera.deviceOrientation.transform()
            
            videoDurationLabel.translatesAutoresizingMaskIntoConstraints = false
            rightBottomView.addSubview(videoDurationLabel)
            
            videoDurationLabel.centerXAnchor.constraint(equalTo: rightBottomView.centerXAnchor).isActive = true
            videoDurationLabel.centerYAnchor.constraint(equalTo: rightBottomView.centerYAnchor).isActive = true
        }
        
        recordingDurationIs(0)
        
        cameraIconImageView.isHidden = true
        thumbImageView.isHidden = true
        
        videoPhotoButtonView.isHidden = false
        videoDurationLabel.isHidden = false
    }
    
    func videoDurationLabelFont() -> UIFont {
        let bodyFont = UIFont.preferredFont(forTextStyle: .body)
        let fontSize = min(24, bodyFont.pointSize)
        print("=== \(file).\(#function) === \(debugDate()), fontSize: \(fontSize)")
        return UIFont.monospacedDigitSystemFont(ofSize: fontSize, weight: .regular)
    }
    
    func recordingDurationIs(_ duration: Float) {
        //print("=== \(file).\(#function) === \(debugDate()) - duration: \(duration)")
        
        let h: Int = Int(duration.truncatingRemainder(dividingBy: 86400) / 3600)
        let m: Int = Int(duration.truncatingRemainder(dividingBy: 3600) / 60)
        let s: Int = Int(duration.truncatingRemainder(dividingBy: 60))
        
        var formattedString = ""
        
        if camera.deviceOrientation.isKindOfPortrait {
            formattedString = String(format: "%02i:%02i:%02i", h, m, s) // "hh:mm:ss"
            videoDurationLabel.numberOfLines = 1
            
        } else {
            formattedString = String(format: "%02ih\n%02im\n%02is", h, m, s) // "hhh\nmmm\nsss"
            videoDurationLabel.numberOfLines = 3
        }
        print("=== \(file).\(#function) === \(debugDate()) - duration: \(duration), formatted: \(formattedString)")
        videoDurationLabel.text = formattedString
    }
    
    func stoppedRecording() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if cameraMode == .video {
            cameraButtonView.setState(.video)
        }
        
        cameraIconImageView.isHidden = false
        thumbImageView.isHidden = false
        
        videoPhotoButtonView?.isHidden = true
        videoDurationLabel?.isHidden = true
    }
    
    func videoSavedToLibrary(id: String) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let assets = PHAsset.fetchAssets(withLocalIdentifiers: [id], options: nil)
        
        camera.photoLibraryHelper.getImage(asset: assets[0], imageSize: self.previewImageSize) { image, mediaType in
            OperationQueue.main.addOperation {
                if let image = image {
                    self.thumbImageView.image = image
                    self.thumbImageView.mediaType = mediaType
                    print("--- \(self.file).\(#function) - completionHandler: image size \(image.size)")
                } else {
                    self.thumbImageView.image = nil
                    self.thumbImageView.mediaType = .unknown
                }
            }
        }
    }
    
    func handleAssetWasDeleted() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if camera.isPhotoLibraryAvailable {
            displayPreviewOnWillAppear = true
        }
    }
    
    func showCameraAlert(title: String?, message: String?) {
        print("=== \(file).\(#function) === \(debugDate())")
        showAlert(title: title, message: message)
    }
}

// MARK: - CameraButtonViewDelegate

extension CameraViewController: CameraButtonViewDelegate {
    
    func didTapOnCameraButtonView(_ view: CameraButtonView) {
        print("=== \(file).\(#function) === \(debugDate())")
    }
}

// MARK: - SFSpeechRecognizerDelegate

extension CameraViewController: SFSpeechRecognizerDelegate {
    
    func speechRecognitionDidDetectSpeech(_ task: SFSpeechRecognitionTask) {
        print("=== \(file).\(#function) === \(debugDate())")
    }
    
    func speechRecognitionTaskFinishedReadingAudio(_ task: SFSpeechRecognitionTask) {
        print("=== \(file).\(#function) === \(debugDate())")
    }
}

// MARK: - CameraOperationDelegate

extension CameraViewController: CameraOperationDelegate {
    
    func operateCamera(_ cameraAction: CameraAction) {
        print("=== \(file).\(#function) === \(debugDate()) - cameraAction: \(cameraAction)")
        
        switch cameraAction {
        case .setPhoto:
            setMode(.photo)
            
        case .takePhoto:
            //setMode(.photo)
            camera.takePicture(isForPhotoInVideo: cameraMode == .video)
            
        case .setVideo:
            setMode(.video)
            
        case .startVideo:
            setMode(.video)
            camera.startRecording()
            
        case .stopVideo:
            camera.stopRecording()
            
        case .startSpeech:
            startRecognizeSpeech()
            
        case .stopSpeech:
            stopRecognizeSpeech()
        }
    }
}
