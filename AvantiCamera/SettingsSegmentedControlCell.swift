//
//  SettingsSegmentedControlCell.swift
//  AvantiCamera
//
//  Created by James Hager on 3/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

class SettingsSegmentedControlCell: UITableViewCell {
    
    let userDefaults = UserDefaults.standard
    var segmentedControlName = ""
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var theSegmentedControl: UISegmentedControl!
    
    @IBAction func theSegmentedControlChanged(_ sender: UISegmentedControl) {
        print("=== theSegmentedControlChanged - segmentedControlName=\(segmentedControlName)")
        userDefaults.set(sender.selectedSegmentIndex, forKey: segmentedControlName)
        userDefaults.synchronize()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String, segmentedControlName: String, values: [String], segmentWidth: CGFloat?, enabled: Bool = true) {
        selectionStyle = UITableViewCellSelectionStyle.none
        self.segmentedControlName = segmentedControlName
        
        print("=== configureCell - text=\(text)")
        print("--- configureCell - segmentedControlName=\(segmentedControlName)")
        print("--- configureCell - values=\(values)")
        
        label.text = text
        
        //let valueHelper = SettingsValueHelper(valueName: segmentedControlName, values: values!)
        
        theSegmentedControl.removeAllSegments()
        
        var index = 0
        for value in values {
            theSegmentedControl.insertSegment(withTitle: value, at: index, animated: false)
            if let segmentWidth = segmentWidth {
                theSegmentedControl.setWidth(segmentWidth, forSegmentAt: index)
            }
            index += 1
        }
        
        theSegmentedControl.selectedSegmentIndex = userDefaults.integer(forKey: segmentedControlName)
        theSegmentedControl.isEnabled = enabled
        theSegmentedControl.tintColor = Colors.switchOn
    }
}
