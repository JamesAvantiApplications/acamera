
import UIKit
import StoreKit

class SettingsTextBuyRestoreCell: UITableViewCell {
    
    var product: SKProduct!
    var viewController: UIViewController!
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var restoreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String, descriptionText: String, product: SKProduct?, viewController: UIViewController, doAdjustFonts: Bool = false) {
        self.viewController = viewController
        label.text = text
        
        if descriptionText == "" {
            descriptionLabel.isHidden = true
        } else {
            descriptionLabel.isHidden = false
            descriptionLabel.text = descriptionText
        }
        
        if product != nil {
            self.product = product
        }
        
        /*
        buyButton.addTarget(self, action: #selector(SettingsTextBuyRestoreCell.doBuy), for: UIControlEvents.touchUpInside)
        restoreButton.addTarget(self, action: #selector(SettingsTextBuyRestoreCell.doRestore), for: UIControlEvents.touchUpInside)
        */
        
        if doAdjustFonts {
            let font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            buyButton.titleLabel!.font = font
            restoreButton.titleLabel!.font = font
        }
    }
    
    /*
    func doBuy() {
        if IAPHelper.canMakePayments() {
            if let _ = product {
                print("=== doBuy - The Buy button has been pressed (in SettingsTextBuyRestoreCell) - product=\(product.localizedTitle) +++")
                IAPProducts.store.purchaseProduct(product)
            } else {
                print("=== doBuy - The Buy button has been pressed (in SettingsTextBuyRestoreCell) - no product +++")
                displayNoProduct()
            }
        } else {
            print("=== doBuy - The Buy button has been pressed (in SettingsTextBuyRestoreCell) - cannot make payments +++")
            displayCannotMakePayments()
        }
    }
    
    func doRestore() {
        print("=== doRestore - The Restore button has been pressed (in SettingsTextBuyRestoreCell) +++")
        if let _ = product {
            IAPProducts.store.restoreCompletedTransactions()
            displayRestoring()
        } else {
            displayNoProduct(restore: true)
        }
    }
    
    func displayCannotMakePayments() {
        print("=== displayCannotMakePayments")
        let alert = UIAlertController(title: "You Cannot Make Payments",
            message: "You must allow your device to make payments in order to purchase the upgrade.",
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        self.viewController.present(alert, animated: true, completion: nil)
    }
    
    func displayNoProduct(restore: Bool = false) {
        print("=== displayNoProduct")
        
        var message: String!
        
        if restore {
            if !connectedToNetwork() {
                message = "The app cannot connect to the iTunes store because you are not connected to the internet.\n\nAfter connecting to the internet, tap the Restore button after the price of the upgrade is displayed."
            } else {
                message = "The app cannot connect to the iTunes store. Please tap the Restore button again after the price of the upgrade is displayed."
            }
        } else {
            if !connectedToNetwork() {
                message = "The app is waiting for iTunes to prepare for the purchase, but you are not connected to the internet.\n\nAfter connecting to the internet, tap the Restore button after the price of the upgrade is displayed."
            } else {
                message = "The app is waiting for iTunes to prepare for the purchase. Please tap the Buy button again after the price of the upgrade is displayed."
            }
        }
        
        let alert = UIAlertController(title: "Waiting For iTunes", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        self.viewController.present(alert, animated: true, completion: nil)
    }
    
    func displayRestoring() {
        print("=== displayRestoring")
        let alert = UIAlertController(title: "Restoring Purchases",
            message: "The iTunes Store has been contacted to restore your purchases.",
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        self.viewController.present(alert, animated: true, completion: nil)
    }
    */
}
