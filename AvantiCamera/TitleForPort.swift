//
//  TitleForPort.swift
//  AvantiCamera
//
//  Created by James Hager on 3/31/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

class TitleForPort: UIView {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    func setTitle(date: Date, dateFormatter: DateFormatter, timeFormatter: DateFormatter) {
        dateLabel.text = dateFormatter.string(from: date)
        timeLabel.text = timeFormatter.string(from: date)
    }
}
