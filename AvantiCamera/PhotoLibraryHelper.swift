//
//  PhotoLibraryHelper.swift
//  AvantiCamera
//
//  Created by James Hager on 3/25/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import Photos

public protocol PhotoLibraryHelperDelegate {
    func handleDidSetIsPhotoLibraryAvailable(_ isPhotoLibraryAvailable: Bool)
    func handleAssetWasDeleted()
}

// MARK: -

class PhotoLibraryHelper {
    
    var delegate: PhotoLibraryHelperDelegate!
    
    var isPhotoLibraryAvailable = false {
        didSet {
            delegate?.handleDidSetIsPhotoLibraryAvailable(isPhotoLibraryAvailable)
        }
    }
    
    var photoAssets: PHFetchResult<PHAsset>!
    
    let imageManager = PHCachingImageManager()
    
    lazy var cameraSettings = CameraSettings()
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    func requestLibraryAuthorization() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        PHPhotoLibrary.requestAuthorization({status in
            OperationQueue.main.addOperation {
                if status == .authorized {
                    self.isPhotoLibraryAvailable = true
                } else {
                    self.isPhotoLibraryAvailable = false
                }
            }
        })
    }
    
    func deleteAssets(_ assets: [PHAsset], completionHandler: ((Bool, Error?) -> Void)? = nil) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.deleteAssets(assets as NSArray)
        }) { success, error in
            if error != nil {
                print("Error deleting image: \(error!.localizedDescription)")
            }
            if success {
                self.delegate?.handleAssetWasDeleted()
                print("--- \(self.file).\(#function) - asset deleted from library")
            }
            if let completionHandler = completionHandler {
                completionHandler(success, error)
            }
        }
    }
    
    func getAssets() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d || mediaType = %d", PHAssetMediaType.image.rawValue, PHAssetMediaType.video.rawValue)
        
        photoAssets = PHAsset.fetchAssets(with: fetchOptions)
        
        print("--- \(file).\(#function) - photoAssets.count=\(photoAssets.count)")
    }
    
    func getAsset(at index: Int) -> PHAsset? {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if index > -1 && index < photoAssets.count {
            return photoAssets[index]
        } else {
            return nil
        }
    }
    
    func getDateForAsset(at index: Int) -> Date? {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if let asset = getAsset(at: index) {
            return asset.creationDate
        } else {
            return nil
        }
    }
    
    func getImage(asset: PHAsset, imageSize: CGSize?, completionHandler: ((UIImage?, PHAssetMediaType) -> Void)? = nil) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        var imageToReturn: UIImage?
        
        var mediaType: PHAssetMediaType = .unknown
        
        var imageSizeToUse: CGSize!
        
        if let imageSize = imageSize {
            imageSizeToUse = imageSize
        } else {
            imageSizeToUse = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
        }
        
        let options = PHImageRequestOptions()
        options.deliveryMode = .highQualityFormat
        options.resizeMode = .none //.exact
        options.isSynchronous = true
        
        imageManager.requestImage(for: asset, targetSize: imageSizeToUse, contentMode: .aspectFill, options: options) { image, info in
            if let image = image {
                imageToReturn = image
                mediaType = asset.mediaType
            }
            if let completionHandler = completionHandler {
                completionHandler(imageToReturn, mediaType)
            }
        }
    }
    
    func getImage(at index: Int, imageSize: CGSize?, completionHandler: ((UIImage?, PHAssetMediaType) -> Void)? = nil) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        var imageToReturn: UIImage?
        
        var mediaType: PHAssetMediaType = .unknown
        
        if let asset = getAsset(at: index) {
            
            var imageSizeToUse: CGSize!
            
            if let imageSize = imageSize {
                imageSizeToUse = imageSize
            } else {
                imageSizeToUse = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
            }
            //print("--- \(file).\(#function) - ....... imageSizeToUse: \(imageSizeToUse!)")
            
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            
            imageManager.requestImage(for: asset, targetSize: imageSizeToUse, contentMode: .aspectFit, options: options) { image, info in
                if let image = image {
                    imageToReturn = image
                    mediaType = asset.mediaType
                    print("--- \(self.file).\(#function) - completion: image.size: \(image.size)")
                }
                if let completionHandler = completionHandler {
                    completionHandler(imageToReturn, mediaType)
                }
            }
            
        } else {
            if let completionHandler = completionHandler {
                completionHandler(imageToReturn, mediaType)
            }
        }
    }
    
    func getImageSize(at index: Int, targetSize: CGSize?, completionHandler: ((CGSize?) -> Void)? = nil) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        var imageSize: CGSize?
        
        if let asset = getAsset(at: index) {
            
            var targetSizeToUse: CGSize!
            
            if let targetSize = targetSize {
                targetSizeToUse = targetSize
            } else {
                targetSizeToUse = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
            }
            //print("--- \(file).\(#function) - ....... targetSizeToUse: \(targetSizeToUse!)")
            
            let options = PHImageRequestOptions()
            options.deliveryMode = .fastFormat
            options.resizeMode = .fast
            //options.isSynchronous = false
            options.isSynchronous = true
            
            imageManager.requestImage(for: asset, targetSize: targetSizeToUse, contentMode: .aspectFit, options: options) { image, info in
                if let image = image {
                    let imageSizeRetrieved = image.size
                    
                    let maxWidth = targetSizeToUse.width
                    let maxHeight = targetSizeToUse.height
                    
                    let actualWidth = imageSizeRetrieved.width
                    let actualHeight = imageSizeRetrieved.height
                    
                    let scale = min(maxWidth/actualWidth, maxHeight/actualHeight)
                    let width = actualWidth * scale
                    let height = actualHeight * scale
                    
                    imageSize = CGSize(width: width, height: height)
                    print("--- \(self.file).\(#function) - completion: imageSize: \(imageSize!)")
                }
                if let completionHandler = completionHandler {
                    completionHandler(imageSize)
                }
            }
            
        } else {
            if let completionHandler = completionHandler {
                completionHandler(imageSize)
            }
        }
    }
    
    func getAVAsset(at index: Int, completionHandler: ((AVAsset?, AVAudioMix?, [AnyHashable : Any]?) -> Void)? = nil) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if let asset = getAsset(at: index) {
            imageManager.requestAVAsset(forVideo: asset, options: nil) { avAsset, avAudioMix, info in
                if avAsset != nil {
                    print("--- \(self.file).\(#function) - completion: avAsset is set")
                } else {
                    print("--- \(self.file).\(#function) - completion: avAsset is nil")
                }
                if let completionHandler = completionHandler {
                    completionHandler(avAsset, avAudioMix, info)
                }
            }
            
        } else {
            if let completionHandler = completionHandler {
                completionHandler(nil, nil, nil)
            }
        }
    }
    
    func getURL(at index: Int, completionHandler: ((URL?) -> Void)? = nil) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if let asset = getAsset(at: index) {
            asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { contentEditingInput, dictInfo in
                var url: URL?
                if asset.mediaType == .image {
                    if let imageURL = contentEditingInput?.fullSizeImageURL {
                        url = imageURL
                        print("IMAGE URL: ", imageURL)
                    }
                } else {
                    if let videoURL = (contentEditingInput!.audiovisualAsset as? AVURLAsset)?.url {
                        url = videoURL
                        print("VIDEO URL: ", videoURL)
                    }
                }
                if let completionHandler = completionHandler {
                    completionHandler(url)
                }
            }
            
        } else {
            if let completionHandler = completionHandler {
                completionHandler(nil)
            }
        }
    }
    
    func getInAppFileURLToShare(at index: Int, completionHandler: ((URL?) -> Void)? = nil) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        var url: URL?
        
        if let asset = getAsset(at: index) {
            if asset.mediaType == .image {
                let targetSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
                
                let options = PHImageRequestOptions()
                options.deliveryMode = .highQualityFormat
                options.resizeMode = .exact
                options.isSynchronous = true
                
                imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: options) { image, info in
                    if let image = image {
                        //print("--- \(self.file).\(#function) - completion: image.size: \(image.size)")
                        
                        // get original filename from asset URL
                        asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { contentEditingInput, dictInfo in
                            let userDefaults = UserDefaults.standard
                            let format = userDefaults.string(forKey: self.cameraSettings.photoFormatKey)!
                            var filename = "image." + format
                            if let imageURL = contentEditingInput?.fullSizeImageURL {
                                filename = self.getFilename(fromURL: imageURL, withExtension: format)
                                //print("--- \(self.file).\(#function) - completion: filename is \(filename)")
                            }
                            
                            // create data to output
                            var data: Data?
                            
                            if let ciImage = CIImage(image: image) {
                                
                                let photoColorSpace = userDefaults.string(forKey: self.cameraSettings.photoColorSpaceKey)!
                                var colorSpace: CGColorSpace!
                                if photoColorSpace == "adobe98" {
                                    colorSpace = CGColorSpace(name: CGColorSpace.adobeRGB1998)!
                                } else {
                                    colorSpace = CGColorSpace(name: CGColorSpace.sRGB)!
                                }
                                
                                if format == "png" {
                                    if let dataCreated = CIContext().pngRepresentation(of: ciImage, format: kCIFormatRGBA8, colorSpace: colorSpace, options: [:]) {
                                        data = dataCreated
                                    }
                                    
                                } else if format == "tif" {
                                    if let dataCreated = CIContext().tiffRepresentation(of: ciImage, format: kCIFormatRGBA8, colorSpace: colorSpace, options: [:]) {
                                        data = dataCreated
                                    }
                                    
                                } else {
                                    let imageQuality = userDefaults.double(forKey: self.cameraSettings.photoImageQualityKey)
                                    let options = [kCGImageDestinationLossyCompressionQuality : imageQuality]
                                    
                                    if format == "heif" {
                                        if let dataCreated = CIContext().heifRepresentation(of: ciImage, format: kCIFormatRGBA8, colorSpace: colorSpace, options: options) {
                                            data = dataCreated
                                        }
                                        
                                        /*
                                         url = self.documentsDirectory().appendingPathComponent(filename)
                                         
                                         do {
                                         try CIContext().writeHEIFRepresentation(of: ciImage, to: url!, format: kCIFormatRGBA8, colorSpace: colorSpace, options: options)
                                         if let creationDate = asset.creationDate {
                                         self.setCreationDate(date: creationDate, ofItemAt: url!)
                                         }
                                         } catch {
                                         url = nil
                                         print(error)
                                         }
                                         */
                                        
                                    } else if format == "jpg" {
                                        if let dataCreated = CIContext().jpegRepresentation(of: ciImage, colorSpace: colorSpace, options: options) {
                                            data = dataCreated
                                        }
                                    }
                                }
                            }
                            
                            /*
                             UIImageJPEGRepresentation(_ image: UIImage, _ compressionQuality: CGFloat) -> Data? // return image as JPEG. May return nil if image has no CGImageRef or invalid bitmap format. compression is 0(most)..1(least)
                             */
                            
                            // create output file
                            if let data = data {
                                url = self.documentsDirectory().appendingPathComponent(filename)
                                
                                do {
                                    try data.write(to: url!)
                                    if let creationDate = asset.creationDate {
                                        self.setCreationDate(date: creationDate, ofItemAt: url!)
                                    }
                                } catch {
                                    print(error)
                                }
                            }
                            if let completionHandler = completionHandler {
                                completionHandler(url)
                            }
                        }
                        
                    } else {
                        if let completionHandler = completionHandler {
                            completionHandler(url)
                        }
                    }
                }
                
            } else if asset.mediaType == .video {
                imageManager.requestAVAsset(forVideo: asset, options: nil) { avAsset, avAudioMix, info in
                    if let avAsset = avAsset {
                        if let exporter = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetHighestQuality) {
                            
                            let userDefaults = UserDefaults.standard
                            let format = userDefaults.string(forKey: "VideoExportFormat")!
                            let filename = self.getFilename(fromURL: (avAsset as! AVURLAsset).url, withExtension: format)
                            //print("--- \(self.file).\(#function) - completion: filename is \(filename)")
                            
                            url = self.documentsDirectory().appendingPathComponent(filename)
                            //print("--- \(self.file).\(#function) - completion: url is \(url!)")
                            
                            exporter.outputURL = url
                            if format == "m4v" {
                                exporter.outputFileType = .m4v
                            } else {
                                exporter.outputFileType = .mp4
                            }
                            
                            exporter.exportAsynchronously(completionHandler: {
                                if let creationDate = avAsset.creationDate?.value as? Date {
                                    self.setCreationDate(date: creationDate, ofItemAt: url!)
                                }
                                if let completionHandler = completionHandler {
                                    completionHandler(url)
                                }
                            })
                        }
                        
                    } else {
                        //print("--- \(self.file).\(#function) - completion: avAsset is nil")
                        if let completionHandler = completionHandler {
                            completionHandler(url)
                        }
                    }
                }
            }
            
            // no asset
        } else {
            if let completionHandler = completionHandler {
                completionHandler(url)
            }
        }
    }
    
    // MARK: - Helper Functions
    
    private func documentsDirectory() -> URL {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[0]
    }
    
    private func getFilename(fromURL url: URL, withExtension extension: String) -> String {
        // create a filename from a full-length URL and apply the specified extension
        let originalName = url.lastPathComponent
        //print("--- \(self.file).\(#function) - originalName: \(originalName)")
        let namePieces = originalName.components(separatedBy: ".")
        let filename = namePieces[0] + "." + `extension`
        //print("--- \(self.file).\(#function) -     filename: \(filename)")
        return filename
    }
    
    private func setCreationDate(date: Date, ofItemAt url: URL) {
        /*
         let dateFormatter = DateFormatter()
         dateFormatter.dateStyle = .medium
         dateFormatter.timeStyle = .short
         dateFormatter.doesRelativeDateFormatting = true
         print("--- \(self.file).\(#function) - exporter completion: creationDate date is \(dateFormatter.string(from: creationDate))")
         */
        //let path = url!.path
        //print("--- \(self.file).\(#function) - exporter completion: path is \(path)")
        let attributes = [FileAttributeKey.creationDate: date, FileAttributeKey.modificationDate: date]
        
        do {
            try FileManager.default.setAttributes(attributes, ofItemAtPath: url.path)
            //print("--- \(self.file).\(#function) - updated attributes")
        } catch {
            print(error)
        }
    }
}
