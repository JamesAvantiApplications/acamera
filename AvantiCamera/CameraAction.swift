//
//  CameraAction.swift
//  AvantiCamera
//
//  Created by James Hager on 3/23/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

enum CameraAction: Int16, CustomStringConvertible {
    case setPhoto = 0
    case takePhoto = 1
    
    case setVideo = 2
    case startVideo = 3
    case stopVideo = 4
    
    case startSpeech = 5
    case stopSpeech = 6
    
    public var description: String {
        switch self {
        case .setPhoto : return "setPhoto"
        case .takePhoto : return "takePhoto"
            
        case .setVideo : return "setVideo"
        case .startVideo : return "startVideo"
        case .stopVideo : return "stopVideo"
            
        case .startSpeech : return "startSpeech"
        case .stopSpeech : return "stopSpeech"
        }
    }
    
    public var command: String {
        switch self {
        case .setPhoto : return "Set Photo"
        case .takePhoto : return "Take Photo"
            
        case .setVideo : return "Set Video"
        case .startVideo : return "Start Video"
        case .stopVideo : return "Stop Video"
            
        case .startSpeech : return "Start Speech"
        case .stopSpeech : return "Stop Speech"
        }
    }
}
