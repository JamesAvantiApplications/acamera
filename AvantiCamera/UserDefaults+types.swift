//
//  UserDefaults+types.swift
//  AvantiCamera
//
//  Created by James Hager on 3/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension UserDefaults {
    func setString(_ string:String, forKey:String) {
        set(string, forKey: forKey)
    }
    func setDate(_ date:Date, forKey:String) {
        set(date, forKey: forKey)
    }
    func dateForKey(_ string:String) -> Date? {
        return object(forKey: string) as? Date
    }
}
