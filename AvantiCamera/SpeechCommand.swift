//
//  SpeechCommand.swift
//  ACamera
//
//  Created by James Hager on 4/15/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation
import CoreData

@objc(SpeechCommand)
class SpeechCommand: NSManagedObject {
    
    var cameraAction: CameraAction {
        get { return CameraAction(rawValue: self.cameraActionSave)! }
        set { self.cameraActionSave = newValue.rawValue }
    }
    
    var command: String {
        get {
            var command = verb.capitalized
            if object != "" {
                command += " "
                command += object.capitalized
            }
            return command
        }
    }
    
    var commandIsEmpty: Bool {
        get {
            return command == ""
        }
    }
    
    var describe: String {
        get {
            return "\(cameraAction): '\(verb)' '\(object)'"
        }
    }
    
    func setUp(cameraAction: CameraAction, verb: String, object: String) {
        self.cameraAction = cameraAction
        self.verb = verb
        self.object = object
    }
    
    func setCommand(_ command: String) {
        let commandComponents = command.components(separatedBy: " ")
        
        verb = commandComponents[0].lowercased()
        
        if commandComponents.count > 1 {
            object = commandComponents[1].lowercased()
        } else {
            object = ""
        }
        
        print("=== SpeechCommand.\(#function) === command: '\(command)' -> verb: '\(verb)', object: '\(object)'")
    }
}
