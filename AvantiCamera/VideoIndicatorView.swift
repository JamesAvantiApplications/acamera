//
//  VideoIndicatorView.swift
//  AvantiCamera
//
//  Created by James Hager on 3/27/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  Triangle drawing based on http://sketchytech.blogspot.com/2014/11/swift-drawing-regular-polygons-with.html
//

import UIKit

extension CGFloat {
    func radians() -> CGFloat {
        let b = CGFloat(Double.pi) * (self/180)
        return b
    }
}

// MARK: -

public protocol VideoIndicatorViewDelegate {
    func didTapOnView(_ videoIndicatorView: VideoIndicatorView)
}

// MARK: -

public class VideoIndicatorView: UIView {
    
    let outerCircleLayer = CAShapeLayer()
    let innerCircleLayer = CAShapeLayer()
    let triangleLayer = CAShapeLayer()
    
    var delegate: VideoIndicatorViewDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        configure()
    }
    
    func configure() {
        outerCircleLayer.strokeColor = UIColor.white.cgColor
        outerCircleLayer.strokeEnd = 1
        outerCircleLayer.fillColor = UIColor.clear.cgColor
        layer.addSublayer(outerCircleLayer)
        
        innerCircleLayer.fillColor = UIColor.black.cgColor
        innerCircleLayer.opacity = 0.4
        layer.addSublayer(innerCircleLayer)
        
        triangleLayer.fillColor = UIColor.white.cgColor
        layer.addSublayer(triangleLayer)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnView))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        let outerWidth = bounds.size.width * 0.025
        
        outerCircleLayer.lineWidth = outerWidth * 2
        outerCircleLayer.frame = bounds
        outerCircleLayer.path = UIBezierPath(ovalIn: bounds).cgPath
        
        innerCircleLayer.frame = bounds
        let ovalFrame = bounds.insetBy(dx: outerWidth, dy: outerWidth)
        innerCircleLayer.path = UIBezierPath(ovalIn: ovalFrame).cgPath
        
        triangleLayer.frame = bounds
        triangleLayer.path = polygonPath(sides: 3, origin: CGPoint(x: bounds.midX , y: bounds.midY), radius: bounds.size.width * 0.25, offset: 0)
    }
    
    @objc func didTapOnView() {
        delegate?.didTapOnView(self)
    }
}
