//
//  ACDeviceOrientation.swift
//  AvantiCamera
//
//  Created by James Hager on 4/5/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

public enum ACDeviceOrientation {
    case portrait // home button at bottom
    case portraitUpsideDown // home button at top
    case landscapeLeft // home button on right
    case landscapeRight // home button on left
    
    var isKindOfPortrait: Bool {
        if self == .portrait || self == .portraitUpsideDown {
            return true
        } else {
            return false
        }
    }
    
    mutating func setFromDeviceOrientation() {
        switch UIDevice.current.orientation {
        case .portraitUpsideDown:
            self = .portrait
        case .landscapeLeft:
            self = .landscapeLeft
        case .landscapeRight:
            self = .landscapeRight
        default:
            self = .portrait
        }
    }
    
    func isStatusBarHidden(view: UIView) -> Bool {
        switch self {
        case .portrait:
            if view.safeAreaInsets != UIEdgeInsets.zero {
                print("=== ACDeviceOrientation.\(#function) === \(debugDate()) - return false")
                return false
            } else {
                print("=== ACDeviceOrientation.\(#function) === \(debugDate()) - return true")
                return true
            }
        default:
            print("=== ACDeviceOrientation.\(#function) === \(debugDate()) - return true")
            return true
        }
    }
    
    func transform() -> CGAffineTransform {
        switch self {
        case .portrait:
            return CGAffineTransform(rotationAngle: CGFloat(0))
        case .portraitUpsideDown:
            return CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        case .landscapeLeft:
            return CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
        case .landscapeRight:
            return CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2))
        }
    }
    
    func imageOrientation(cameraFrontBackState: Int) -> UIImageOrientation {
        // cameraFrontBackState = 0 for front camera
        // cameraFrontBackState = 1 for back camera
        
        switch self {
        case .portrait:
            return .right
        case .portraitUpsideDown:
            return .left
        case .landscapeLeft:
            if cameraFrontBackState == 0 {
                return .down
            } else {
                return .up
            }
        case .landscapeRight:
            if cameraFrontBackState == 0 {
                return .up
            } else {
                return .down
            }
        }
    }
    
    func uiInterfaceOrientation() -> UIInterfaceOrientation {
        switch self {
        case .portrait:
            return .portrait
        case .portraitUpsideDown:
            return .portraitUpsideDown
        case .landscapeLeft:
            return .landscapeRight
        case .landscapeRight:
            return .landscapeLeft
        }
    }
}
