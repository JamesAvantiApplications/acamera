//
//  PhotoViewController.swift
//  AvantiCamera
//
//  Created by James Hager on 3/29/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

public protocol PhotoViewControllerDelegate {
    func closePhotoViewController(scrollToIndex index: Int?, didDeleteAsset: Bool)
}

// MARK: -

class PhotoViewController: UIViewController {
    
    var photoLibraryHelper: PhotoLibraryHelper!
    
    var delegate: PhotoViewControllerDelegate!
    
    override var prefersStatusBarHidden: Bool {
        return  true
    }
    
    var index: Int!
    var indexOnEntry: Int! {
        didSet {
            index = indexOnEntry
        }
    }
    
    var doPlayOnEntry = false
    
    var didDeleteAsset = false
    
    fileprivate let videoIndicatorSize = CGFloat(70)
    
    fileprivate var firstTime = true
    
    fileprivate var isVideoViewPrepped = false
    
    fileprivate var avPlayer: AVPlayer!
    fileprivate var avPlayerLayer: AVPlayerLayer!
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    // MARK: - IB Stuff
    
    @IBOutlet weak var videoIndicatorView: VideoIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewInScrollView: UIView!
    @IBOutlet weak var imageView: ACImageView!
    @IBOutlet weak var videoView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("=========================================")
        print("=== \(file).\(#function) === \(debugDate())")
        print("=========================================")
        
        videoIndicatorView.isHidden = true
        videoIndicatorView.delegate = self
        
        scrollView.delegate = self
        scrollView.maximumZoomScale = 1.0
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        scrollView.addGestureRecognizer(tapGestureRecognizer)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeLeft))
        swipeLeft.direction = .left
        scrollView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeRight))
        swipeRight.direction = .right
        scrollView.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeUp))
        swipeUp.direction = .up
        scrollView.addGestureRecognizer(swipeUp)
        
        imageView.contentMode = .scaleAspectFit
        
        videoView.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        
        if firstTime {
            if let imageSize = imageView?.image?.size {
                firstTime = false
                
                print("--- \(file).\(#function) - scrollView.frame.size.width=\(scrollView.frame.size.width), scrollView.contentSize.width=\(scrollView.contentSize.width)")
                let scaleWidth = scrollView.frame.size.width / imageSize.width
                print("--- \(file).\(#function) - scrollView.frame.size.height=\(scrollView.frame.size.height), scrollView.contentSize.height=\(scrollView.contentSize.height)")
                let scaleHeight = scrollView.frame.size.height / imageSize.height
                
                print("--- \(file).\(#function) - scaleWidth=\(scaleWidth)")
                print("--- \(file).\(#function) - scaleHeight=\(scaleHeight)")
                let minScale = min(scaleWidth, scaleHeight);
                print("--- \(file).\(#function) - minScale=\(minScale)")
                scrollView.minimumZoomScale = minScale
                scrollView.zoomScale = minScale
            }
        }
        
        /*
         print("--- \(file).\(#function) - viewInScrollView.frame.size.width=\(viewInScrollView.frame.size.width), scrollView.contentSize.width=\(scrollView.contentSize.width)")
         print("--- \(file).\(#function) - viewInScrollView.frame.size.height=\(viewInScrollView.frame.size.height), scrollView.contentSize.height=\(scrollView.contentSize.height)")
         */
        
        view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("-----------------------------------------")
        print("=== \(file).\(#function) === \(debugDate())")
        print("-----------------------------------------")
        
        displayImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("=== \(file).\(#function) === \(debugDate())")
        showInfoAlert()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("=== \(file).\(#function) === \(debugDate()) - height=\(size.height), width=\(size.width)")
        
        super.viewWillTransition(to: size, with: coordinator)
        
        firstTime = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("=== \(file).\(#function) === \(debugDate())")
        
        if !videoView.isHidden {
            afterDelay(0.2) {
                self.videoView.isHidden = true
                self.avPlayer.replaceCurrentItem(with: nil)
            }
        }
    }
    
    // MARK: - Functions
    
    func showInfoAlert() {
        
        let userDefaults = UserDefaults.standard
        let firstPhotoViewControllerAlertWasShown = userDefaults.bool(forKey: "FirstPhotoViewControllerAlertWasShown")
        
        if !firstPhotoViewControllerAlertWasShown {
            userDefaults.set(true, forKey: "FirstPhotoViewControllerAlertWasShown")
            userDefaults.synchronize()
            let title = "Gestures"
            let message = "Pinch to zoom and scroll the item.\n\nTap to close the view.\n\nIf the whole item is visible, swipe left/right to go to the next item or swipe up to delete the item."
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Got It", style: .default, handler: nil)
            alert.addAction(okAction)
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    func displayImage() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        photoLibraryHelper.getImage(at: index, imageSize: nil) { image, mediaType in
            if let image = image {
                OperationQueue.main.addOperation {
                    self.scrollView.contentSize = image.size
                    self.imageView.image = image
                    if mediaType == .video && !self.doPlayOnEntry {
                        self.videoIndicatorView.isHidden = false
                    } else {
                        self.videoIndicatorView.isHidden = true
                    }
                    print("--- \(self.file).\(#function) - completionHandler: image size \(image.size)")
                    self.firstTime = true
                    self.view.layoutIfNeeded()
                    self.scrollViewDidZoom(self.scrollView)
                    self.scrollView.setZoomScale(0.01, animated: true)
                    
                    self.avPlayerLayer?.frame = self.videoView.bounds
                    
                    if self.doPlayOnEntry {
                        self.doPlayOnEntry = false
                        self.doPlayVideo()
                    }
                }
            }
        }
    }
    
    @objc func handleTap() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if !videoView.isHidden {
            stopVideoPlayback()
        } else {
            closeSelf()
        }
    }
    
    @objc func handleSwipeLeft() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let newIndex = index - 1
        if newIndex > -1 {
            displayNewImage(newIndex)
        }
    }
    
    @objc func handleSwipeRight() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let newIndex = index + 1
        if newIndex < photoLibraryHelper.photoAssets.count {
            displayNewImage(newIndex)
        }
    }
    
    @objc func handleSwipeUp() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if index > -1 && index < photoLibraryHelper.photoAssets.count {
            //stopVideoPlayback()
            if !videoView.isHidden {
                avPlayer.pause()
            }
            
            photoLibraryHelper.deleteAssets([photoLibraryHelper.photoAssets[index]]) { deleted, error in
                if error != nil {
                    print("Error deleting image: \(error!.localizedDescription)")
                }
                if deleted {
                    print("--- \(self.file).\(#function) - completionHandler: image deleted from library")
                    OperationQueue.main.addOperation {
                        self.didDeleteAsset = true
                        self.stopVideoPlayback()
                        self.photoLibraryHelper.getAssets()
                        self.displayImage()
                    }
                } else {
                    print("--- \(self.file).\(#function) - completionHandler: image not deleted from library")
                    OperationQueue.main.addOperation {
                        if !self.videoView.isHidden {
                            self.avPlayer.play()
                        }
                    }
                }
            }
        }
    }
    
    func stopVideoPlayback() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if !videoView.isHidden {
            avPlayer.pause()
            videoIndicatorView.isHidden = false
            videoView.isHidden = true
            avPlayer.replaceCurrentItem(with: nil)
        }
    }
    
    func displayNewImage(_ newIndex: Int) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        stopVideoPlayback()
        index = newIndex
        displayImage()
    }
    
    func doPlayVideo() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if !isVideoViewPrepped {
            prepVideoView()
        }
        
        photoLibraryHelper.getAVAsset(at: index) { avAsset, avAudioMix, info in
            if let avAsset = avAsset {
                OperationQueue.main.addOperation {
                    print("--- \(self.file).\(#function) - completion: avAsset is set")
                    self.playVideo(avAsset: avAsset)
                }
            } else {
                print("--- \(self.file).\(#function) - completion: avAsset is nil")
            }
        }
    }
    
    func playVideo(avAsset: AVAsset) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let playerItem = AVPlayerItem(asset: avAsset)
        avPlayer.replaceCurrentItem(with: playerItem)
        
        avPlayer.play()
        
        afterDelay(0.2) {
            self.videoIndicatorView.isHidden = true
            self.videoView.isHidden = false
        }
    }
    
    func prepVideoView() {
        print("=== \(file).\(#function) === \(debugDate())")
        isVideoViewPrepped = true
        
        avPlayer = AVPlayer()
        
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.frame = videoView.bounds
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        videoView.layer.insertSublayer(avPlayerLayer, at: 0)
        
        view.layoutIfNeeded()
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.videoFinished(_:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    @objc func videoFinished(_ notification: NSNotification) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        videoIndicatorView.isHidden = false
        videoView.isHidden = true
        
        if doPlayOnEntry {
            closeSelf()
        }
    }
    
    func closeSelf() {
        print("=== \(file).\(#function) === \(debugDate())")
        if !videoView.isHidden {
            avPlayer.pause()
        }
        
        var scrollToIndex: Int?
        if index != indexOnEntry {
            scrollToIndex = index
        }
        delegate?.closePhotoViewController(scrollToIndex: scrollToIndex, didDeleteAsset: didDeleteAsset)
    }
}

// MARK: - VideoIndicatorViewDelegate

extension PhotoViewController: VideoIndicatorViewDelegate {
    
    func didTapOnView(_ videoIndicatorView: VideoIndicatorView) {
        print("=== \(file).\(#function) ===")
        
        doPlayVideo()
    }
}

// MARK: - UIScrollViewDelegate

extension PhotoViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return viewInScrollView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        print("=== \(file).\(#function) ===")
        let viewInScrollViewSize = viewInScrollView.frame.size
        let scrollViewSize = scrollView.bounds.size
        
        print("--- \(file).\(#function) - viewInScrollViewSize - height=\(viewInScrollViewSize.height), width=\(viewInScrollViewSize.width)")
        print("--- \(file).\(#function) - scrollViewSize - height=\(scrollViewSize.height), width=\(scrollViewSize.width)")
        
        let verticalPadding = viewInScrollViewSize.height < scrollViewSize.height ? round((scrollViewSize.height - viewInScrollViewSize.height) / 2) : 0
        let horizontalPadding = viewInScrollViewSize.width < scrollViewSize.width ? round((scrollViewSize.width - viewInScrollViewSize.width) / 2) : 0
        
        print("--- \(file).\(#function) - verticalPadding=\(verticalPadding), horizontalPadding=\(horizontalPadding)")
        
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    }
}
