//
//  ACImageView.swift
//  AvantiCamera
//
//  Created by James Hager on 3/27/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  Set videoIndicatorSize before mediaType to get proper placement of the indicator
//

import UIKit
import Photos

public protocol ACImageViewDelegate {
    func didMultiTouchOnACImageView(_ acImageView: ACImageView)
    func didTapOnVideoIndicatorView(in acImageView: ACImageView)
}

// MARK: -

public class ACImageView: UIImageView {
    
    var delegate: ACImageViewDelegate? {
        didSet {
            //print("=== ACImageView.delegate didSet - tag: \(tag)")
            if delegate != nil {
                addGestureRecognizers()
            } else {
                removeGestureRecognizers()
            }
        }
    }
    
    var mediaType: PHAssetMediaType! {
        didSet {
            if mediaType != mediaTypeDisplayed {
                if mediaType == .video {
                    showVideoIndicator()
                } else {
                    hideVideoIndicator()
                }
            }
        }
    }
    
    var mediaTypeDisplayed = PHAssetMediaType.unknown
    
    var videoIndicatorSize = CGFloat(50) {
        didSet {
            if videoIndicatorView != nil {
                setVideoIndicatorConstraints()
            }
        }
    }
    
    var listenForVideoIndicatorSizeShouldChange = false {
        didSet {
            //print("=== ACImageView.delegate didSet - listenForVideoIndicatorSizeShouldChange: \(listenForVideoIndicatorSizeShouldChange)")
            if listenForVideoIndicatorSizeShouldChange != listenForVideoIndicatorSizeShouldChangeSet {
                listenForVideoIndicatorSizeShouldChangeSet = listenForVideoIndicatorSizeShouldChange
                if listenForVideoIndicatorSizeShouldChange {
                    NotificationCenter.default.addObserver(self, selector: #selector(self.changeVideoIndicatorSize(_:)), name: videoIndicatorSizeShouldChangeNotificationName, object: nil)
                } else {
                    NotificationCenter.default.removeObserver(self, name: videoIndicatorSizeShouldChangeNotificationName, object: nil)
                }
            }
        }
    }
    var listenForVideoIndicatorSizeShouldChangeSet = false
    
    var multiTouchGestureRecognizer: UITapGestureRecognizer!
    var tapGestureRecognizer: UITapGestureRecognizer!
    
    var videoIndicatorView: VideoIndicatorView!
    var videoIndicatorConstraints = [NSLayoutConstraint]()
    
    // MARK: - VideoIndicatorView
    
    func setUpVideoIndicatorView() {
        //print("=== ACImageView.setUpVideoIndicatorView - tag: \(tag)")
        let frame = CGRect(x: 0, y: 0, width: videoIndicatorSize, height: videoIndicatorSize)
        videoIndicatorView = VideoIndicatorView(frame: frame)
        videoIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(videoIndicatorView)
        
        videoIndicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        videoIndicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        setVideoIndicatorConstraints()
        
        if delegate != nil {
            addTapGestureRecognizer()
        }
    }
    
    func setVideoIndicatorConstraints() {
        NSLayoutConstraint.deactivate(videoIndicatorConstraints)
        videoIndicatorConstraints = [NSLayoutConstraint]()
        
        videoIndicatorConstraints.append(videoIndicatorView.widthAnchor.constraint(equalToConstant: videoIndicatorSize))
        videoIndicatorConstraints.append(videoIndicatorView.heightAnchor.constraint(equalToConstant: videoIndicatorSize))
        
        NSLayoutConstraint.activate(videoIndicatorConstraints)
        
        videoIndicatorView?.layoutIfNeeded()
    }
    
    func showVideoIndicator() {
        if videoIndicatorView == nil {
            setUpVideoIndicatorView()
        }
        videoIndicatorView.isHidden = false
    }
    
    func hideVideoIndicator() {
        videoIndicatorView?.isHidden = true
    }
    
    func addGestureRecognizers() {
        addMultiTouchGestureRecognizer()
        if videoIndicatorView != nil {
            addTapGestureRecognizer()
        }
    }
    
    func addMultiTouchGestureRecognizer() {
        
        self.isMultipleTouchEnabled = true
        if multiTouchGestureRecognizer == nil {
            multiTouchGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didMultiTouchOnACImageView))
        }
        multiTouchGestureRecognizer.numberOfTouchesRequired = 2
        self.addGestureRecognizer(multiTouchGestureRecognizer)
        self.isUserInteractionEnabled = true
    }
    
    func addTapGestureRecognizer() {
        if tapGestureRecognizer == nil {
            tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnVideoIndicatorView))
        }
        videoIndicatorView.addGestureRecognizer(tapGestureRecognizer)
        videoIndicatorView.isUserInteractionEnabled = true
    }
    
    func removeGestureRecognizers() {
        if multiTouchGestureRecognizer != nil {
            self.removeGestureRecognizer(multiTouchGestureRecognizer)
            self.isUserInteractionEnabled = false
        }
        
        if tapGestureRecognizer != nil {
            videoIndicatorView.removeGestureRecognizer(tapGestureRecognizer)
            videoIndicatorView.isUserInteractionEnabled = false
        }
    }
    
    @objc func changeVideoIndicatorSize(_ notification: Notification) {
        //print("=== ACImageView.\(#function) - tag: \(tag)")
        
        if let userInfo = notification.userInfo as? Dictionary<String,Any>,
            let newVideoIndicatorSize = userInfo["videoIndicatorSize"] as? CGFloat {
            if newVideoIndicatorSize != videoIndicatorSize {
                videoIndicatorSize = newVideoIndicatorSize
            }
        }
    }
    
    // MARK: - Actions
    
    @objc func didMultiTouchOnACImageView() {
        //print("=== ACImageView.\(#function) - tag: \(tag)")
        delegate?.didMultiTouchOnACImageView(self)
    }
    
    @objc func didTapOnVideoIndicatorView() {
        //print("=== ACImageView.\(#function) - tag: \(tag)")
        delegate?.didTapOnVideoIndicatorView(in: self)
    }
}
