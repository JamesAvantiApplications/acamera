//
//  PhotoLibraryMultipleViewController.swift
//  ACamera
//
//  Created by James Hager on 4/14/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import Photos

public protocol PhotoLibraryMultipleViewControllerDelegate {
    func photoLibraryMultipleViewControllerWillDisappear(scrollToIndexPath indexPath: IndexPath?, didDeleteAsset: Bool)
}

// MARK: -

class PhotoLibraryMultipleViewController: UIViewController {
    
    var photoLibraryHelper: PhotoLibraryHelper!
    
    var delegate: PhotoLibraryMultipleViewControllerDelegate?
    
    var isSmallDevice = true
    
    var indexOnEntry = 0
    
    var collectionViewDidBeginDragging = false
    var didDeleteAsset = false
    
    var doHandleWillAppear = true
    var updateAssets = false
    
    var actionButton: UIBarButtonItem!
    
    var collectionViewCellSize = CGSize(width: 100, height: 100)
    var collectionViewImageSize = CGSize(width: 90, height: 90)
    var videoIndicatorSize = CGFloat(25)
    let marginInCell = CGFloat(6)
    
    var indexPathAtCenter: IndexPath? {
        let viewCenterPoint = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        let collectionViewCenterPoint = view.convert(viewCenterPoint, to: collectionView)
        if let centerIndexPath = collectionView.indexPathForItem(at: collectionViewCenterPoint) {
            return centerIndexPath
        }
        
        if collectionView.indexPathsForVisibleItems.count > 1 {
            let firstIndex = collectionView.indexPathsForVisibleItems.first!.row
            let lastIndex = collectionView.indexPathsForVisibleItems.last!.row
            let row = (firstIndex + lastIndex) / 2
            return IndexPath(row: row, section: 0)
        }
        return nil
    }
    
    struct CollectionViewCellIdentifiers {
        static let selectableCell = "PhotoLibrarySelectableCell"
    }
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    // MARK: - IB Stuff
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("=========================================")
        print("=== \(file).\(#function) === \(debugDate())")
        print("=========================================")
        //print("--- \(file).\(#function) - frame.size: \(view.frame.size), collectionView.frame.size: \(collectionView.frame.size)")
        
        setBarButtonItems()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
        collectionView.register(PhotoLibrarySelectableCell.self, forCellWithReuseIdentifier: CollectionViewCellIdentifiers.selectableCell)
        collectionView.allowsMultipleSelection = true
        
        collectionView.backgroundColor = Colors.reallyLightGray
        
        setCollectionViewSizes()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("-----------------------------------------")
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation())")
        print("-----------------------------------------")
        
        collectionViewDidBeginDragging = false
        
        handleWillAppear()
    }
    
    /*
     override func viewDidAppear(_ animated: Bool) {
     super.viewDidAppear(animated)
     print("=== \(file).\(#function) === \(debugDate())")
     print("--- \(file).\(#function) - frame.size: \(view.frame.size), collectionView.frame.size: \(collectionView.frame.size)")
     }
     */
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("=== \(file).\(#function) === \(debugDate())")
        
        if collectionViewDidBeginDragging {
            delegate?.photoLibraryMultipleViewControllerWillDisappear(scrollToIndexPath: indexPathAtCenter, didDeleteAsset: didDeleteAsset)
        } else {
            delegate?.photoLibraryMultipleViewControllerWillDisappear(scrollToIndexPath: nil, didDeleteAsset: didDeleteAsset)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("=== \(file).\(#function) === \(debugDate())")
        super.viewWillTransition(to: size, with: coordinator)
        
        if let indexPath = indexPathAtCenter {
            print("=== \(file).\(#function) - indexPathAtCenter \(displayPath(indexPath))")
            coordinator.animate(alongsideTransition: { _ -> Void in
                self.collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
            })
        }
    }
    
    //MARK:- Setup
    
    func setNavigationTitle() {
        print("=== \(file).\(#function) === \(debugDate())")
        var numSelected = 0
        if let indexPaths = collectionView.indexPathsForSelectedItems {
            numSelected = indexPaths.count
        }
        navigationItem.title = "\(numSelected) of \(photoLibraryHelper.photoAssets.count)"
    }
    
    func setBarButtonItems() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let trashButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(handleTrash))
        actionButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(handleAction))
        
        let noneButton = UIBarButtonItem(title: "None", style: .plain, target: self, action: #selector(handleNone))
        let allButton = UIBarButtonItem(title: "All", style: .plain, target: self, action: #selector(handleAll))
        
        //noneButton.isEnabled = false
        
        if isSmallDevice {
            navigationItem.setRightBarButtonItems([trashButton, actionButton], animated: false)
            navigationItem.leftItemsSupplementBackButton = true
            navigationItem.setLeftBarButtonItems([noneButton, allButton], animated: false)
        } else {
            let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            space.width = 18.0
            navigationItem.setRightBarButtonItems([trashButton, actionButton, space, allButton, space, noneButton], animated: false)
        }
    }
    
    func setCollectionViewSizes() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let minViewDimension = min(view.frame.size.width,view.frame.size.height)
        
        let numberAcross = CGFloat(Int(minViewDimension / 115))
        let edgeMargin = CGFloat(4)
        let betweenMargin = CGFloat(2)
        let sectionInset = UIEdgeInsets(top: edgeMargin, left: edgeMargin, bottom: edgeMargin, right: edgeMargin)
        
        let cellDimension = minViewDimension / numberAcross - (numberAcross - 1) * betweenMargin - edgeMargin / 2 //- 2 * edgeMargin
        let imageDimension = cellDimension - marginInCell
        
        collectionViewCellSize = CGSize(width: cellDimension, height: cellDimension)
        collectionViewImageSize = CGSize(width: imageDimension, height: imageDimension)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = sectionInset
        layout.itemSize = collectionViewCellSize
        layout.minimumInteritemSpacing = betweenMargin
        layout.minimumLineSpacing = 2 * betweenMargin
        collectionView?.collectionViewLayout = layout
        
        videoIndicatorSize = imageDimension * 0.18
        //print("--- \(file).\(#function) - dimension: \(cellDimension) \(imageDimension), videoIndicatorSize: \(videoIndicatorSize)")
    }
    
    //MARK:- Actions
    
    func handleWillAppear() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if updateAssets {
            photoLibraryHelper.getAssets()
            updateAssets = false
        }
        
        displayPhotosAndVideos()
        
        let indexPath = IndexPath(row: indexOnEntry, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: false)
        
        doHandleWillAppear = false
    }
    
    @objc func handleAll() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        for index in 0..<photoLibraryHelper.photoAssets.count {
            collectionView.selectItem(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: [])
        }
        setNavigationTitle()
    }
    
    @objc func handleNone() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        for index in 0..<photoLibraryHelper.photoAssets.count {
            collectionView.deselectItem(at: IndexPath(row: index, section: 0), animated: false)
        }
        setNavigationTitle()
    }
    
    @objc func handleAction() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if let indexPaths = collectionView.indexPathsForSelectedItems {
            if indexPaths.count > 0 {
                var urls = [URL]()
                
                for indexPath in indexPaths {
                    let index = indexPath.row
                    print("--- \(self.file).\(#function) - get index \(index)")
                    photoLibraryHelper.getInAppFileURLToShare(at: index) { url in
                        if let url = url {
                            urls.append(url)
                            print("--- \(self.file).\(#function) - index \(index), urls: \(urls.count), url: \(url)")
                            if urls.count == indexPaths.count {
                                ActionHelper.presentAction(urls: urls, actionButton: self.actionButton, viewController: self)
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func handleTrash() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if let indexPaths = collectionView.indexPathsForSelectedItems {
            if indexPaths.count > 0 {
                doHandleWillAppear = false
                
                var assets = [PHAsset]()
                
                for indexPath in indexPaths {
                    if let asset = photoLibraryHelper.getAsset(at: indexPath.row) {
                        assets.append(asset)
                    }
                }
                
                photoLibraryHelper.deleteAssets(assets) { deleted, error in
                    if error != nil {
                        print("Error deleting image: \(error!.localizedDescription)")
                    }
                    if deleted {
                        print("--- \(self.file).\(#function) - completionHandler: images deleted from library")
                        OperationQueue.main.addOperation {
                            self.didDeleteAsset = true
                            self.getPhotosAndVideos()
                        }
                    } else {
                        print("--- \(self.file).\(#function) - completionHandler: images not deleted from library")
                    }
                }
            }
        }
    }
    
    func getPhotosAndVideos() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        photoLibraryHelper.getAssets()
        displayPhotosAndVideos()
    }
    
    func displayPhotosAndVideos() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        print("--- \(self.file).\(#function) - call collectionView.reloadData()")
        collectionView.reloadData()
        setNavigationTitle()
    }
}

// MARK: - UIScrollViewDelegate

extension PhotoLibraryMultipleViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        collectionViewDidBeginDragging = true
    }
}

// MARK: - UICollectionViewDataSource

extension PhotoLibraryMultipleViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        print("=== \(file).\(#function) === \(debugDate())")
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let numberOfItems = photoLibraryHelper.photoAssets.count
        print("=== \(file).\(#function) -> \(numberOfItems) === \(debugDate())")
        return numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("=== \(file).\(#function) \(displayPath(indexPath)) === \(debugDate())")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCellIdentifiers.selectableCell, for: indexPath) as! PhotoLibrarySelectableCell
        
        photoLibraryHelper.getImage(at: indexPath.row, imageSize: collectionViewImageSize) { image, mediaType in
            if let image = image {
                
                cell.configureCell(image: image, mediaType: mediaType, videoIndicatorSize: self.videoIndicatorSize, marginInCell: self.marginInCell)
                //print("--- \(self.file).\(#function) \(displayPath(indexPath)) - completionHandler: image size \(image.size)")
            }
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegate

extension PhotoLibraryMultipleViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("=== \(file).\(#function) \(displayPath(indexPath)) === \(debugDate())")
        setNavigationTitle()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("=== \(file).\(#function) \(displayPath(indexPath)) === \(debugDate())")
        setNavigationTitle()
    }
}
