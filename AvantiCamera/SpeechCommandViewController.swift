//
//  SpeechCommandViewController.swift
//  ACamera
//
//  Created by James Hager on 4/15/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import CoreData

class SpeechCommandViewController: UITableViewController {
    
    var coreDataStack: CoreDataStack!
    
    var cameraAction: CameraAction! {
        didSet {
            if fetchRequest != nil {
                defineFetchRequestPredicate()
                performFetchAndReload()
            }
        }
    }
    
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    var fetchRequest: NSFetchRequest<NSFetchRequestResult>!
    
    var doPerformFetchOnViewWillAppear = false
    
    var indexPathSelected: IndexPath?
    
    override var prefersStatusBarHidden: Bool {
        return  false
    }
    
    var defaultCellSeparatorInset: UIEdgeInsets!
    let noCellSeparatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    
    var doAdjustFonts = false
    
    struct TableViewCellIdentifiers {
        static let displayCell = SettingsCellIdentifiers.textCell.rawValue
        static let editCell = "SpeechCommandEditCell"
    }
    
    lazy var file: String = getSourceFileNameFromFullPath(#file)
    
    func initSettingsDataItems() {
        print("=== \(file).\(#function) ===")
    }
    
    // MARK: - IB Stuff
    
    override func viewDidLoad() {
        print("=========================================")
        print("=== \(file).\(#function) === \(debugDate())")
        print("=========================================")
        super.viewDidLoad()
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAdd))
        
        let resetButton = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(handleReset))
        
        navigationItem.setRightBarButtonItems([addButton, resetButton], animated: false)
        
        registerCells()
        
        defineFetchedResultsController()
        performFetch()
        
        tableView.estimatedRowHeight = 68.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        print("-----------------------------------------")
        print("=== \(file).\(#function) === \(debugDate())")
        print("-----------------------------------------")
        
        navigationItem.title = cameraAction.command
        
        if doPerformFetchOnViewWillAppear {
            performFetch()
        } else {
            doPerformFetchOnViewWillAppear = true
        }
        
        //performFetch()
        //tableView.reloadData()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        print("=== \(file).\(#function) - self=\(self)")
        
    }
    
    // MARK: - Functions
    
    func registerCells() {
        print("=== \(file).\(#function) ===")

        tableView.register(UINib(nibName: TableViewCellIdentifiers.displayCell, bundle: nil), forCellReuseIdentifier: TableViewCellIdentifiers.displayCell)
        
        tableView.register(UINib(nibName: TableViewCellIdentifiers.editCell, bundle: nil), forCellReuseIdentifier: TableViewCellIdentifiers.editCell)
    }
    
    fileprivate func defineFetchedResultsController() {
        print("=== \(file).\(#function) ===")
        
        fetchRequest = NSFetchRequest()
        
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: "SpeechCommand", in: coreDataStack.context)
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "verb", ascending: true), NSSortDescriptor(key: "object", ascending: true)]
        
        fetchRequest.fetchBatchSize = 20
        
        defineFetchRequestPredicate()
        
        print("--- \(file).\(#function) - fetchRequest.predicate=\(fetchRequest.predicate!)")
        
        fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: coreDataStack.context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        fetchedResultsController.delegate = self
    }
    
    fileprivate func defineFetchRequestPredicate() {
        print("=== \(file).\(#function) ===")
        
        fetchRequest.predicate = NSPredicate(format: "cameraActionSave == %d", cameraAction.rawValue)
    }
    
    fileprivate func performFetch() {
        print("=== \(file).\(#function) ===")
        
        do {
            try fetchedResultsController.performFetch()
            
        } catch {
            fatalCoreDataError(error)
        }
    }
    
    fileprivate func performFetchAndReload() {
        performFetch()
        tableView.reloadData()
    }
    
    func doReset() {
        indexPathSelected = nil
        coreDataStack.deleteSpeechCommands(cameraAction: cameraAction)
        setDefaultSpeechCommands(cameraAction: cameraAction, coreDataStack: coreDataStack)
        coreDataStack.saveContext()
        performFetchAndReload()
    }
    
    func setBottomSeperator(indexPath: IndexPath, cell: UITableViewCell) {
        print("=== \(file).\(#function) - indexPath.......: \(displayPath(indexPath))")
        
        var nextRowIndexPath = indexPath
        nextRowIndexPath.row += 1
        print("--- \(file).\(#function) - nextRowIndexPath: \(displayPath(nextRowIndexPath))")
        
        if defaultCellSeparatorInset == nil {
            defaultCellSeparatorInset = cell.separatorInset
        }
        
        var hideBottomSeperator = false
        
        if nextRowIndexPath.row < tableView(tableView, numberOfRowsInSection: indexPath.section) {
            if let indexPathSelected = indexPathSelected {
                if nextRowIndexPath == indexPathSelected {
                    print("--- \(file).\(#function) - next row is selected")
                    hideBottomSeperator =  true
                }
            }
        }
        
        if hideBottomSeperator {
            cell.separatorInset = noCellSeparatorInset
        } else {
            cell.separatorInset = defaultCellSeparatorInset
        }
    }
    
    // MARK: - Acions
    
    @objc func handleAdd() {
        print("=== \(file).\(#function) === \(debugDate())")
        indexPathSelected = nil
        coreDataStack.makeNewSpeechCommand(cameraAction: cameraAction)
    }
    
    @objc func handleReset() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let alert = UIAlertController(title: "Reset Commands", message: "Reset the \"\(cameraAction.command)\" speech commands to the default values?", preferredStyle: .alert)
        
        let resetAction = UIAlertAction(title: "Reset", style: .destructive) { (alertAction) -> Void in
            self.doReset()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(resetAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }

    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        let numSections = fetchedResultsController.sections!.count
        print("=== \(file).\(#function) === \(numSections)")
        return numSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        print("=== \(file).\(#function) \(section) -> \(sectionInfo.numberOfObjects)")
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("=== \(file).\(#function) === \(displayPath(indexPath))")
        
        let speechCommand = fetchedResultsController.object(at: indexPath) as! SpeechCommand
        
        var selected = false
        if let indexPathSelected = indexPathSelected {
            selected = indexPath == indexPathSelected
        }
        
        if selected || speechCommand.commandIsEmpty {
            indexPathSelected = indexPath
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.editCell, for: indexPath) as! SpeechCommandEditCell
            cell.configureCell(speechCommand: speechCommand, coreDataStack: coreDataStack, delegate: self)
            cell.separatorInset = noCellSeparatorInset
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.displayCell, for: indexPath) as! SettingsTextCell
            cell.configureCell(speechCommand.command)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
        }
    }

    // MARK: - UITableViewDelegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("=== \(file).\(#function) \(displayPath(indexPath)) ===")
        
        /*
        var indexPaths = [IndexPath]()
        if let indexPathSelected = indexPathSelected {
            indexPaths.append(indexPathSelected)
        }
        indexPathSelected = indexPath
        indexPaths.append(indexPathSelected!)
        tableView.reloadRows(at: indexPaths, with: .none)
        */
        
        indexPathSelected = indexPath
        tableView.reloadData()
        
        print("--- \(file).\(#function) \(displayPath(indexPath)) - becomeFirstResponder")
        let cell = tableView.cellForRow(at: indexPath) as! SpeechCommandEditCell
        cell.commandTextField.becomeFirstResponder()
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        print("=== \(file).\(#function) - indexPath=\(displayPath(indexPath))")
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let speechCommand = fetchedResultsController.object(at: indexPath) as! SpeechCommand
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            print("=== \(self.file).\(#function) - indexPath=\(displayPath(indexPath)) - Delete \(speechCommand.command)")
            self.coreDataStack.context.delete(speechCommand)
            self.coreDataStack.saveContext()
        }
        
        return [delete]
    }
}

// MARK: - NSFetchedResultsControllerDelegate

extension SpeechCommandViewController : NSFetchedResultsControllerDelegate{
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        print("*** \(file).\(#function), \(debugDate())")
        /*
        if let indexPathSelected = indexPathSelected {
            self.indexPathSelected = nil
            tableView.reloadRows(at: [indexPathSelected], with: .none)
        }
        */
        indexPathSelected = nil
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            print("*** \(file) NSFetchedResultsChangeInsert (object), \(debugDate()), indexPath=\(displayPath(indexPath)), newIndexPath=\(displayPath(newIndexPath))")
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            
        case .delete:
            print("*** \(file) NSFetchedResultsChangeDelete (object), \(debugDate()), indexPath=\(displayPath(indexPath)), newIndexPath=\(displayPath(newIndexPath))")
            tableView.deleteRows(at: [indexPath!], with: .fade)
            
        case .update:
            print("*** \(file) NSFetchedResultsChangeUpdate (object), \(debugDate()), indexPath=\(displayPath(indexPath)), newIndexPath=\(displayPath(newIndexPath))")
            
        case .move:
            print("*** \(file) NSFetchedResultsChangeMove (object), \(debugDate()), indexPath=\(displayPath(indexPath)), newIndexPath=\(displayPath(newIndexPath))")
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            print("*** \(file) NSFetchedResultsChangeInsert (section), \(debugDate()) - sectionIndex=\(sectionIndex)")
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
            
        case .delete:
            print("*** \(file) NSFetchedResultsChangeDelete (section), \(debugDate()) - sectionIndex=\(sectionIndex)")
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
            
        case .update:
            print("*** \(file) NSFetchedResultsChangeUpdate (section), \(debugDate()) - sectionIndex=\(sectionIndex)")
            
        case .move:
            print("*** \(file) NSFetchedResultsChangeMove (section), \(debugDate()) - sectionIndex=\(sectionIndex)")
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        print("*** \(file).\(#function), \(debugDate())")
        tableView.endUpdates()
        tableView.reloadData()
    }
}

// MARK: - SpeechCommandEditCellDelegate

extension SpeechCommandViewController: SpeechCommandEditCellDelegate {
    
    func deselectSpeechCommandEditCell(_ cell: SpeechCommandEditCell) {
        indexPathSelected = nil
        tableView.reloadData()
    }
}
