//
//  Camera.swift
//  AvantiCamera
//
//  Created by James Hager on 4/5/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import CoreMotion

public protocol CameraDelegate {
    func cameraOrientationDidChange(_ cameraOrientation: ACDeviceOrientation)
    func deviceOrientationDidChange(_ deviceOrientation: ACDeviceOrientation)
    func isPhotoLibraryAvailableDidChange(_ isPhotoLibraryAvailable: Bool)
    func isCameraAvailableDidChange(_ isCameraAvailable: Bool)
    func photoAspectRatioDidSet(_ photoAspectRatio: CGFloat)
    func camPreviewAspectRatioDidSet(_ camPreviewAspectRatio: CGFloat)
    func cameraSelfTimerStateDidChange()
    func selfTimerIsRunning(duration: Int, timeLeft: Int)
    func takingPicture()
    func savedImage(_ image: UIImage)
    func startedRecording()
    func recordingDurationIs(_ duration: Float)
    func stoppedRecording()
    func videoSavedToLibrary(id: String)
    func handleAssetWasDeleted()
    func showCameraAlert(title: String?, message: String?)
}

// MARK: -

class Camera: NSObject {
    
    var delegate: CameraDelegate?
    
    var actOnIsCameraAvailableDidSet = false
    
    let photoLibraryHelper = PhotoLibraryHelper()
    
    // MARK: - Camera Variables
    
    let captureSession = AVCaptureSession()
    
    let stillImageOutput = AVCapturePhotoOutput()
    let movieOutput = AVCaptureMovieFileOutput()
    
    var cropArea = CGRect.zero
    
    var videoInput: AVCaptureDeviceInput!
    
    var outputURL: URL!
    
    var assetCreatedIdentifier: String!
    
    // MARK: - Orientation & Permission Variables
    
    let motionManager = CMMotionManager()
    
    var cameraOrientation: ACDeviceOrientation = .portrait {
        didSet {
            if cameraOrientation != cameraOrientationSet {
                print("=== \(file).\(#function) === \(debugDate()) - didSet new cameraOrientation: \(cameraOrientation)")
                cameraOrientationSet = cameraOrientation
                OperationQueue.main.addOperation {
                    self.delegate?.cameraOrientationDidChange(self.cameraOrientation)
                }
            }
        }
    }
    var cameraOrientationSet: ACDeviceOrientation = .portrait
    
    var deviceOrientation: ACDeviceOrientation = .portrait {
        didSet {
            print("=== \(file).\(#function) === \(debugDate()) - didSet new deviceOrientation: \(deviceOrientation)")
            if deviceOrientation != deviceOrientationSet {
                deviceOrientationSet = deviceOrientation
                delegate?.deviceOrientationDidChange(deviceOrientation)
            }
        }
    }
    var deviceOrientationSet: ACDeviceOrientation = .portrait
    
    var isVideoAvailable = false {
        didSet {
            setIsCameraAvailableFromConstituents()
        }
    }
    
    var isAudioAvailable = false {
        didSet {
            setIsCameraAvailableFromConstituents()
        }
    }
    
    var isPhotoLibraryAvailable = false {
        didSet {
            if isPhotoLibraryAvailable != isPhotoLibraryAvailableSet {
                isPhotoLibraryAvailableSet = isPhotoLibraryAvailable
                delegate?.isPhotoLibraryAvailableDidChange(isPhotoLibraryAvailable)
                setIsCameraAvailableFromConstituents()
            }
        }
    }
    var isPhotoLibraryAvailableSet = false
    
    var isCameraAvailable = false {
        didSet {
            print("=== \(file).\(#function) didSet === \(debugDate()) - \(isCameraAvailable)")
            if actOnIsCameraAvailableDidSet {
                if isCameraAvailable != isCameraAvailableSet {
                    isCameraAvailableSet = isCameraAvailable
                    delegate?.isCameraAvailableDidChange(isCameraAvailable)
                }
            }
        }
    }
    var isCameraAvailableSet = false
    
    var isBothCamerasAvailable: Bool {
        get {
            return isCameraAvailable && deviceHasFrontCamera && deviceHasBackCamera
        }
    }
    
    // MARK: - Aspect Ratio Variables
    
    var photoAspectRatio: CGFloat = 1.5 {
        didSet {
            delegate?.photoAspectRatioDidSet(photoAspectRatio)
        }
    }
    var videoAspectRatio: CGFloat = 16.0/9.0
    
    var camPreviewAspectRatio: CGFloat = 1.0 {
        didSet {
            delegate?.camPreviewAspectRatioDidSet(camPreviewAspectRatio)
        }
    }
    
    // MARK: - User Settings Variables
    
    var cameraFrontBackState: Int! {
        didSet {
            if cameraFrontBackState != cameraFrontBackStateInUse {
                cameraFrontBackStateInUse = cameraFrontBackState
                if isCameraAvailable {
                    _ = attachedCameraToSession()
                }
            }
        }
    }
    var cameraFrontBackStateInUse = -1
    
    var cameraAspectRatio: Double! {
        didSet {
            let cameraAspectRatioCGFloat = CGFloat(cameraAspectRatio)
            if cameraAspectRatioCGFloat != photoAspectRatio {
                photoAspectRatio = cameraAspectRatioCGFloat
            }
        }
    }
    
    var cameraFlashState: Int! {
        didSet {
            if cameraFlashState != cameraFlashStateInUse {
                cameraFlashStateInUse = cameraFlashState
                if let cameraFlashMode = cameraFlashModeFromState[cameraFlashState] {
                    self.cameraFlashMode = cameraFlashMode
                }
            }
        }
    }
    var cameraFlashStateInUse = -1
    
    var cameraSelfTimerState: Int! {
        didSet {
            if cameraSelfTimerState != cameraSelfTimerStateInUse {
                cameraSelfTimerStateInUse = cameraSelfTimerState
                delegate?.cameraSelfTimerStateDidChange()
            }
        }
    }
    var cameraSelfTimerStateInUse = -1
    
    var cameraBurstState: Int! {
        didSet {
            if cameraBurstState != cameraBurstStateInUse {
                cameraBurstStateInUse = cameraBurstState
            }
        }
    }
    var cameraBurstStateInUse = -1
    
    var videoTorchState: Int!
    
    var cameraFlashMode: AVCaptureDevice.FlashMode!
    let cameraFlashModeFromState: [Int: AVCaptureDevice.FlashMode] = [0: .off, 1: .auto, 2: .on]
    
    var selfTimerDuration: Int {
        get {
            if let duration = selfTimerDurationFromState[cameraSelfTimerState] {
                return duration
            } else {
                return 0
            }
        }
    }
    let selfTimerDurationFromState: [Int: Int] = [0: 0, 1: 3, 2: 10]
    
    var burstNumberOfExposures: Int {
        get {
            if let number = burstNumberOfExposuresFromState[cameraBurstState] {
                return number
            } else {
                return 1
            }
        }
    }
    let burstNumberOfExposuresFromState: [Int: Int] = [0: 1, 1: 3, 2: 10]
    
    var burstExposureCount = 0
    
    var durationDelay: Double = 1
    var deltaDurationDelayTarget: Double?
    
    var deviceHasFrontCamera = false
    var deviceHasBackCamera = false
    
    var deviceHasFlash = true {
        didSet {
            if !deviceHasFlash {
                cameraFlashState = 0
                userDefaults.set(0, forKey: cameraSettings.flashStateKey)
            }
        }
    }
    
    var deviceHasTorch = true {
        didSet {
            if !deviceHasTorch {
                videoTorchState = 0
                userDefaults.set(0, forKey: cameraSettings.torchStateKey)
            }
        }
    }
    
    var torchFlashIteration = 0
    var torchFlashIterations = 0
    
    // MARK: - Setup Stuff
    
    lazy var cameraSettings = CameraSettings()
    
    lazy var userDefaults: UserDefaults = UserDefaults.standard
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    override init() {
        super.init()
        photoLibraryHelper.delegate = self
        
        stillImageOutput.isHighResolutionCaptureEnabled = true
        
        checkCamerasAvailable()
        
        getCameraAspectRatioSetting()
        
        motionManager.accelerometerUpdateInterval = 0.2
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.deviceOrientationDidChange), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    // MARK: - Orientation & Permission Stuff
    
    func startMotionManager() {
        motionManager.startAccelerometerUpdates(to: OperationQueue()) { accelerometerData, _ in
            if let ad = accelerometerData {
                self.cameraOrientation =
                    abs( ad.acceleration.y ) < abs( ad.acceleration.x )
                    ?   ad.acceleration.x > 0 ? .landscapeRight  :   .landscapeLeft
                    :   ad.acceleration.y > 0 ? .portraitUpsideDown   :   .portrait
                //print("---  - cameraOrientation: \(cameraOrientation)")
            }
        }
    }
    
    func stopMotionManager() {
        motionManager.stopAccelerometerUpdates()
    }
    
    @objc func deviceOrientationDidChange() {
        //print("=== \(file).\(#function) === \(debugDate())")
        print("=== \(file).\(#function) === \(debugDate()) - deviceOrientation ent : \(deviceOrientation)")
        
        deviceOrientation.setFromDeviceOrientation()
        
        print("--- \(file).\(#function) === \(debugDate()) - deviceOrientation set : \(deviceOrientation)")
    }
    
    func requestAuthorization() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        AVCaptureDevice.requestAccess(for: .video) { response in
            OperationQueue.main.addOperation {
                if response {
                    self.isVideoAvailable = true
                } else {
                    self.isVideoAvailable = false
                }
            }
        }
        
        AVCaptureDevice.requestAccess(for: .audio) { response in
            OperationQueue.main.addOperation {
                if response {
                    self.isAudioAvailable = true
                } else {
                    self.isAudioAvailable = false
                }
            }
        }
        
        photoLibraryHelper.requestLibraryAuthorization()
    }
    
    func setIsCameraAvailableFromConstituents() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if isVideoAvailable {
            if AVCaptureDevice.default(for: .video) == nil {
                isVideoAvailable = false
            }
        }
        
        if isAudioAvailable {
            if AVCaptureDevice.default(for: .audio) == nil {
                isAudioAvailable = false
            }
        }
        
        isCameraAvailable = isVideoAvailable && isAudioAvailable && isPhotoLibraryAvailable
        print("--- \(file).\(#function) - video:\(isVideoAvailable), audio:\(isAudioAvailable), photoLib:\(isPhotoLibraryAvailable) - \(isCameraAvailable)")
    }
    
    // MARK: - User Settings Stuff
    
    func checkSettingsInUse() {
        print("=== \(file).\(#function) ===")
        getSettings()
    }
    
    func getSettings() {
        print("=== \(file).\(#function) === \(debugDate())")
        getCameraAspectRatioSetting()
        
        if deviceHasBackCamera && deviceHasFrontCamera {
            cameraFrontBackState = userDefaults.integer(forKey: cameraSettings.fontBackStateKey)
        } else if deviceHasFrontCamera {
            cameraFrontBackState = 0
        } else {
            cameraFrontBackState = 1
        }
        
        cameraFlashState = userDefaults.integer(forKey: cameraSettings.flashStateKey)
        cameraSelfTimerState = userDefaults.integer(forKey: cameraSettings.selfTimerStateKey)
        cameraBurstState = userDefaults.integer(forKey: cameraSettings.burstStateKey)
        videoTorchState = userDefaults.integer(forKey: cameraSettings.torchStateKey)
    }
    
    func getCameraAspectRatioSetting() {
        print("=== \(file).\(#function) === \(debugDate())")
        cameraAspectRatio = userDefaults.double(forKey: cameraSettings.aspectRatioKey)
    }
    
    // MARK: - Camera Stuff
    
    func checkCamerasAvailable() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        var deviceHasFlash = false
        var deviceHasTorch = false
        
        if let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) {
            deviceHasFrontCamera = true
            if device.hasFlash {
                deviceHasFlash = true
                //print("--- \(file).\(#function) - front hasFlash")
            }
            if device.hasTorch {
                deviceHasTorch = true
                //print("--- \(file).\(#function) - front hasTorch")
            }
        }
        
        if let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) {
            deviceHasBackCamera = true
            if device.hasFlash {
                deviceHasFlash = true
                //print("--- \(file).\(#function) - back hasFlash")
            }
            if device.hasTorch {
                deviceHasTorch = true
                //print("--- \(file).\(#function) - back hasTorch")
            }
        }
        
        self.deviceHasFlash = deviceHasFlash
        self.deviceHasTorch = deviceHasTorch
        
        print("--- \(file).\(#function) - deviceHasFrontCamera: \(deviceHasFrontCamera), deviceHasBackCamera: \(deviceHasBackCamera), deviceHasFlash: \(deviceHasFlash), deviceHasTorch: \(deviceHasTorch)")
    }
    
    func setupSession() -> Bool {
        
        captureSession.sessionPreset = AVCaptureSession.Preset.high
        
        // Setup Camera
        
        guard
            attachedCameraToSession()
            else {
                return false
        }
        
        // Setup Microphone
        
        guard
            let microphone = AVCaptureDevice.default(for: .audio)
            else {
                return false
        }
        
        do {
            let micInput = try AVCaptureDeviceInput(device: microphone)
            if captureSession.canAddInput(micInput) {
                captureSession.addInput(micInput)
            }
        } catch {
            print("Error setting device audio input: \(error)")
            return false
        }
        
        // Still output
        if captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }
        
        // Movie output
        if captureSession.canAddOutput(movieOutput) {
            captureSession.addOutput(movieOutput)
        }
        
        return true
    }
    
    func attachedCameraToSession() -> Bool {
        
        if videoInput != nil {
            captureSession.removeInput(videoInput)
        }
        
        let cameraPositionFromState: [Int: AVCaptureDevice.Position] = [0: .front, 1: .back]
        let position = cameraPositionFromState[cameraFrontBackState]!
        
        guard
            let camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: position)
            else {
                return false
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: camera)
            if captureSession.canAddInput(input) {
                captureSession.addInput(input)
                videoInput = input
            }
        } catch {
            print("Error setting device video input: \(error)")
            return false
        }
        
        return true
    }
    
    func startSession() {
        if !captureSession.isRunning {
            videoQueue().async {
                self.captureSession.startRunning()
            }
        }
    }
    
    func stopSession() {
        if captureSession.isRunning {
            videoQueue().async {
                self.captureSession.stopRunning()
            }
        }
    }
    
    func videoQueue() -> DispatchQueue {
        return DispatchQueue.main
    }
    
    func currentVideoOrientation() -> AVCaptureVideoOrientation {
        
        switch UIDevice.current.orientation {
        case .portrait:
            return .portrait
        case .landscapeRight:
            return .landscapeLeft
        case .portraitUpsideDown:
            return .portraitUpsideDown
        default:
            return .landscapeRight
        }
    }
    
    func tempURL() -> URL? {
        let directory = NSTemporaryDirectory() as NSString
        
        if directory != "" {
            let path = directory.appendingPathComponent(NSUUID().uuidString + ".mp4")
            return URL(fileURLWithPath: path)
        }
        
        return nil
    }
    
    func toggleFrontBackCamera() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if isBothCamerasAvailable {
            if cameraFrontBackState == 0 {
                cameraFrontBackState = 1
            } else {
                cameraFrontBackState = 0
            }
            userDefaults.set(cameraFrontBackState, forKey: cameraSettings.fontBackStateKey)
        }
    }
    
    func takePicture(isForPhotoInVideo: Bool) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if !isCameraAvailable {
            return
        }
        
        startMotionManager()
        
        delegate?.takingPicture()
        
        if let _ = stillImageOutput.connection(with: .video) {
            burstExposureCount = 0
            let delay = Double(0.5) / Double(burstNumberOfExposures)
            doCapturePhoto(isForPhotoInVideo: isForPhotoInVideo, delay: delay)
        }
    }
    
    func doCapturePhoto(isForPhotoInVideo: Bool, delay: Double) {
        print("=== \(file).\(#function) === \(debugDate()) - burstNumberOfExposures: \(burstNumberOfExposures), burstExposureCount: \(burstExposureCount)")
        
        let settings = AVCapturePhotoSettings()
        if !isForPhotoInVideo {
            settings.flashMode = cameraFlashMode
        }
        settings.isHighResolutionPhotoEnabled = true
        
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 160,
                             kCVPixelBufferHeightKey as String: 160]
        settings.previewPhotoFormat = previewFormat
        
        stillImageOutput.capturePhoto(with: settings, delegate: self)
        
        burstExposureCount += 1
        if burstExposureCount < burstNumberOfExposures {
            afterDelay(0.1) {
                self.doCapturePhoto(isForPhotoInVideo: isForPhotoInVideo, delay: delay)
            }
        }
    }
    
    func startRecording() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if !isCameraAvailable {
            return
        }
        
        if movieOutput.isRecording == false {
            delegate?.startedRecording()
            
            let connection = movieOutput.connection(with: .video)
            if (connection?.isVideoOrientationSupported)! {
                connection?.videoOrientation = currentVideoOrientation()
            }
            
            if (connection?.isVideoStabilizationSupported)! {
                connection?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
            }
            
            let device = videoInput.device
            if device.isSmoothAutoFocusSupported {
                do {
                    try device.lockForConfiguration()
                    device.isSmoothAutoFocusEnabled = false
                    device.unlockForConfiguration()
                } catch {
                    print("Error setting configuration: \(error)")
                }
            }
            
            if deviceHasTorch && videoTorchState > 0 {
                if videoTorchState == 1 {
                    setTorchMode(.auto, forDevice: device)
                } else {
                    setTorchModeOn(level: AVCaptureDevice.maxAvailableTorchLevel, forDevice: device)
                }
            }
            
            outputURL = tempURL()
            movieOutput.startRecording(to: outputURL, recordingDelegate: self)
            
        } else {
            stopRecording()
        }
    }
    
    func provideRecordedDuration() {
        if movieOutput.isRecording == true {
            let duration = CMTimeGetSeconds(movieOutput.recordedDuration)
            print("=== \(file).\(#function) === \(debugDate()) - duration: \(duration), durationDelay ent: \(durationDelay)")
            delegate?.recordingDurationIs(Float(duration))
            
            let deltaDurationDelay = duration - Double(Int(duration))
            
            if let deltaDurationDelayTarget = deltaDurationDelayTarget {
                
                let durationDelayDelta = (deltaDurationDelay - deltaDurationDelayTarget) * 0.4
                print("--- \(file).\(#function) - deltaDurationDelay: \(deltaDurationDelay), deltaDurationDelayTarget: \(deltaDurationDelayTarget), durationDelayDelta: \(durationDelayDelta)")
                
                durationDelay = durationDelay - durationDelayDelta
                if deltaDurationDelayTarget == 0 {
                    self.deltaDurationDelayTarget = deltaDurationDelay
                } else {
                    self.deltaDurationDelayTarget = 0.4 * deltaDurationDelay + 0.6 * deltaDurationDelayTarget
                }
                print("--- \(file).\(#function) --- \(debugDate()) - duration: \(duration), durationDelay set: \(durationDelay)")
                
            } else {
                deltaDurationDelayTarget = deltaDurationDelay
                print("--- \(file).\(#function) --- \(debugDate()) - deltaDurationDelay: \(deltaDurationDelay), deltaDurationDelayTarget: \(deltaDurationDelayTarget!)")
            }
            
            afterDelay(durationDelay) {
                self.provideRecordedDuration()
            }
        }
    }
    
    func stopRecording() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if deviceHasTorch && videoInput != nil && videoTorchState > 0 {
            setTorchMode(.off, forDevice: videoInput.device)
        }
        
        if movieOutput.isRecording == true {
            movieOutput.stopRecording()
        }
        
        delegate?.stoppedRecording()
    }
    
    func setTorchMode(_ torchMode: AVCaptureDevice.TorchMode, forDevice device: AVCaptureDevice) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if device.isTorchModeSupported(torchMode) {
            do {
                try device.lockForConfiguration()
                device.torchMode = torchMode
                device.unlockForConfiguration()
            } catch {
                print("Error:-\(error)")
            }
        }
    }
    
    func setTorchModeOn(level torchLevel: Float, forDevice device: AVCaptureDevice) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if device.isTorchModeSupported(.on) {
            do {
                try device.lockForConfiguration()
                do {
                    try device.setTorchModeOn(level: torchLevel)
                } catch {
                    print("Error:-\(error)")
                }
                device.unlockForConfiguration()
            } catch {
                print("Error:-\(error)")
            }
        }
    }
    
    func provideSelfTimerFeedback(duration: Int, timeLeft: Int) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        delegate?.selfTimerIsRunning(duration: duration, timeLeft: timeLeft)
        
        if !isCameraAvailable || !deviceHasTorch {
            return
        }
        
        let device = videoInput.device
        
        if timeLeft > 1 {
            flashTheTorch(duration: 0.1, iterations: 1, level: 0.2, forDevice: device)
            
        } else {
            flashTheTorch(duration: 0.05, iterations: 3, level: 0.2, forDevice: device)
        }
    }
    
    func flashTheTorch(duration: Double, iterations: Int, level: Float, forDevice device: AVCaptureDevice) {
        torchFlashIteration = 0
        torchFlashIterations = iterations
        flashTheTorch(duration: duration, level: level, forDevice: device)
    }
    
    func flashTheTorch(duration: Double, level: Float, forDevice device: AVCaptureDevice) {
        print("=== \(file).\(#function) === \(debugDate()) - torchFlashIteration: \(torchFlashIteration)")
        setTorchModeOn(level: level, forDevice: device)
        afterDelay(duration) {
            self.setTorchMode(.off, forDevice: device)
            self.torchFlashIteration += 1
            if self.torchFlashIteration < self.torchFlashIterations {
                afterDelay(duration) {
                    self.flashTheTorch(duration: duration, level: level, forDevice: device)
                }
            }
        }
    }
}

// MARK: - AVCapturePhotoCaptureDelegate

extension Camera: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if let error = error {
            print("Error capturing still: \(error.localizedDescription)")
        }
        
        if let imageData = photo.fileDataRepresentation(),
            let imageCaptured = UIImage(data: imageData) {
            
            var width: CGFloat!
            var height: CGFloat!
            
            if imageCaptured.size.height / imageCaptured.size.width > camPreviewAspectRatio {
                width = imageCaptured.size.width
                height = width * camPreviewAspectRatio
            } else {
                height = imageCaptured.size.width
                width = height / camPreviewAspectRatio
            }
            
            /*
             print("---  - camPreviewAspectRatio: \(camPreviewAspectRatio)")
             print("---  - capturedH: \(imageCaptured.size.height)")
             print("---  - capturedW: \(imageCaptured.size.width)")
             print("---  -    height: \(height)")
             print("---  -     width: \(width)")
             */
            
            // note: the CGImage coordinates are rotated 90 deg
            cropArea.origin = CGPoint(x: (imageCaptured.size.height - height) / 2.0, y: 0.0)
            cropArea.size = CGSize(width: height, height: width)
            
            print("---  - cropArea: \(cropArea)")
            
            if let cgImage = imageCaptured.cgImage?.cropping(to: cropArea) {
                let image = UIImage(cgImage: cgImage, scale: 1.0, orientation: cameraOrientation.imageOrientation(cameraFrontBackState: cameraFrontBackState))
                
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAsset(from: image)
                }) { saved, error in
                    if error != nil {
                        print("Error saving image: \(error!.localizedDescription)")
                        OperationQueue.main.addOperation {
                            self.delegate?.showCameraAlert(title: "Error", message: "There was an error saving the image.")
                        }
                    }
                    if saved {
                        print("--- \(self.file).\(#function) - image saved to library")
                        OperationQueue.main.addOperation {
                            self.delegate?.savedImage(image)
                        }
                    }
                }
            }
        }
    }
}

// MARK: - AVCaptureFileOutputRecordingDelegate

extension Camera: AVCaptureFileOutputRecordingDelegate {
    
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        //provideRecordedDuration()
        afterDelay(0.5) {
            self.provideRecordedDuration()
        }
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if error != nil {
            print("Error recording video: \(error!.localizedDescription)")
            OperationQueue.main.addOperation {
                self.delegate?.showCameraAlert(title: "Error", message: "There was an error recording video.")
            }
            
        } else {
            PHPhotoLibrary.shared().performChanges({
                let createAssetRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputFileURL)
                let assetPlaceholder = createAssetRequest?.placeholderForCreatedAsset
                self.assetCreatedIdentifier = assetPlaceholder?.localIdentifier
            }) { saved, error in
                if error != nil {
                    print("Error saving video: \(error!.localizedDescription)")
                    OperationQueue.main.addOperation {
                        self.delegate?.showCameraAlert(title: "Error", message: "There was an error saving the video.")
                    }
                }
                if saved {
                    print("--- \(self.file).\(#function) - video saved to library")
                    self.delegate?.videoSavedToLibrary(id: self.assetCreatedIdentifier)
                }
            }
        }
        outputURL = nil
    }
}

// MARK: - PhotoLibraryHelperDelegate

extension Camera: PhotoLibraryHelperDelegate {
    
    func handleDidSetIsPhotoLibraryAvailable(_ isPhotoLibraryAvailable: Bool) {
        print("=== \(file).\(#function) === \(debugDate()) - isPhotoLibraryAvailable: \(isPhotoLibraryAvailable)")
        
        self.isPhotoLibraryAvailable = isPhotoLibraryAvailable
    }
    
    func handleAssetWasDeleted() {
        print("=== \(file).\(#function) === \(debugDate()) - isPhotoLibraryAvailable: \(isPhotoLibraryAvailable)")
        
        delegate?.handleAssetWasDeleted()
    }
}
