
import UIKit

class SettingsSwitchCell: UITableViewCell {
    
    let userDefaults = UserDefaults.standard
    var switchName = ""
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var theSwitch: UISwitch!
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        print("=== switchValueChanged - switchName=\(switchName)")
        userDefaults.set(sender.isOn, forKey: switchName)
        userDefaults.synchronize()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String, switchName: String, enabled: Bool = true) {
        selectionStyle = UITableViewCellSelectionStyle.none
        self.switchName = switchName
        
        label.text = text
        if enabled {
            label.textColor = UIColor.black
        } else {
            label.textColor = UIColor.lightGray
        }
        
        let switchVal = userDefaults.bool(forKey: switchName)
        theSwitch.setOn(switchVal, animated:true)
        theSwitch.isEnabled = enabled
        theSwitch.onTintColor = Colors.switchOn
    }
}
