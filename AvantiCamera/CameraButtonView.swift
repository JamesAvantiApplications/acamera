//
//  CameraButtonView.swift
//  AvantiCamera
//
//  Created by James Hager on 4/5/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

public protocol CameraButtonViewDelegate {
    func didTapOnCameraButtonView(_ view: CameraButtonView)
}

// MARK: -

public enum CameraButtonState: String, CustomStringConvertible {
    case inactive
    
    case photo
    case photoSelfTimer
    case photoSelfTimerRunning
    case photoInterval
    case photoIntervalRunning
    
    case video
    case videoRunning
    
    public var description: String { return rawValue }
    
    var outerColor: CGColor {
        switch self {
        case .inactive:
            return Colors.inactive.cgColor
        default :
            return Colors.lightLightGray.cgColor
        }
    }
    
    var outerForeIsHidden: Bool {
        switch self {
        case .photoSelfTimerRunning, .videoRunning:
            return false
        default :
            return true
        }
    }
    
    var centerColor: CGColor {
        switch self {
        case .inactive:
            return Colors.inactive.cgColor
        case .video, .videoRunning :
            return Colors.cameraButtonRed.cgColor
        default :
            return Colors.lightLightGray.cgColor
        }
    }
    
    var startIsHidden: Bool {
        switch self {
        case .photoSelfTimerRunning, .photoIntervalRunning, .videoRunning:
            return true
        default :
            return false
        }
    }
}

// MARK: -

public class CameraButtonView: UIView {
    
    var videoTestCount = 0
    let videoTestCountToRun = 20
    var t1: Date!
    
    var camera: Camera!
    
    var isForPhotoInVideo = false
    
    var delegate: CameraButtonViewDelegate?
    
    var cameraButtonState: CameraButtonState = .inactive {
        didSet {
            if cameraButtonState != cameraButtonStateSet {
                cameraButtonStateSet = cameraButtonState
                setButton()
            }
        }
    }
    var cameraButtonStateSet: CameraButtonState = .inactive
    
    let outerCircleLineWidth = CGFloat(4)
    let outerInnerGap = CGFloat(2)
    let deltaFrameForRed = CGFloat(0.33)
    
    let outerBackgroundLayer = CAShapeLayer()
    let outerForegroundLayer = CAShapeLayer()
    let startLayer = CAShapeLayer()
    let stopLayer = CAShapeLayer()
    
    var circlePathHelper: CirclePathHelper!
    
    class CirclePathHelper {
        var centerPoint: CGPoint
        var circleRadius: CGFloat
        
        let piOverTwo = CGFloat(Double.pi / 2)
        let twoPi = CGFloat(Double.pi * 2)
        
        init(bounds: CGRect, outerCircleLineWidth: CGFloat) {
            centerPoint = CGPoint(x: bounds.midX, y: bounds.midY)
            circleRadius = (min(bounds.width, bounds.height) - outerCircleLineWidth) / 2
        }
        
        func cgPath(startAngle startAngleIn: CGFloat) -> CGPath {
            let startAngle = -CGFloat(Double.pi / 2) + startAngleIn
            let endAngle = CGFloat(Double.pi * 2) + startAngle
            
            return UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: startAngle, endAngle: endAngle, clockwise: true).cgPath
        }
    }
    
    var selfTimerImageView: UIImageView!
    var outerCircleView: UIView!
    
    var isTimerRunning = false
    var timerDuration: Double = 0
    var timerTimeCompleted: Double = 0 {
        didSet {
            print("=== \(file).\(#function) didSet === \(debugDate()) - timerTimeCompleted: \(timerTimeCompleted)")
            var progress: CGFloat!
            if timerDuration > 0 {
                progress = CGFloat(timerTimeCompleted / timerDuration)
            } else {
                progress = 0
            }
            if cameraButtonState == .photoSelfTimerRunning {
                outerForegroundLayer.strokeEnd = progress
            } else {
                let angle = 2 * CGFloat(Double.pi) * progress
                print("--- \(file).\(#function) didSet - progress: \(progress!), angle: \(angle)")
                
                //outerForegroundLayer.path = circlePathHelper.cgPath(startAngle: angle)
                
                outerCircleView.transform = CGAffineTransform(rotationAngle: angle)
            }
        }
    }
    var timerSegments = 3
    var timerSegment = 0
    var timerSubsegments = 5
    var timerSubsegment = 0
    var timerSubsegmentDuration: Double = 0.1
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    // MARK: - Setup
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        configure()
    }
    
    func configure() {
        print("=== \(file).\(#function) === \(debugDate()) - isUserInteractionEnabled: \(isUserInteractionEnabled)")
        print("--- \(file).\(#function) --- \(debugDate()) - bounds: \(bounds)")
        
        outerBackgroundLayer.lineWidth = outerCircleLineWidth
        outerBackgroundLayer.strokeColor = Colors.lightLightGray.cgColor
        outerBackgroundLayer.strokeEnd = 1
        outerBackgroundLayer.fillColor = UIColor.clear.cgColor
        layer.addSublayer(outerBackgroundLayer)
        
        outerForegroundLayer.lineWidth = outerCircleLineWidth
        outerForegroundLayer.strokeColor = Colors.cameraButtonRed.cgColor
        outerForegroundLayer.strokeEnd = 0
        outerForegroundLayer.fillColor = UIColor.clear.cgColor
        layer.addSublayer(outerForegroundLayer)
        
        startLayer.lineWidth = 0
        layer.addSublayer(startLayer)
        
        stopLayer.lineWidth = 0
        layer.addSublayer(stopLayer)
        
        setButton()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnView))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func setState(_ state: CameraButtonState) {
        print("=== \(file).\(#function) === \(debugDate()) - state............: \(state)")
        
        if isForPhotoInVideo {
            if state == .inactive {
                cameraButtonState = .inactive
            } else {
                cameraButtonState = .photo
            }
        } else {
            if state == .photo {
                if camera.selfTimerDuration > 0 {
                    cameraButtonState = .photoSelfTimer
                } else {
                    cameraButtonState = state
                }
            } else {
                cameraButtonState = state
            }
        }
        print("--- \(file).\(#function) --- \(debugDate()) - cameraButtonState: \(cameraButtonState)")
    }
    
    func setButton() {
        print("=== \(file).\(#function) === \(debugDate()) - cameraButtonState: \(cameraButtonState)")
        
        outerBackgroundLayer.strokeColor = cameraButtonState.outerColor
        
        outerForegroundLayer.isHidden = cameraButtonState.outerForeIsHidden
        
        if cameraButtonState == .videoRunning {
            outerForegroundLayer.strokeEnd = 0.1
        } else {
            outerForegroundLayer.strokeEnd = 0
        }
        
        startLayer.fillColor = cameraButtonState.centerColor
        startLayer.isHidden = cameraButtonState.startIsHidden
        
        stopLayer.fillColor = startLayer.fillColor
        stopLayer.isHidden = !startLayer.isHidden
        
        if startLayer.fillColor != outerBackgroundLayer.strokeColor {
            startLayer.frame = bounds.offsetBy(dx: deltaFrameForRed, dy: deltaFrameForRed)
            stopLayer.frame = bounds.offsetBy(dx: deltaFrameForRed, dy: deltaFrameForRed)
        } else {
            startLayer.frame = bounds
            stopLayer.frame = bounds
        }
        
        if cameraButtonState == .photoSelfTimer {
            if selfTimerImageView == nil {
                setUpSelfTimerImageView()
            }
            selfTimerImageView?.isHidden = false
        } else {
            selfTimerImageView?.isHidden = true
        }
        
        if cameraButtonState == .video && outerCircleView == nil {
            setUpOuterCircleView()
        }
        
        if cameraButtonState == .videoRunning {
            startTimer(duration: 1)
        } else if cameraButtonState == .video {
            isTimerRunning = false
            afterDelay(0.5) {
                self.outerCircleView.transform = CGAffineTransform(rotationAngle: 0)
            }
        }
    }
    
    func setUpSelfTimerImageView() {
        print("=== \(file).\(#function) === \(debugDate())")
        if let image = UIImage(named: "SelfTimer") {
            selfTimerImageView = UIImageView(image: image)
            selfTimerImageView.contentMode = .scaleAspectFit
            
            selfTimerImageView.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(selfTimerImageView)
            
            selfTimerImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
            selfTimerImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            
            let delta = (outerCircleLineWidth + outerInnerGap) * 2
            let frame = bounds.insetBy(dx: delta, dy: delta)
            
            selfTimerImageView.widthAnchor.constraint(equalToConstant: frame.size.width).isActive = true
            selfTimerImageView.heightAnchor.constraint(equalToConstant: frame.size.height).isActive = true
        }
    }
    
    func setUpOuterCircleView() {
        outerCircleView = UIView(frame: CGRect.zero)
        outerCircleView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(outerCircleView)
        
        outerCircleView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        outerCircleView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        outerCircleView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        outerCircleView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        outerForegroundLayer.removeFromSuperlayer()
        outerCircleView.layer.addSublayer(outerForegroundLayer)
    }
    
    func outerFrame() -> CGRect {
        let delta = outerCircleLineWidth / 2
        let frame = bounds.insetBy(dx: delta, dy: delta)
        print("=== \(file).\(#function) === \(debugDate()) - frame: \(frame)")
        return frame
    }
    
    func startFrame() -> CGRect {
        let delta = outerCircleLineWidth + outerInnerGap
        let frame = bounds.insetBy(dx: delta, dy: delta)
        print("=== \(file).\(#function) === \(debugDate()) - frame: \(frame)")
        return frame
    }
    
    func stopFrame() -> CGRect {
        let delta = bounds.size.width * 0.3
        let frame = bounds.insetBy(dx: delta, dy: delta)
        print("=== \(file).\(#function) === \(debugDate()) - frame: \(frame)")
        return frame
    }
    
    /*
     func circlePath(startAngle startAngleIn: CGFloat) -> UIBezierPath {
     let centerPoint = CGPoint(x: bounds.midX, y: bounds.midY)
     let circleRadius = (min(bounds.width, bounds.height) - outerCircleLineWidth) / 2
     let startAngle = -CGFloat(Double.pi / 2) + startAngleIn
     let endAngle = CGFloat(Double.pi * 2) + startAngle
     
     return UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
     }
     */
    
    func circlePath() -> UIBezierPath {
        let centerPoint = CGPoint(x: bounds.midX, y: bounds.midY)
        let circleRadius = (min(bounds.width, bounds.height) - outerCircleLineWidth) / 2
        let piOverTwo = CGFloat(Double.pi / 2)
        let startAngle = -piOverTwo
        let endAngle = CGFloat(Double.pi * 2) + startAngle
        
        return UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        print("=== \(file).\(#function) === \(debugDate()) - bounds: \(bounds)")
        
        if circlePathHelper == nil {
            circlePathHelper = CirclePathHelper(bounds: bounds, outerCircleLineWidth: outerCircleLineWidth)
        }
        
        outerBackgroundLayer.frame = bounds
        //outerBackgroundLayer.path = circlePath(startAngle: 0.0).cgPath
        outerBackgroundLayer.path = circlePathHelper.cgPath(startAngle: 0.0)
        
        outerForegroundLayer.frame = outerBackgroundLayer.frame
        outerForegroundLayer.path = outerBackgroundLayer.path
        
        startLayer.frame = bounds
        startLayer.path = UIBezierPath(ovalIn: startFrame()).cgPath
        
        stopLayer.frame = bounds
        stopLayer.path = UIBezierPath(roundedRect: stopFrame(), cornerRadius: 4).cgPath
    }
    
    // MARK: - Actions
    
    @objc func didTapOnView() {
        print("=== \(file).\(#function) === \(debugDate())")
        //delegate?.didTapOnCameraButtonView(self)
        
        /*
         switch cameraButtonState {
         case .inactive:
         cameraButtonState = .photo
         
         case .photo:
         cameraButtonState = .photoSelfTimer
         case .photoSelfTimer:
         cameraButtonState = .photoSelfTimerRunning
         case .photoSelfTimerRunning:
         cameraButtonState = .photoInterval
         case .photoInterval:
         cameraButtonState = .photoIntervalRunning
         case .photoIntervalRunning:
         cameraButtonState = .video
         
         case .video:
         cameraButtonState = .videoRunning
         case .videoRunning:
         cameraButtonState = .inactive
         }
         */
        
        /*
         switch cameraButtonState {
         case .inactive:
         cameraButtonState = .photo
         
         case .photo:
         cameraButtonState = .photoSelfTimerRunning
         case .photoSelfTimerRunning:
         cameraButtonState = .video
         
         case .video:
         cameraButtonState = .videoRunning
         case .videoRunning:
         cameraButtonState = .inactive
         
         default: break
         }
         */
        
        /*
         cameraButtonState = .photoSelfTimerRunning
         startTimer(duration: 10)
         */
        switch cameraButtonState {
        case .inactive:
            break
        case .photo:
            camera.takePicture(isForPhotoInVideo: isForPhotoInVideo)
            
        case .photoSelfTimer:
            cameraButtonState = .photoSelfTimerRunning
            startTimer(duration: camera.selfTimerDuration)
        case .photoSelfTimerRunning:
            cameraButtonState = .photoSelfTimer
            isTimerRunning = false
            
        case .photoInterval:
            break
        case .photoIntervalRunning:
            break
            
        case .video:
            camera.startRecording()
            
            /*
             videoTestCount = 0
             t1 = Date()
             cameraButtonState = .videoRunning
             startTimer(duration: 1)
             */
        case .videoRunning:
            camera.stopRecording()
            
            //cameraButtonState = .video
        }
    }
    
    func startTimer(duration: Int) {
        // duration is in seconds
        print("=== \(file).\(#function) === \(debugDate()) - duration: \(duration)")
        
        timerDuration = Double(duration)
        
        timerTimeCompleted = 0
        timerSegments = duration - 1
        timerSegment = 0
        timerSubsegment = 0
        
        if cameraButtonState == .photoSelfTimerRunning {
            timerSubsegments = 6
        } else {
            timerSubsegments = 12
        }
        print("--- \(file).\(#function) - timerSubsegments: \(timerSubsegments)")
        
        timerSubsegmentDuration = Double(1) / Double(timerSubsegments)
        
        print("--- \(file).\(#function) - timerSubsegmentDuration: \(timerSubsegmentDuration)")
        
        isTimerRunning = true
        if cameraButtonState == .photoSelfTimerRunning {
            provideSelfTimerFeedback()
        }
        doSubTimer()
    }
    
    func doSubTimer() {
        print("=== \(file).\(#function) === \(debugDate()) - timerTimeCompleted: \(timerTimeCompleted), timerSegment: \(timerSegment), timerSubsegment: \(timerSubsegment)")
        
        timerTimeCompleted = Double(timerSegment) + Double(timerSubsegment + 1) * timerSubsegmentDuration
        
        afterDelay(timerSubsegmentDuration) {
            if self.isTimerRunning {
                self.timerSubsegment += 1
                if self.timerSubsegment < self.timerSubsegments {
                    self.doSubTimer()
                } else if self.timerSegment < self.timerSegments {
                    self.timerSegment += 1
                    self.timerSubsegment = 0
                    if self.cameraButtonState == .photoSelfTimerRunning {
                        self.provideSelfTimerFeedback()
                    }
                    self.doSubTimer()
                } else {
                    self.timerComplete()
                }
            }
        }
    }
    
    func timerComplete() {
        print("=== \(file).\(#function) === \(debugDate())")
        if cameraButtonState == .photoSelfTimerRunning {
            cameraButtonState = .photoSelfTimer
            camera.takePicture(isForPhotoInVideo: isForPhotoInVideo)
        } else if cameraButtonState == .videoRunning {
            /*
             videoTestCount += 1
             if videoTestCount == videoTestCountToRun {
             let t2 = Date()
             let dt = t2.timeIntervalSince(t1)
             let at = dt - Double(videoTestCountToRun)
             print("--- \(file).\(#function) - dt: \(dt), animation time: \(at)")
             return
             }
             */
            //camera.provideRecordedDuration()
            startTimer(duration: 1)
        }
    }
    
    func provideSelfTimerFeedback() {
        let duration = timerSegments + 1
        let timeLeft = duration - timerSegment
        camera.provideSelfTimerFeedback(duration: duration, timeLeft: timeLeft)
    }
}
