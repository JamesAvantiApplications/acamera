//
//  CoreDataStack+CreateObjects.swift
//  ACamera
//
//  Created by James Hager on 4/15/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import CoreData

extension CoreDataStack {
    
    func makeNewSpeechCommand(cameraAction: CameraAction) {
        print("=== \(file).\(#function) ===")
        makeSpeechCommand(cameraAction: cameraAction, verb: "", object: "")
    }
    
    func makeSpeechCommand(cameraAction: CameraAction, verb: String, object: String, andSaveContext doSaveContext: Bool = true) {
        print("=== \(file).\(#function) ===")
        
        let speechCommandEntity = NSEntityDescription.entity(forEntityName: "SpeechCommand", in: context)
        let speechCommand = SpeechCommand(entity: speechCommandEntity!, insertInto: context)
        speechCommand.setUp(cameraAction: cameraAction, verb: verb, object: object)
        if doSaveContext {
            saveContext()
        }
    }
    
    func deleteAllSpeechCommands(andSaveContext doSaveContext: Bool = true) {
        print("=== \(file).\(#function) ===")
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SpeechCommand")
        
        do {
            let speechCommands = try context.fetch(fetchRequest) as! [SpeechCommand]
            
            for speechCommand in speechCommands {
                context.delete(speechCommand)
            }
            
            if doSaveContext {
                saveContext()
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func deleteSpeechCommands(cameraAction: CameraAction) {
        print("=== \(file).\(#function) - cameraAction: \(cameraAction)")
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SpeechCommand")
        fetchRequest.predicate = NSPredicate(format: "cameraActionSave == %d", cameraAction.rawValue)
        
        do {
            let speechCommands = try context.fetch(fetchRequest) as! [SpeechCommand]
            
            for speechCommand in speechCommands {
                context.delete(speechCommand)
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
}
