//
//  PhotoLibrarySelectableCell.swift
//  ACamera
//
//  Created by James Hager on 4/14/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import Photos

class PhotoLibrarySelectableCell: UICollectionViewCell {
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                showSelected()
            } else {
                hideSelected()
            }
        }
    }
    
    var selectedView: UIView!
    var imageView: ACImageView!
    
    var marginInCell: CGFloat!
    var subviewConstraints = [NSLayoutConstraint]()
    
    /*
     override func awakeFromNib() {
     super.awakeFromNib()
     contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
     }
     */
    
    func configureCell(image: UIImage, mediaType: PHAssetMediaType, videoIndicatorSize: CGFloat, marginInCell: CGFloat) {
        //print("=== PhotoLibraryThumbCell.configureCell - imageViewTag: \(imageViewTag)")
        
        self.marginInCell = marginInCell
        self.tag = tag
        
        if imageView == nil {
            contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
            setUpSelectedView()
            setUpImageView()
        }
        
        imageView.image = image
        
        setSubviewConstraints()
        
        imageView.videoIndicatorSize = videoIndicatorSize
        imageView.mediaType = mediaType
    }
    
    func setUpSelectedView() {
        
        selectedView = UIView(frame: CGRect.zero)
        selectedView.backgroundColor = UIColor.red
        selectedView.isHidden = !isSelected
        
        selectedView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(selectedView)
        
        selectedView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        selectedView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    }
    
    func setUpImageView() {
        
        imageView = ACImageView(frame: CGRect.zero)
        //imageView.contentMode = .scaleAspectFit
        
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.black.cgColor
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageView)
        
        imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    }
    
    func setSubviewConstraints() {
        NSLayoutConstraint.deactivate(subviewConstraints)
        subviewConstraints = [NSLayoutConstraint]()
        
        let imageSize = imageView.image!.size
        
        subviewConstraints.append(selectedView.widthAnchor.constraint(equalToConstant: imageSize.width + marginInCell))
        subviewConstraints.append(selectedView.heightAnchor.constraint(equalToConstant: imageSize.height + marginInCell))
        
        subviewConstraints.append(imageView.widthAnchor.constraint(equalToConstant: imageSize.width))
        subviewConstraints.append(imageView.heightAnchor.constraint(equalToConstant: imageSize.height))
        
        NSLayoutConstraint.activate(subviewConstraints)
        
        contentView.layoutIfNeeded()
    }
    
    func showSelected() {
        selectedView?.isHidden = false
    }
    
    func hideSelected() {
        selectedView?.isHidden = true
    }
}
