//
//  PolygonPath.swift
//  AvantiCamera
//
//  Created by James Hager on 4/5/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  Based on http://sketchytech.blogspot.com/2014/11/swift-drawing-regular-polygons-with.html
//

import UIKit

func polygonPointArray(sides: Int, origin: CGPoint, radius: CGFloat, offset: CGFloat) -> [CGPoint] {
    // radius = radius of circle
    
    let angle = (360/CGFloat(sides)).radians()
    let offsetInRadians = offset.radians()
    
    var i = 0
    var points = [CGPoint]()
    while i <= sides {
        let x = origin.x + radius * cos(angle * CGFloat(i) - offsetInRadians)
        let y = origin.y + radius * sin(angle * CGFloat(i) - offsetInRadians)
        points.append(CGPoint(x: x, y: y))
        i += 1
    }
    return points
}

func polygonPath(sides: Int, origin: CGPoint, radius: CGFloat, offset: CGFloat) -> CGPath {
    let path = CGMutablePath()
    let points = polygonPointArray(sides: sides, origin: origin, radius: radius, offset: offset)
    var isFirst = true
    for point in points {
        if isFirst {
            path.move(to: point)
            isFirst = false
        } else {
            path.addLine(to: point)
        }
    }
    path.closeSubpath()
    return path
}
