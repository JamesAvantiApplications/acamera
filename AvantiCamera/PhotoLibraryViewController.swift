//
//  PhotoLibraryViewController.swift
//  AvantiCamera
//
//  Created by James Hager on 3/25/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import Photos

class PhotoLibraryViewController: UIViewController {
    
    var photoLibraryHelper: PhotoLibraryHelper!
    
    var isStatusBarHidden = true
    
    override var prefersStatusBarHidden: Bool {
        return  isStatusBarHidden
    }
    
    var isFirstWillAppear = true
    var doHandleWillAppear = true
    var updateAssets = false
    
    var doAdjustLayout1st = false
    var doAdjustLayout2nd = false
    
    var sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    
    var collectionViewScrolling = -1
    var isHandlingDidSelect = false
    var isDidScrollDetectImageActive = [true, true]
    
    var indexPathSelected = IndexPath(row: 0, section: 0) {
        didSet {
            print("=== \(file).\(#function) didSet === \(debugDate()) - collectionViewScrolling=\(collectionViewScrolling) \(displayPath(indexPathSelected))")
            
            setTitle(index: indexPathSelected.row)
            
            if isHandlingDidSelect {
                collectionView[0].scrollToItem(at: indexPathSelected, at: .centeredHorizontally, animated: false)
                collectionView[1].scrollToItem(at: indexPathSelected, at: .centeredHorizontally, animated: true)
            } else {
                if isDidScrollDetectImageActive[0] {
                    collectionView[1].scrollToItem(at: indexPathSelected, at: .centeredHorizontally, animated: true)
                } else if isDidScrollDetectImageActive[1] {
                    collectionView[0].scrollToItem(at: indexPathSelected, at: .centeredHorizontally, animated: false)
                } else {
                    collectionView[0].scrollToItem(at: indexPathSelected, at: .centeredHorizontally, animated: false)
                    collectionView[1].scrollToItem(at: indexPathSelected, at: .centeredHorizontally, animated: false)
                }
            }
        }
    }
    
    var collectionView: [UICollectionView]!
    
    var collectionViewImageSize = [CGSize(width: 200, height: 200), CGSize(width: 56, height: 56)]
    
    var videoIndicatorSize: [CGFloat] = [70, 10]
    
    var isSmallDevice = true
    var actionButton: UIBarButtonItem!
    
    var photoViewController: PhotoViewController!
    var photoLibraryMultipleViewController: PhotoLibraryMultipleViewController!
    
    struct CollectionViewCellIdentifiers {
        static let thumbCell = "PhotoLibraryThumbCell"
    }
    
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long //.medium
        dateFormatter.timeStyle = .none //.short
        dateFormatter.doesRelativeDateFormatting = true
        return dateFormatter
    }()
    
    lazy var timeFormatter: DateFormatter = {
        let timeFormatter = DateFormatter()
        timeFormatter.dateStyle = .none
        timeFormatter.timeStyle = .short
        return timeFormatter
    }()
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    // MARK: - IB Stuff
    @IBOutlet weak var previewCollectionView: UICollectionView!
    @IBOutlet weak var thumbContainerView: UIView!
    @IBOutlet weak var thumbCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("=========================================")
        print("=== \(file).\(#function) === \(debugDate())")
        print("=========================================")
        
        navigationItem.title = "Photo Library"
        
        let horizontalSizeClass = traitCollection.horizontalSizeClass
        let verticalSizeClass = traitCollection.verticalSizeClass
        print("--- \(file).\(#function) - horizontalSizeClass = \(horizontalSizeClass) / \(horizontalSizeClass.rawValue)")
        print("--- \(file).\(#function) - verticalSizeClass   = \(verticalSizeClass) / \(verticalSizeClass.rawValue)")
        
        isSmallDevice = min(horizontalSizeClass.rawValue,verticalSizeClass.rawValue) == 1
        
        print("--- \(file).\(#function) - isSmallDevice: \(isSmallDevice)")
        
        let trashButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(handleTrash))
        actionButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(handleAction))
        
        navigationItem.setRightBarButtonItems([trashButton, actionButton], animated: false)
        
        collectionView = [previewCollectionView, thumbCollectionView]
        
        let cellNib = UINib(nibName: CollectionViewCellIdentifiers.thumbCell, bundle: nil)
        
        for collectionView in collectionView {
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
            collectionView.register(cellNib, forCellWithReuseIdentifier: CollectionViewCellIdentifiers.thumbCell)
        }
        
        setCollectionViewImageSize0()
        print("--- \(file).\(#function) - collectionViewImageSize[0]: \(collectionViewImageSize[0])")
        
        //collectionView[0].backgroundColor = .gray
        collectionView[1].backgroundColor = Colors.reallyLightGray
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeUp))
        swipeUp.direction = .up
        previewCollectionView.addGestureRecognizer(swipeUp)
        
        thumbContainerView.backgroundColor = Colors.reallyLightGray
        thumbContainerView.layer.borderWidth = 1
        thumbContainerView.layer.borderColor = UIColor.lightGray.cgColor
        
        setDisplay(size: view.frame.size)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("-----------------------------------------")
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation())")
        print("-----------------------------------------")
        
        setTitle(index: indexPathSelected.row)
        
        setDisplay(size: view.frame.size)
        
        if isFirstWillAppear {
            isFirstWillAppear = false
            view.layoutIfNeeded()
        }
        
        setCollectionViewImageSize0()
        print("--- \(file).\(#function) - collectionViewImageSize[0]: \(collectionViewImageSize[0])")
        
        handleWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("=== \(file).\(#function) === \(debugDate())")
        showInfoAlert()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("=== \(file).\(#function) === \(debugDate()) - height=\(size.height), width=\(size.width)")
        
        super.viewWillTransition(to: size, with: coordinator)
        
        setDisplay(size: size)
        
        doAdjustLayout1st = true
        doAdjustLayout2nd = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("=== \(file).\(#function) === \(debugDate()) - height=\(view.frame.size.height), width=\(view.frame.size.width)")
        
        if doAdjustLayout1st || doAdjustLayout2nd {
            if doAdjustLayout1st {
                doAdjustLayout1st = false
            } else if doAdjustLayout2nd {
                doAdjustLayout2nd = false
            }
            
            setCollectionViewImageSize0()
            print("--- \(file).\(#function) - collectionViewImageSize[0]: \(collectionViewImageSize[0])")
            
            guard let flowLayout0 = collectionView[0].collectionViewLayout as? UICollectionViewFlowLayout,
                let flowLayout1 = collectionView[1].collectionViewLayout as? UICollectionViewFlowLayout
                else { return }
            
            flowLayout0.invalidateLayout()
            flowLayout1.invalidateLayout()
            
            DispatchQueue.main.async {
                self.collectionView[0].scrollToItem(at: self.indexPathSelected, at: .centeredHorizontally, animated: false)
                self.collectionView[1].scrollToItem(at: self.indexPathSelected, at: .centeredHorizontally, animated: false)
            }
        }
    }
    
    //MARK:- Setup
    
    func setDisplay(size: CGSize) {
        print("=== \(file).\(#function) === \(debugDate()) - size: \(size)")
        
        if size.height > size.width {
            setDisplayForPort()
        } else {
            setDisplayForLand()
        }
        
        //if size.height > size.width && view.safeAreaInsets != UIEdgeInsets.zero {
        if size.height > size.width {
            isStatusBarHidden = false
        } else {
            isStatusBarHidden = true
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func setDisplayForPort() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if let date = photoLibraryHelper.getDateForAsset(at: indexPathSelected.row) {
            setTitleForPort(date: date)
        }
    }
    
    func setDisplayForLand() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if let date = photoLibraryHelper.getDateForAsset(at: indexPathSelected.row) {
            setTitleForLand(date: date)
        }
    }
    
    func setTitle(index: Int) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if let date = photoLibraryHelper.getDateForAsset(at: index) {
            if view.frame.size.height > view.frame.size.width {
                setTitleForPort(date: date)
            } else {
                setTitleForLand(date: date)
            }
        } else {
            navigationItem.title = "Photo Library"
        }
    }
    
    func setTitleForPort(date: Date) {
        print("=== \(file).\(#function) === \(debugDate())")
        let titleView: TitleForPort = UIView.fromNib()
        titleView.setTitle(date: date, dateFormatter: dateFormatter, timeFormatter: timeFormatter)
        navigationItem.titleView = titleView
    }
    
    func setTitleForLand(date: Date) {
        print("=== \(file).\(#function) === \(debugDate())")
        let titleView: TitleForLand = UIView.fromNib()
        titleView.setTitle(date: date, dateFormatter: dateFormatter, timeFormatter: timeFormatter)
        navigationItem.titleView = titleView
    }
    
    func setCollectionViewImageSize0() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        collectionViewImageSize[0] = collectionView[0].frame.size
        
        let newVideoIndicatorSize = min(collectionView[0].frame.size.height * 0.18, 70)
        if newVideoIndicatorSize != videoIndicatorSize[0] {
            videoIndicatorSize[0] = newVideoIndicatorSize
            let userInfo = ["videoIndicatorSize" : newVideoIndicatorSize]
            NotificationCenter.default.post(name: videoIndicatorSizeShouldChangeNotificationName, object: nil, userInfo: userInfo)
        }
        //print("--- \(file).\(#function) --- \(debugDate()) - videoIndicatorSize[0]: \(videoIndicatorSize[0])")
    }
    
    //MARK:- Actions
    
    func handleWillAppear() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if updateAssets {
            photoLibraryHelper.getAssets()
            updateAssets = false
        }
        
        displayPhotosAndVideos()
        
        doHandleWillAppear = false
    }
    
    func showInfoAlert() {
        
        let userDefaults = UserDefaults.standard
        let firstPhotoLibraryViewControllerAlertWasShown = userDefaults.bool(forKey: "FirstPhotoLibraryViewControllerAlertWasShown")
        
        if !firstPhotoLibraryViewControllerAlertWasShown {
            userDefaults.set(true, forKey: "FirstPhotoLibraryViewControllerAlertWasShown")
            userDefaults.synchronize()
            let title = "Large Preview Gestures"
            let message = "Tap to switch to a full-image view.\n\nTwo-finger tap to switch to a multiple-image selection view.\n\nSwipe left/right to go to another item.\n\nSwipe up to delete the item."
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Got It", style: .default, handler: nil)
            alert.addAction(okAction)
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func handleAction() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        photoLibraryHelper.getInAppFileURLToShare(at: indexPathSelected.row) { url in
            if let url = url {
                print("--- \(self.file).\(#function) - url: \(url)")
                ActionHelper.presentAction(urls: [url], actionButton: self.actionButton, viewController: self)
            }
        }
    }
    
    @objc func handleTrash() {
        print("=== \(file).\(#function) === \(debugDate())")
        deleteImage()
    }
    
    func deleteImage() {
        print("=== \(file).\(#function) === \(debugDate())")
        let indexSelected = indexPathSelected.row
        if indexSelected > -1 && indexSelected < photoLibraryHelper.photoAssets.count {
            doHandleWillAppear = false
            
            photoLibraryHelper.deleteAssets([photoLibraryHelper.photoAssets[indexSelected]]) { deleted, error in
                if error != nil {
                    print("Error deleting image: \(error!.localizedDescription)")
                }
                if deleted {
                    print("--- \(self.file).\(#function) - completionHandler: image deleted from library")
                    OperationQueue.main.addOperation {
                        self.getPhotosAndVideos()
                    }
                } else {
                    print("--- \(self.file).\(#function) - completionHandler: image not deleted from library")
                }
            }
        }
    }
    
    @objc func handleSwipeUp() {
        print("=== \(file).\(#function) === \(debugDate())")
        deleteImage()
    }
    
    func getPhotosAndVideos() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        photoLibraryHelper.getAssets()
        displayPhotosAndVideos()
    }
    
    func displayPhotosAndVideos() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        print("--- \(self.file).\(#function) - call collectionView.reloadData()")
        previewCollectionView.reloadData()
        thumbCollectionView.reloadData()
        setTitle(index: indexPathSelected.row)
    }
    
    func showPhotoViewController(index: Int, doPlayOnEntry: Bool = false) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if photoViewController == nil {
            photoViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
            photoViewController.photoLibraryHelper = photoLibraryHelper
            photoViewController.delegate = self
        }
        
        photoViewController.indexOnEntry = index
        photoViewController.doPlayOnEntry = doPlayOnEntry
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController?.pushViewController(photoViewController, animated: false)
    }
    
    func showPhotoLibraryMultipleViewController() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if photoLibraryMultipleViewController == nil {
            photoLibraryMultipleViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotoLibraryMultipleViewController") as! PhotoLibraryMultipleViewController
            photoLibraryMultipleViewController.photoLibraryHelper = photoLibraryHelper
            photoLibraryMultipleViewController.delegate = self
            photoLibraryMultipleViewController.isSmallDevice = isSmallDevice
        }
        
        photoLibraryMultipleViewController.indexOnEntry = indexPathSelected.row
        
        if isSmallDevice {
            navigationItem.title = ""
        } else {
            navigationItem.title = "Back"
        }
        navigationController?.pushViewController(photoLibraryMultipleViewController, animated: false)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension PhotoLibraryViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("=== \(file).\(#function) tag: \(collectionView.tag) \(displayPath(indexPath)) === \(debugDate())")
        
        if collectionView.tag == 0 {
            return collectionViewImageSize[0]
        }
        
        var size = CGSize.zero
        
        photoLibraryHelper.getImageSize(at: indexPath.row, targetSize: collectionViewImageSize[1]) { imageSize in
            if let imageSize = imageSize {
                size = imageSize
            }
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        print("=== \(file).\(#function) tag: \(collectionView.tag) === \(debugDate())")
        
        if collectionView.tag == 0 {
            return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        }
        
        if photoLibraryHelper.photoAssets.count == 0 {
            return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        }
        
        var visibleRect = CGRect.zero
        visibleRect.size = collectionView.frame.size
        
        let maxDim = CGFloat(60)
        
        var width = CGFloat(0)
        
        // get inset for first image
        photoLibraryHelper.getImage(at: 0, imageSize: collectionViewImageSize[1]) { image, mediaType in
            if let image = image {
                let imageSize = image.size
                let scale = min(maxDim/imageSize.width, maxDim/imageSize.height)
                width = imageSize.width * scale
            }
            self.sectionInsets.left = visibleRect.midX - width / 2
        }
        
        // get inset for last image
        if photoLibraryHelper.photoAssets.count > 1 {
            photoLibraryHelper.getImage(at: photoLibraryHelper.photoAssets.count - 1, imageSize: collectionViewImageSize[1]) { image, mediaType in
                if let image = image {
                    let imageSize = image.size
                    let scale = min(maxDim/imageSize.width, maxDim/imageSize.height)
                    width = imageSize.width * scale
                }
                self.sectionInsets.right = visibleRect.midX - width / 2
            }
            
        } else {
            self.sectionInsets.right = visibleRect.midX - width / 2
        }
        
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        print("=== \(file).\(#function) tag: \(collectionView.tag) === \(debugDate())")
        /*
         print("--- \(file).\(#function) tag: \(collectionView.tag) -         contentInset: \(collectionView.contentInset)")
         print("--- \(file).\(#function) tag: \(collectionView.tag) - adjustedContentInset: \(collectionView.adjustedContentInset)")
         */
        
        if collectionView.tag == 0 {
            return CGFloat(0)
        } else {
            return CGFloat(1)
        }
    }
}

// MARK: - UICollectionViewDataSource

extension PhotoLibraryViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        print("=== \(file).\(#function) === \(debugDate())")
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let numberOfItems = photoLibraryHelper.photoAssets.count
        print("=== \(file).\(#function) -> \(numberOfItems) === \(debugDate())")
        return numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("=== \(file).\(#function) tag: \(collectionView.tag) \(displayPath(indexPath)) === \(debugDate())")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCellIdentifiers.thumbCell, for: indexPath) as! PhotoLibraryThumbCell
        
        let imageSize = collectionViewImageSize[collectionView.tag]
        //print("--- \(file).\(#function) tag: \(collectionView.tag) \(displayPath(indexPath)) - imageSize: \(imageSize!)")
        
        photoLibraryHelper.getImage(at: indexPath.row, imageSize: imageSize) { image, mediaType in
            if let image = image {
                
                var imageViewTag: Int!
                if collectionView.tag == 0 {
                    imageViewTag = indexPath.row
                } else {
                    imageViewTag = -1
                }
                
                //print("--- \(self.file).\(#function) tag: \(collectionView.tag) \(displayPath(indexPath)) - imageViewTag: \(imageViewTag!)")
                
                cell.configureCell(image: image, mediaType: mediaType, videoIndicatorSize: self.videoIndicatorSize[collectionView.tag], imageViewTag: imageViewTag, imageViewDelegate: self)
                //print("--- \(self.file).\(#function) tag: \(collectionView.tag) \(displayPath(indexPath)) - completionHandler: image size \(image.size)")
            }
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegate

extension PhotoLibraryViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("=== \(file).\(#function) tag: \(collectionView.tag) \(displayPath(indexPath)) === \(debugDate())")
        
        if collectionView.tag == 0 {
            showPhotoViewController(index: indexPath.row)
            
        } else {
            if indexPathSelected != indexPath {
                isHandlingDidSelect = true
                indexPathSelected = indexPath
                isDidScrollDetectImageActive[1] = false
            }
        }
    }
}

// MARK: - UIScrollViewDelegate

extension PhotoLibraryViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("=== \(file).\(#function) tag: \(scrollView.tag) === \(debugDate()) - isDidScrollDetectImageActive: \(isDidScrollDetectImageActive)")
        
        if isDidScrollDetectImageActive[scrollView.tag] {
            let collectionView = self.collectionView[scrollView.tag]
            
            let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
            
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            print("--- \(file).\(#function) tag: \(collectionView.tag) - visiblePoint: \(visiblePoint)")
            
            guard let indexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
            
            print("--- \(file).\(#function) tag: \(collectionView.tag) - center indexPath: \(displayPath(indexPath))")
            
            if indexPathSelected != indexPath {
                indexPathSelected = indexPath
                print("--- \(file).\(#function) tag: \(collectionView.tag) - new indexPathSelected: \(displayPath(indexPathSelected))")
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("=== \(file).\(#function) tag: \(scrollView.tag) === \(debugDate()) - isDidScrollDetectImageActive: \(isDidScrollDetectImageActive)")
        
        thumbCollectionView.scrollToItem(at: indexPathSelected, at: .centeredHorizontally, animated: true)
        
        isDidScrollDetectImageActive[0] = false
        isDidScrollDetectImageActive[1] = false
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("=== \(file).\(#function) tag: \(scrollView.tag) === \(debugDate()) - isDidScrollDetectImageActive: \(isDidScrollDetectImageActive)")
        
        isHandlingDidSelect = false
        collectionViewScrolling = scrollView.tag
        
        isDidScrollDetectImageActive[0] = scrollView.tag == 0
        isDidScrollDetectImageActive[1] = scrollView.tag == 1
    }
    
    /*
     func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
     print("=== \(file).\(#function) tag: \(scrollView.tag) === \(debugDate()) - isDidScrollDetectImageActive: \(isDidScrollDetectImageActive)")
     
     //isDidScrollDetectImageActive[0] = false
     //isDidScrollDetectImageActive[1] = false
     
     /*
     if scrollView.tag == 1 && isDidScrollDetectImageActive[1] {
     thumbCollectionView.scrollToItem(at: indexPathSelected, at: .centeredHorizontally, animated: true)
     }
     */
     //collectionViewScrolling = -1
     }
     */
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("=== \(file).\(#function) tag: \(scrollView.tag) === \(debugDate()) - isDidScrollDetectImageActive: \(isDidScrollDetectImageActive)")
        
        if scrollView.tag == 1 && isDidScrollDetectImageActive[1] {
            thumbCollectionView.scrollToItem(at: indexPathSelected, at: .centeredHorizontally, animated: true)
        }
        
        isDidScrollDetectImageActive[0] = false
        isDidScrollDetectImageActive[1] = false
    }
}

// MARK: - ACImageViewDelegate

extension PhotoLibraryViewController: ACImageViewDelegate {
    
    func didMultiTouchOnACImageView(_ acImageView: ACImageView) {
        print("=== \(file).\(#function) === \(debugDate()) - tag: \(acImageView.tag)")
        showPhotoLibraryMultipleViewController()
    }
    
    func didTapOnVideoIndicatorView(in acImageView: ACImageView) {
        print("=== \(file).\(#function) === \(debugDate()) - tag: \(acImageView.tag)")
        showPhotoViewController(index: acImageView.tag, doPlayOnEntry: true)
    }
}

// MARK: - PhotoViewControllerDelegate

extension PhotoLibraryViewController: PhotoViewControllerDelegate {
    
    func closePhotoViewController(scrollToIndex index: Int?, didDeleteAsset: Bool) {
        print("=== \(file).\(#function) === \(debugDate())")
        navigationController?.popViewController(animated: false)
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        if didDeleteAsset {
            getPhotosAndVideos()
        }
        
        if let index = index {
            isDidScrollDetectImageActive[0] = false
            isDidScrollDetectImageActive[1] = false
            indexPathSelected = IndexPath(row: index, section: 0)
        }
    }
}

// MARK: - PhotoLibraryMultipleViewControllerDelegate

extension PhotoLibraryViewController: PhotoLibraryMultipleViewControllerDelegate {
    
    func photoLibraryMultipleViewControllerWillDisappear(scrollToIndexPath indexPath: IndexPath?, didDeleteAsset: Bool) {
        print("=== \(file).\(#function) === \(debugDate())")
        
        if didDeleteAsset {
            getPhotosAndVideos()
        }
        
        if let indexPath = indexPath {
            isDidScrollDetectImageActive[0] = false
            isDidScrollDetectImageActive[1] = false
            indexPathSelected = indexPath
        }
    }
}
