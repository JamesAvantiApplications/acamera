//
//  SpeechCommand+CoreDataProperties.swift
//  ACamera
//
//  Created by James Hager on 4/15/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation
import CoreData

extension SpeechCommand {
    
    @NSManaged public var cameraActionSave: Int16
    @NSManaged public var object: String
    @NSManaged public var verb: String
}
