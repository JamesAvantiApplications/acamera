//
//  ActionHelper.swift
//  ACamera
//
//  Created by James Hager on 4/14/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

class ActionHelper {
    
    static func presentAction(urls: [URL], actionButton: UIBarButtonItem, viewController: UIViewController) {
        print("=== ActionHelper.\(#function) === \(debugDate())")
        
        let activityViewController = UIActivityViewController(activityItems: urls, applicationActivities: nil)
        
        /*
         activityViewController.excludedActivityTypes = [
         UIActivityType.postToTwitter,
         UIActivityType.postToFacebook,
         UIActivityType.postToWeibo,
         UIActivityType.message,
         //UIActivityType.mail,
         UIActivityType.print,
         UIActivityType.copyToPasteboard,
         UIActivityType.assignToContact,
         UIActivityType.saveToCameraRoll,
         UIActivityType.addToReadingList,
         UIActivityType.postToFlickr,
         UIActivityType.postToVimeo,
         UIActivityType.postToTencentWeibo
         ]
         */
        activityViewController.excludedActivityTypes = [
            UIActivityType.saveToCameraRoll
        ]
        
        activityViewController.completionWithItemsHandler = {(activityType, completed, returnedItems, error) in
            
            for url in urls {
                do {
                    try FileManager.default.removeItem(at: url)
                    print("removed url \(url)")
                } catch {
                    print("could not remove url \(url)")
                }
            }
        }
        
        activityViewController.popoverPresentationController?.barButtonItem = actionButton
        
        viewController.present(activityViewController, animated: true, completion: nil)
    }
}
