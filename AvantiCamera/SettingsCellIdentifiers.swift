//
//  SettingsCellIdentifiers.swift
//  Weather_NWS
//
//  Created by James Hager on 3/15/16.
//  Copyright © 2016 James Hager. All rights reserved.
//

enum SettingsCellIdentifiers: String {
    case bulletItemCell = "SettingsBulletItemCell"
    case legalCell = "SettingsLegalCell"
    case sectionCell = "SettingsSectionCell"
    case segmentedControlCell = "SettingsSegmentedControlCell"
    case switchCell = "SettingsSwitchCell"
    case textBuyRestoreCell = "SettingsTextBuyRestoreCell"
    case textCell = "SettingsTextCell"
    case textSmallCell = "SettingsTextSmallCell"
    case valueCell = "SettingsValueCell"
}
