//
//  ConnectedToNetwork.swift
//  ANotes
//
//  Created by James Hager on 11/30/17.
//  Copyright © 2017 James Hager. All rights reserved.
//
//  func connectedToNetwork from http://stackoverflow.com/questions/25623272/how-to-use-scnetworkreachability-in-swift/25623647#25623647
//

import Foundation
import UIKit

import SystemConfiguration

public func connectedToNetwork() -> Bool {
    
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
            SCNetworkReachabilityCreateWithAddress(nil, $0)
        }
    }) else {
        return false
    }
    
    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    
    return (isReachable && !needsConnection)
}

public func notConnectedToNetwork(message: String?) -> Bool {
    
    if !connectedToNetwork() {
        #if os(iOS) || os(watchOS) || os(tvOS)
            var userInfo = [String : String]()
            userInfo["title"] = "You Are Offline"
            if let message = message {
                userInfo["message"] = message
            }
            NotificationCenter.default.post(name: presentSimpleNoticeNotificationName, object: nil, userInfo: userInfo)
            
        #elseif os(OSX)
            showNotConnectedToNetwork(message: message)
            
        #endif
        return true
    }
    
    return false
}
    
public func showNotConnectedToNetwork(_ viewController: UIViewController, refreshControl: UIRefreshControl? = nil) {
    print("=== ConnectedToNetwork.showNotConnectedToNetwork(_:refreshControl:) ===")
    let alert = UIAlertController(title: "You Are Offline",
                                  message: "You must be connected to a network to use voice control.",
                                  preferredStyle: .alert)
    
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alert.addAction(okAction)
    
    if let refreshControl = refreshControl {
        viewController.present(alert, animated: true) {
            refreshControl.endRefreshing()
        }
    } else {
        viewController.present(alert, animated: true, completion: nil)
    }
}
