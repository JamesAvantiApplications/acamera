//
//  UIView+fromNib.swift
//  AvantiCamera
//
//  Created by James Hager on 3/31/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  From https://stackoverflow.com/questions/24857986/load-a-uiview-from-nib-in-swift
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
