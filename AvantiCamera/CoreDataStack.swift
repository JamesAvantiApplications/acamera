//
//  CoreDataStack.swift
//  ACamera
//
//  Created by James Hager on 4/15/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//

import CoreData

open class CoreDataStack: NSObject {
    
    deinit{
        print("=== \(file).\(#function) ===")
        NotificationCenter.default.removeObserver(self)
    }
    
    let modelName = "ACameraData"
    
    var modelDirectoryURL: URL!
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    lazy open var context: NSManagedObjectContext = {
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.psc
        return managedObjectContext
    }()
    
    lazy open var psc: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent(self.modelName)
        
        do {
            let options = [NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true]
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch  {
            print("Error adding persistent store.")
        }
        
        return coordinator
    }()
    
    fileprivate lazy var managedObjectModel: NSManagedObjectModel = {
        print("self.modelName='\(self.modelName)'")
        let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd")!
        /*
         let aNotesBundle = Bundle(identifier: "com.AvantiApplications.ANotesKit")
         let modelURL = aNotesBundle!.url(forResource: self.modelName, withExtension: "momd")!
         */
        print("modelURL='\(modelURL)'")
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    private lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    open func saveContext() {
        print("<<< saveContext >>>")
        if context.hasChanges {
            do {
                try context.save()
            } catch let error as NSError {
                print("Error: \(error.localizedDescription)")
                abort()
            }
        }
    }
}
