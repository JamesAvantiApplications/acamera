//
//  CameraSettings.swift
//  AvantiCamera
//
//  Created by James Hager on 3/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation
import ImageIO
import AVFoundation

class CameraSettings {
    
    // Camera Settings
    
    let fontBackStateKey = "CameraFrontBackState"
    let fontBackStateDefault = 1
    let fontBackStates = ["front", "back"]
    
    // Photo Settings
    
    let aspectRatioKey = "CameraAspectRatio"
    let aspectRatioDefault = 1.5
    let aspectRatios = [
        [16.0 / 9.0, "16x9 (video)"],
        [1.5, "2x3 (4x6)"],
        [1.4, "5x7"],
        [4.0 / 3.0, "3x4 (native)"],
        [1.25, "4x5 (8x10)"],
        [1.0, "square"]
    ]
    
    let flashStateKey = "CameraFlashState"
    let flashStateDefault = 1
    let flashStates = ["off", "auto", "on"]
    
    let selfTimerStateKey = "CameraSelfTimerState"
    let selfTimerStateDefault = 0
    let selfTimerStates = ["off", "3s", "10s"]
    
    let burstStateKey = "CameraBurstState"
    let burstStateDefault = 0
    let burstStates = ["off", "3", "10"]
    
    // Video Settings
    
    let torchStateKey = "VideoTorchState"
    let torchStateDefault = 1
    let torchStates = ["off", "auto", "on"]
    
    // Export Formats
    
    let photoFormatKey = "PhotoExportFormat"
    let photoFormatDefault = "jpg"
    var photoFormats = [
        ["jpg", "JPEG"],
        ["png", "PNG"],
        ["tif", "TIFF"]
    ]
    
    let photoColorSpaceKey = "PhotoExportColorSpace"
    let photoColorSpaceDefault = "sRGB"
    let photoColorSpaces = [
        ["adobe98", "Adobe 1998"],
        ["sRGB", "sRGB"]
    ]
    
    let photoImageQualityKey = "PhotoExportImageQuality"
    let photoImageQualityDefault = "1.0"
    let photoImageQualities = [
        [1.0, "Max Quality"],
        [0.9, "90%"],
        [0.8, "80%"],
        [0.7, "70%"],
        [0.6, "60%"],
        [0.5, "50%"],
        [0.4, "40%"],
        [0.3, "30%"],
        [0.2, "20%"],
        [0.1, "10%"],
        [0.0, "Max Compression"]
    ]
    
    let videoFormatKey = "VideoExportFormat"
    let videoFormatDefault = "mp4"
    let videoFormats = [
        ["mov", "MOV"],
        ["mp4", "MP4"]
    ]
    
    init() {
        if deviceSupports(avFileType: .heic) {
            let heif = ["heif", "HEIF"]
            photoFormats.insert(heif, at: 0)
        }
    }
    
    func deviceSupports(avFileType: AVFileType) -> Bool {
        let supportedTypes = CGImageDestinationCopyTypeIdentifiers() as NSArray
        return supportedTypes.contains(avFileType)
    }
}
