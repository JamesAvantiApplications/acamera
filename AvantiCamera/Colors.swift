//
//  Colors.swift
//  AvantiCamera
//
//  Created by James Hager on 3/22/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

struct Colors {
    static let avantiGreen = UIColor(red: 0.0, green: 84/255.0, blue: 73/255.0, alpha: 1.0)
    static let mediumGreen = UIColor(red: 0.24, green: 0.48, blue: 0.44, alpha: 1.0)
    static let lightGreen = UIColor(red: 215/255.0, green: 229/255.0, blue: 227/255.0, alpha: 1.0)
    static let text = UIColor.white
    static let textSelectable = UIColor(red: 230/255.0, green: 160/255.0, blue: 12/255.0, alpha: 1.0)
    static let switchOn = UIColor(red: 0.49, green: 0.63, blue: 0.60, alpha: 1.0)
    static let inactive = UIColor(red: 0.33, green: 0.33, blue: 0.33, alpha: 1.0)
    static let lighterGray = UIColor(red: 0.75, green: 0.75, blue: 0.75, alpha: 1.0)
    static let lightLightGray = UIColor(red: 220/255.0, green: 220/255.0, blue: 220/255.0, alpha: 1.0)
    static let reallyLightGray = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
    static let cameraButtonRed = UIColor(red: 0.85, green: 0, blue: 0, alpha: 1.0)
}
