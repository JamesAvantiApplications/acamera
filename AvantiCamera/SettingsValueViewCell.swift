//
//  SettingsValueViewCell.swift
//  Weather_NWS
//
//  Created by James Hager on 3/15/16.
//  Copyright © 2016 James Hager. All rights reserved.
//

import UIKit

class SettingsValueViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String) {
        
        label.text = text
        
    }
}
