
import UIKit

class SettingsTextSmallCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String) {
        label.text = text
    }
}
