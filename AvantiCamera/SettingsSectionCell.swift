
import UIKit

class SettingsSectionCell: UITableViewCell {
    
    lazy var file: String = getSourceFileNameFromFullPath(#file)
    
    @IBOutlet weak var sectionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String) {
        //print("=== \(file).\(#function) - text=\(text) *** ***")
        backgroundColor = Colors.avantiGreen
        selectionStyle = UITableViewCellSelectionStyle.none
        
        sectionLabel.text = text
        sectionLabel.textColor = Colors.text
        
        /*
        let attributes = [
        NSTextEffectAttributeName : NSTextEffectLetterpressStyle
        ]
        let attributedString = NSAttributedString(string: text, attributes: attributes)
        sectionLabel.attributedText = attributedString
        */
    }
}
