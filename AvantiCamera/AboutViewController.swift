//
//  AboutViewController.swift
//  AvantiCamera
//
//  Created by James Hager on 3/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
//import StoreKit

class AboutViewController: SettingsViewControllerGeneral {
    
    var camera: Camera!
    
    var fileLocal = "AboutViewController"
    
    override func initSettingsDataItems() {
        print("=== \(fileLocal).\(#function) ===")
        var settingsDataItem: SettingsDataItem!
        
        /*
         settingsDataItem = SettingsDataItem(dataType: .upgradeToFullVersion)
         addSettingsDataItem(settingsDataItem)
         */
        
        // Camera
        
        settingsDataItem = SettingsDataItem(text: "Camera", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
        
        var text = "Tap on the camera/video icon to toggle photo/video capture."
        if camera.deviceHasBackCamera && camera.deviceHasFrontCamera {
            text += " Long-press on the camera/video icon to toggle the front/back camera."
        }
        settingsDataItem = SettingsDataItem(text: text)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Tap on the image preview to go to the photo library viewer.")
        addSettingsDataItem(settingsDataItem)
        
        // Speech Recognition
        
        settingsDataItem = SettingsDataItem(text: "Speech Recognition", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Use speech recognition to control the camera. Tap on the microphone icon, then use voice commands to:")
        addSettingsDataItem(settingsDataItem)
        
        let cameraActions: [CameraAction] = [
            .setPhoto, .takePhoto,
            .setVideo, .startVideo, .stopVideo,
            .stopSpeech
        ]
        
        for cameraAction in cameraActions {
            settingsDataItem = SettingsDataItem(dataType: .bulletItem, text: cameraAction.command)
            addSettingsDataItem(settingsDataItem)
        }
        
        settingsDataItem = SettingsDataItem(text: "You can customize the voice commands from the Settings. Tap on a command to edit it.")
        addSettingsDataItem(settingsDataItem)
        
        // Photo Library Viewer
        
        settingsDataItem = SettingsDataItem(text: "Photo Library Viewer", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "In the main view:")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(dataType: .bulletItem, text: "Tap on the large preview to switch to a full-image view.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(dataType: .bulletItem, text: "Two-finger tap on the large preview to switch to a multiple-image selection view.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(dataType: .bulletItem, text: "Swipe left/right to go to another item.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(dataType: .bulletItem, text: "Swipe up to delete the item.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "In the full-image view:")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(dataType: .bulletItem, text: "Pinch to zoom and scroll the item.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(dataType: .bulletItem, text: "Tap to close the view.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(dataType: .bulletItem, text: "If the whole item is visible, swipe left/right to go to the next item or swipe up to delete the item.")
        addSettingsDataItem(settingsDataItem)
        
        // Legal
        
        settingsDataItem = SettingsDataItem(text: "Legal", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(legalItem: "Graphics Icons", desc: "Some icons are from www.flaticon.com and were designed by Freepik.")
        addSettingsDataItem(settingsDataItem)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("=== \(fileLocal).\(#function) ===")
        navigationItem.title = "About"
        tableView.separatorColor = UIColor.clear
    }
    
    override func rowToShowForIndexPath(_ indexPath: IndexPath) -> Int {
        /*
         var rowToShow: Int!
         if vectorClockHelper.accessControl.licenseState.access == .full {
         rowToShow = settingsDataItemsIndexForIAP[indexPath.row]!
         } else {
         rowToShow = indexPath.row
         }
         */
        let rowToShow = indexPath.row
        print("=== \(file).\(#function) \(displayPath(indexPath)) -> \(rowToShow) ===")
        return rowToShow
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*
         var numberOfRows: Int!
         if vectorClockHelper.accessControl.licenseState.access == .full {
         let lastIndex = settingsDataItemsIndexForIAP.count - 1
         print("=== \(file).\(#function) - lastIndex \(lastIndex) ===")
         numberOfRows = lastIndex - (settingsDataItemsIndexForIAP[lastIndex]! - lastIndex) + 1
         } else {
         numberOfRows =  settingsDataItems.count
         }
         */
        let numberOfRows =  settingsDataItems.count
        print("=== \(file).\(#function) -> \(numberOfRows) ===")
        return numberOfRows
    }
}
