//
//  SettingsDataItem.swift
//  Weather_NWS
//
//  Created by James Hager on 3/15/16.
//  Copyright © 2016 James Hager. All rights reserved.
//

import UIKit

class SettingsDataItem {
    
    var dataType: DataType
    var text: String!
    var text2: String!
    var textFromClosure: (() -> String)!
    var cellType: SettingsCellIdentifiers
    var disclosureIndicator: Bool
    var closure: (() -> Void)!
    var segmentedControlName: String!
    var segmentWidth: CGFloat?
    var switchName: String!
    var enabledOnClosure: (() -> Bool)!
    var valueName: String!
    var valueType: ValueType!
    var values: AnyObject!
    
    enum DataType {
        case bulletItem
        case legal
        case regular
        case segmentedControl
        case `switch`
        case upgradeToFullVersion
        case valuePicker
        case version
    }
    
    convenience init(text: String) {
        self.init(text: text, cellType: .textCell)
    }
    
    convenience init(text: String, closure: (() -> Void)!) {
        self.init(text: text, cellType: .textCell, disclosureIndicator: true, closure: closure)
    }
    
    convenience init(legalItem: String, desc: String) {
        self.init(text: legalItem, cellType: .legalCell)
        self.text2 = desc
    }
    
    init(textFromClosure closure: @escaping (() -> String), cellType: SettingsCellIdentifiers, disclosureIndicator: Bool) {
        self.dataType = .regular
        self.textFromClosure = closure
        self.cellType = cellType
        self.disclosureIndicator = disclosureIndicator
    }
    
    convenience init(dataType: DataType) {
        switch dataType{
        case .version:
            self.init(dataType: dataType, cellType: .textSmallCell, disclosureIndicator: false)
        case .upgradeToFullVersion:
            self.init(dataType: dataType, cellType: .textBuyRestoreCell, disclosureIndicator: false)
        default:
            self.init(text: "blank", cellType: .textCell)
        }
    }
    
    init(dataType: DataType, text: String) {
        self.dataType = dataType
        self.text = text
        self.cellType = .bulletItemCell
        self.disclosureIndicator = false
    }
    
    init(text: String, segmentedControlName: String, values: AnyObject, segmentWidth: CGFloat?) {
        self.dataType = .segmentedControl
        self.text = text
        self.cellType = .segmentedControlCell
        self.disclosureIndicator = false
        self.segmentedControlName = segmentedControlName
        self.values = values
        self.segmentWidth = segmentWidth
    }
    
    convenience init(text: String, segmentedControlName: String, values: AnyObject, segmentWidth: CGFloat?, enabledOnClosure closure: @escaping (() -> Bool)) {
        self.init(text: text, segmentedControlName: segmentedControlName, values: values, segmentWidth: segmentWidth)
        self.enabledOnClosure = closure
    }
    
    init(text: String, switchName: String) {
        self.dataType = .switch
        self.text = text
        self.cellType = .switchCell
        self.disclosureIndicator = false
        self.switchName = switchName
    }
    
    convenience init(text: String, switchName: String, enabledOnClosure closure: @escaping (() -> Bool)) {
        self.init(text: text, switchName: switchName)
        self.enabledOnClosure = closure
    }
    
    init(text: String, valueName: String, valueType: ValueType, values: AnyObject) {
        self.dataType = .valuePicker
        self.text = text
        self.cellType = .valueCell
        self.disclosureIndicator = true
        self.valueName = valueName
        self.valueType = valueType
        self.values = values
    }
    
    init(text: String, cellType: SettingsCellIdentifiers) {
        self.dataType = .regular
        self.text = text
        self.cellType = cellType
        self.disclosureIndicator = false
    }
    
    init(text: String, cellType: SettingsCellIdentifiers, disclosureIndicator: Bool, closure: (() -> Void)!) {
        self.dataType = .regular
        self.text = text
        self.cellType = cellType
        self.disclosureIndicator = disclosureIndicator
        self.closure = closure
    }
    
    init(dataType: DataType, cellType: SettingsCellIdentifiers, disclosureIndicator: Bool) {
        self.dataType = dataType
        self.cellType = cellType
        self.disclosureIndicator = disclosureIndicator
    }
}
