//
//  CameraMode.swift
//  AvantiCamera
//
//  Created by James Hager on 3/21/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

enum CameraMode: String, CustomStringConvertible {
    case photo
    case video
    
    public var description: String { return rawValue }
}
