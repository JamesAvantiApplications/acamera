//
//  NavigationController.swift
//  AvantiCamera
//
//  Created by James Hager on 4/3/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    
    var viewControllerWillShow: UIViewController?
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation())")
        //return (visibleViewController?.supportedInterfaceOrientations)!
        if let viewControllerWillShow = viewControllerWillShow {
            print("--- \(file).\(#function) --- \(debugDate()) - supportedOrientations: \(describeUIInterfaceOrientationMask(viewControllerWillShow.supportedInterfaceOrientations))")
            return viewControllerWillShow.supportedInterfaceOrientations
        } else {
            print("--- \(file).\(#function) --- \(debugDate()) - supportedOrientations: [.portrait]")
            return [.portrait]
        }
    }
}
