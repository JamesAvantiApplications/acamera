//
//  UITextField+clearButtonWithImage.swift
//  ACamera
//
//  Created by James Hager on 4/16/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  Based on https://danilovdev.blogspot.com/2017/03/create-custom-uitextfield-and-custom.html
//

import UIKit

extension UITextField {
    
    func clearButtonWithImage(_ image: UIImage, color: UIColor) {
        let clearButton = UIButton()
        clearButton.tintColor = color
        let coloredImage = image.withRenderingMode(.alwaysTemplate)
        clearButton.setImage(coloredImage, for: .normal)
        clearButton.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(self.clear(sender:)), for: .touchUpInside)
        self.rightView = clearButton
        //self.rightViewMode = mode
    }
    
    @objc func clear(sender: AnyObject) {
        self.text = ""
    }
    
}
