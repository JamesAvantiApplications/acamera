//
//  ValueType.swift
//  Weather_NWS
//
//  Created by James Hager on 3/15/16.
//  Copyright © 2016 James Hager. All rights reserved.
//

import Foundation

enum ValueType {
    case double
    case int
    case string
}