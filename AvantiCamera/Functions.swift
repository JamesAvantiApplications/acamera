//
//  Functions.swift
//  AvantiCamera
//
//  Created by James Hager on 3/21/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

func afterDelay(_ seconds: Double, closure: @escaping () -> ()) {
    let when = DispatchTime.now() + Double(Int64(seconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

var debugDateDateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .none
    dateFormatter.timeStyle = .medium
    return dateFormatter
}()

func debugDate() -> String {
    return debugDateDateFormatter.string(from: Date())
}

func describeOrientation() -> String {
    switch UIDevice.current.orientation {
    case .unknown:
        return ".unknown"
    case .portrait:
        return ".portrait"
    case .portraitUpsideDown:
        return ".portraitUpsideDown"
    case .landscapeLeft:
        return ".landscapeLeft"
    case .landscapeRight:
        return ".landscapeRight"
    case .faceUp:
        return ".faceUp"
    case .faceDown:
        return ".faceDown"
    default:
        return "_other_"
    }
}

func describeUIInterfaceOrientationMask(_ orientationMask: UIInterfaceOrientationMask) -> String {
    var returnString = "["
    var first = true
    
    if orientationMask.contains(.all) {
        returnString += ".all"
        
    } else {
        if orientationMask.contains(.portrait) {
            first = false
            returnString += ".portrait"
        }
        if orientationMask.contains(.portraitUpsideDown) {
            if first {
                first = false
            } else {
                returnString += ", "
            }
            returnString += ".portraitUpsideDown"
        }
        if orientationMask.contains(.landscapeLeft) {
            if first {
                first = false
            } else {
                returnString += ", "
            }
            returnString += ".landscapeLeft"
        }
        if orientationMask.contains(.landscapeRight) {
            if first {
                first = false
            } else {
                returnString += ", "
            }
            returnString += ".landscapeRight"
        }
    }
    
    returnString += "]"
    
    return returnString
}

func displayPath(_ inputPath: IndexPath?) -> String {
    if let path = inputPath {
        let nsPath = path as NSIndexPath
        let length = path.count
        var pathString = "["
        var first = true
        for position in (0..<length) {
            if first {
                first = false
            } else {
                pathString += ", "
            }
            pathString += "\(nsPath.index(atPosition: position))"
        }
        
        pathString += "]"
        return pathString
    } else {
        return "nil"
    }
}

public func getSourceFileNameFromFullPath(_ file: String) -> String {
    let fileComponents1 = file.components(separatedBy: "/")
    let lastComponent1 = fileComponents1.last
    let fileComponents2 = lastComponent1!.components(separatedBy: ".")
    let firstComponent2 = fileComponents2.first
    return firstComponent2!
}
