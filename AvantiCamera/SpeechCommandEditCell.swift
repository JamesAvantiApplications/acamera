
import UIKit

protocol SpeechCommandEditCellDelegate {
    func deselectSpeechCommandEditCell(_ cell: SpeechCommandEditCell)
}

// MARK: -

class SpeechCommandEditCell: UITableViewCell {
    
    weak var speechCommand: SpeechCommand!
    
    weak var coreDataStack: CoreDataStack!
    
    var delegate: SpeechCommandEditCellDelegate!
    
    var doneOK = false {
        didSet {
            commandTextField.enablesReturnKeyAutomatically = doneOK
            if doneOK {
                commandTextField.rightViewMode = .always
            } else {
                commandTextField.rightViewMode = .never
            }
        }
    }
    
    @IBOutlet weak var commandTextField: UITextField!
    
    @IBAction func buttonPressed(_ sender: Any) {
        print("=== SpeechCommandEditCell.\(#function) - speechCommand.command: \(speechCommand.command)")
        
        // delete
        if speechCommand.commandIsEmpty {
            coreDataStack.context.delete(speechCommand)
            coreDataStack.saveContext()
            
            // revert
        } else {
            commandTextField.resignFirstResponder()
            delegate?.deselectSpeechCommandEditCell(self)
            commandTextField.text = speechCommand.command
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if let image = UIImage(named: "ClearButton") {
            commandTextField.clearButtonWithImage(image, color: Colors.mediumGreen)
        }
        
        commandTextField.addTarget(self, action: #selector(self.textFieldEdited(_:)), for: .editingChanged)
        commandTextField.delegate = self
    }
    
    func configureCell(speechCommand: SpeechCommand, coreDataStack: CoreDataStack, delegate: SpeechCommandEditCellDelegate?) {
        self.speechCommand = speechCommand
        self.coreDataStack = coreDataStack
        self.delegate = delegate
        
        commandTextField.text = speechCommand.command
        
        doneOK = commandTextField.text!.count > 0
        
        backgroundColor = Colors.lightGreen
        commandTextField.backgroundColor = Colors.lightGreen
        
        if speechCommand.commandIsEmpty {
            afterDelay(0.2) {
                self.commandTextField.becomeFirstResponder()
            }
        }
    }
    
    @objc func textFieldEdited(_ sender: UITextField){
        print("=== SpeechCommandEditCell.\(#function) - text=\(sender.text!)")
        
        doneOK = sender.text!.count > 0
    }
}

extension SpeechCommandEditCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("=== SpeechCommandEditCell.\(#function) ===")
        if doneOK {
            commandTextField.resignFirstResponder()
            speechCommand.setCommand(commandTextField.text!)
            coreDataStack.saveContext()
        }
        return doneOK
    }
}
