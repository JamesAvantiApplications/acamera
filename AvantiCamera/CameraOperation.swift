//
//  CameraOperation.swift
//  AvantiCamera
//
//  Created by James Hager on 3/23/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation
import CoreData

protocol CameraOperationDelegate {
    func operateCamera(_ cameraAction: CameraAction)
}

// MARK: -

class CameraOperation {
    
    /*
     let verbs = ["start", "take", "stop", "end", "select", "set"]
     
     let objects = ["camera", "photo", "picture", "video", "recording", "speech", "voice"]
     
     let verbFromVerb = [
     "start": "start",
     "take": "start",
     "stop": "stop",
     "end": "stop",
     "select": "set",
     "set": "set"
     ]
     
     let objectFromObject = [
     "camera": "photo",
     "photo": "photo",
     "picture": "photo",
     "video": "video",
     "recording": "video",
     "speech": "speech",
     "voice": "speech"
     ]
     
     let cameraActionFromCommand: [String: CameraAction] = [
     "set photo": .setPhoto,
     "start photo": .takePhoto,
     "set video": .setVideo,
     "start video": .startVideo,
     "stop video": .stopVideo,
     "stop speech": .stopSpeech
     ]
     */
    
    var coreDataStack: CoreDataStack!
    
    var verbs: Set<String>!
    var objects: Set<String>!
    
    var verb = ""
    var object = ""
    var command = ""
    
    var delegate: CameraOperationDelegate?
    
    var fetchRequest: NSFetchRequest<NSFetchRequestResult>!
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    init(coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
        
        fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SpeechCommand")
        
        setVerbsAndObjects()
    }
    
    func setVerbsAndObjects() {
        print("=== \(file).\(#function) === \(debugDate())")
        
        verbs = Set<String>()
        objects = Set<String>()
        
        do {
            let speechCommands = try coreDataStack.context.fetch(fetchRequest) as! [SpeechCommand]
            
            for speechCommand in speechCommands {
                verbs.insert(speechCommand.verb)
                objects.insert(speechCommand.object)
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func resetPhrase() {
        print("=== \(file).\(#function) === \(debugDate())")
        verb = ""
        object = ""
    }
    
    func testWord(_ wordIn: String) {
        print("=== \(file).\(#function) === \(debugDate()) - wordIn: '\(wordIn)'")
        let word = wordIn.lowercased()
        
        if verbs.contains(word) {
            verb = word
            print("--- \(file).\(#function) verb: '\(verb)'")
            return
        }
        
        if objects.contains(word) {
            object = word
            print("--- \(file).\(#function) object: '\(object)'")
        }
        
        command = verb + " " + object
        print("--- \(file).\(#function) - command: '\(command)'")
        
        if let cameraAction = cameraActionFor(verb: verb, object: object) {
            print("--- \(file).\(#function) - cameraAction: \(cameraAction)")
            resetPhrase()
            delegate?.operateCamera(cameraAction)
        }
    }
    
    func cameraActionFor(verb: String, object: String) -> CameraAction? {
        
        fetchRequest.predicate = NSPredicate(format: "verb == %@ AND object == %@", verb, object)
        
        do {
            let speechCommands = try coreDataStack.context.fetch(fetchRequest) as! [SpeechCommand]
            
            if speechCommands.count > 0 {
                return speechCommands[0].cameraAction
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return nil
    }
}
