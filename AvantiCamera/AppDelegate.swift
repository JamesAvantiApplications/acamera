//
//  AppDelegate.swift
//  AvantiCamera
//
//  Created by James Hager on 3/21/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import CoreData

public let copyrightYear = 2018
public let fullVersionDescriptionText = ""

public let presentSimpleNoticeNotificationName = Notification.Name(rawValue: "PresentSimpleNoticeNotificationName")
public let videoIndicatorSizeShouldChangeNotificationName = Notification.Name(rawValue: "VideoIndicatorSizeShouldChangeNotificationName")

public let myManagedObjectContextSaveDidFailNotificationName = Notification.Name(rawValue: "MyManagedObjectContextSaveDidFailNotification")

func fatalCoreDataError(_ error: Error) {
    print("*** Fatal error: \(error)")
    NotificationCenter.default.post(name: myManagedObjectContextSaveDidFailNotificationName, object: nil)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var supportedInterfaceOrientations: UIInterfaceOrientationMask = .portrait
    
    var navigationController: NavigationController!
    
    lazy var coreDataStack = CoreDataStack()
    
    lazy var file = getSourceFileNameFromFullPath(#file)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        print("=== \(file).\(#function) === \(debugDate())")
        
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let bundle = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
        print("--- \(file).\(#function) - version: \(version) (\(bundle))")
        
        customizeAppearance()
        setUserDefaultsIfNeeded()
        
        navigationController = window!.rootViewController as! NavigationController
        navigationController.delegate = self
        
        let cameraViewController = navigationController.viewControllers[0] as! CameraViewController
        cameraViewController.coreDataStack = coreDataStack
        
        subscribeToNotifications()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        print("=== \(file).\(#function) === \(debugDate())")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("=== \(file).\(#function) === \(debugDate())")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("=== \(file).\(#function) === \(debugDate())")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("=== \(file).\(#function) === \(debugDate())")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("=== \(file).\(#function) === \(debugDate())")
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        print("=== \(file).\(#function) === \(debugDate()) - supportedOrientations: \(describeUIInterfaceOrientationMask(supportedInterfaceOrientations))")
        return supportedInterfaceOrientations
    }
    
    //MARK: - Set Up App
    
    func customizeAppearance() {
        print("=== \(file).\(#function) ===")
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        UINavigationBar.appearance().barStyle = .black
        UINavigationBar.appearance().barTintColor = Colors.avantiGreen
        UINavigationBar.appearance().tintColor = Colors.textSelectable
        UINavigationBar.appearance().isTranslucent = false
        
        let selectionView = UIView()
        selectionView.backgroundColor = Colors.lightGreen
        UITableViewCell.appearance().selectedBackgroundView = selectionView
    }
    
    func setUserDefaultsIfNeeded() {
        print("=== setUserDefaultsIfNeeded ===")
        
        let userDefaults = UserDefaults.standard
        let userDefaultsCreated = userDefaults.bool(forKey: "UserDefaultsCreated")
        
        if !userDefaultsCreated {
            print("--- setUserDefaultsIfNeeded - set user defaults")
            userDefaults.set(true, forKey: "UserDefaultsCreated")
            setUserDefaultSettings(userDefaults)
            userDefaults.synchronize()
        }
        
        let speechDefaultsCreated = userDefaults.bool(forKey: "SpeechDefaultsCreated")
        if !speechDefaultsCreated {
            print("--- setUserDefaultsIfNeeded - set speech defaults")
            setDefaultSpeechCommands(coreDataStack: coreDataStack)
            userDefaults.set(true, forKey: "SpeechDefaultsCreated")
        }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SpeechCommand")
        
        do {
            let speechCommands = try coreDataStack.context.fetch(fetchRequest) as! [SpeechCommand]
            
            for speechCommand in speechCommands {
                print("=== setUserDefaultsIfNeeded === \(speechCommand.describe)")
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    //MARK: - Setup Functions
    
    func viewControllerForShowingAlert() -> UIViewController {
        let rootViewController = self.window!.rootViewController!
        if let presentedViewController = rootViewController.presentedViewController {
            return presentedViewController
        } else {
            return rootViewController
        }
    }
    
    func presentOnMainThread(alert: UIAlertController) {
        print("=== \(file).\(#function) === \(debugDate()) ===")
        OperationQueue.main.addOperation {
            self.viewControllerForShowingAlert().present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Notifications
    
    func subscribeToNotifications() {
        print("=== \(file).\(#function) === \(debugDate()) ===")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentSimpleNotice(_:)), name: presentSimpleNoticeNotificationName, object: nil)
    }
    
    @objc func presentSimpleNotice(_ notification: Notification) {
        print("=== \(file).\(#function) - notification='\(notification)' ===")
        
        if let userInfo = notification.userInfo as? Dictionary<String,Any>,
            let title = userInfo["title"] as? String {
            
            var message: String?
            
            if let theMessage = userInfo["message"] as? String {
                message = theMessage
            }
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            
            presentOnMainThread(alert: alert)
        }
    }
}

// MARK: - UINavigationControllerDelegate

extension AppDelegate: UINavigationControllerDelegate {
    
    func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        print("=== \(file).\(#function) === \(debugDate()) - supportedOrientations: \(describeUIInterfaceOrientationMask(supportedInterfaceOrientations))")
        return supportedInterfaceOrientations
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        print("=== \(file).\(#function) === \(debugDate()) - orientation: \(describeOrientation())")
        supportedInterfaceOrientations = viewController.supportedInterfaceOrientations
        print("--- \(file).\(#function) --- \(debugDate()) - supportedOrientations: \(describeUIInterfaceOrientationMask(supportedInterfaceOrientations))")
        self.navigationController.viewControllerWillShow = viewController
        
        if !(viewController is CameraViewController) {
            UIViewController.attemptRotationToDeviceOrientation()
        }
    }
}
