//
//  PhotoLibraryThumbCell.swift
//  AvantiCamera
//
//  Created by James Hager on 3/26/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import Photos

class PhotoLibraryThumbCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: ACImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    func configureCell(image: UIImage, mediaType: PHAssetMediaType, videoIndicatorSize: CGFloat, imageViewTag: Int, imageViewDelegate: ACImageViewDelegate) {
        //print("=== PhotoLibraryThumbCell.configureCell - imageViewTag: \(imageViewTag)")
        
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        imageView.videoIndicatorSize = videoIndicatorSize
        imageView.mediaType = mediaType
        
        imageView.tag = imageViewTag
        
        if imageViewTag > -1 {
            imageView.delegate = imageViewDelegate
        } else {
            imageView.delegate = nil
        }
        
        imageView.listenForVideoIndicatorSizeShouldChange = (mediaType == .video) && (imageViewTag > -1)
    }
}
